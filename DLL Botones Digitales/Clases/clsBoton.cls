VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsBoton"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mvarRelacion                                            As String
Private mvarPosicion                                            As Integer
Private mvarCodigo                                              As String
Private mvarClave                                               As String
Private mvaresGrupo                                             As Boolean
Private mvarDescripcion                                         As String
Private mvarColor                                               As Long
Private mvarTeclaMask                                           As String
Private mvarTecla                                               As String
Private mImagen                                                 As StdPicture
Private mFuente                                                 As StdFont

Property Let Relacion(ByVal vData As String)
    mvarRelacion = vData
End Property

Property Get Relacion() As String
    Relacion = mvarRelacion
End Property

Property Let Posicion(ByVal vData As Integer)
    mvarPosicion = vData
End Property

Property Get Posicion() As Integer
    Posicion = mvarPosicion
End Property

Property Set Fuente(ByVal vData As StdFont)
    Set mFuente = vData
End Property

Property Get Fuente() As StdFont
    Set Fuente = mFuente
End Property

Property Set Imagen(ByVal vData As StdPicture)
    Set mImagen = vData
End Property

Property Get Imagen() As StdPicture
    Set Imagen = mImagen
End Property

Property Let Color(ByVal vData As Long)
    mvarColor = vData
End Property

Property Get Color() As Long
    Color = mvarColor
End Property

Property Let Descripcion(ByVal vData As String)
    mvarDescripcion = vData
End Property

Property Get Descripcion() As String
    Descripcion = mvarDescripcion
End Property

Property Let esGrupo(ByVal vData As Boolean)
    mvaresGrupo = vData
End Property

Property Get esGrupo() As Boolean
    esGrupo = mvaresGrupo
End Property

Property Let Codigo(ByVal vData As String)
    mvarCodigo = vData
End Property

Property Get Codigo() As String
    Codigo = mvarCodigo
End Property

Property Let Clave(ByVal vData As String)
    mvarClave = vData
End Property

Property Get Clave() As String
    Clave = mvarClave
End Property

Property Let TeclaMask(ByVal vData As String)
    mvarTeclaMask = vData
End Property

Property Get TeclaMask() As String
    TeclaMask = mvarTeclaMask
End Property

Property Let tecla(ByVal vData As String)
    mvarTecla = vData
End Property

Property Get tecla() As String
    tecla = mvarTecla
End Property

Private Sub Class_Initialize()
    Set Imagen = New StdPicture
    Set mFuente = New StdFont
End Sub

Private Sub Class_Terminate()
    Set mImagen = Nothing
    Set mFuente = Nothing
End Sub
