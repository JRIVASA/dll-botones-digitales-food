VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsBotones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"clsBoton"
Attribute VB_Ext_KEY = "Member0" ,"clsBoton"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mCol As Collection

Public Function AddBoton(Rs As ADODB.Recordset) As clsBoton
    
    Dim objNewMember As clsBoton
    
    Set objNewMember = New clsBoton

    objNewMember.Codigo = Rs!Tag
    objNewMember.Clave = Rs!Clave
    objNewMember.Descripcion = Rs!texto
    objNewMember.esGrupo = Left(UCase(Trim(Rs!Clave)), 1) = "G"
    objNewMember.Color = BotonBkColor
    objNewMember.Relacion = Rs!Relacion
    objNewMember.Posicion = Rs!Posicion
    objNewMember.tecla = Rs!tecla
    objNewMember.TeclaMask = Rs!Mascara
    objNewMember.Fuente.Name = BotonFuente
    objNewMember.Fuente.Bold = BotonFuenteNeg
    objNewMember.Fuente.Size = BotonFuenteTam
    
    mCol.Add objNewMember
    
    Set AddBoton = objNewMember
    Set objNewMember = Nothing
    
End Function

Public Function AddBotonNew(Rs As ADODB.Recordset) As clsBoton
    
    Dim objNewMember As clsBoton
    
    Set objNewMember = New clsBoton

    objNewMember.Codigo = Rs!Tag
    objNewMember.Clave = Rs!Clave
    objNewMember.Descripcion = Rs!texto
    objNewMember.esGrupo = Left(UCase(Trim(Rs!Clave)), 1) = "G"
    objNewMember.Color = IIf(IsNull(Rs!Color), BotonBkColor, Rs!Color)
    objNewMember.Relacion = Rs!Relacion
    objNewMember.Posicion = Rs!Posicion
    objNewMember.tecla = Rs!tecla
    objNewMember.TeclaMask = Rs!Mascara
    
    If Not IsNull(Rs!imagenbtn) Then
        Cargar_Imagen objNewMember, Rs.Fields("imagenbtn")
    End If
    
    If Not IsNull(Rs!fuentenombre) Then
        objNewMember.Fuente.Name = Rs!fuentenombre
    Else
        objNewMember.Fuente.Name = BotonFuente
    End If
    
    If Not IsNull(Rs!fuentenegrita) Then
        objNewMember.Fuente.Bold = Rs!fuentenegrita
    Else
        objNewMember.Fuente.Bold = BotonFuenteNeg
    End If
    
    If Not IsNull(Rs!fuentetamano) Then
        objNewMember.Fuente.Size = Rs!fuentetamano
    Else
        objNewMember.Fuente.Size = BotonFuenteTam
    End If
    
    If Not IsNull(Rs!fuenteitalica) Then objNewMember.Fuente.Italic = Rs!fuenteitalica
    If Not IsNull(Rs!fuentesubrayado) Then objNewMember.Fuente.Underline = Rs!fuentesubrayado
    
    mCol.Add objNewMember
    
    Set AddBotonNew = objNewMember
    Set objNewMember = Nothing
    
End Function

Friend Property Get Item(vntIndexKey As Variant) As clsBoton
    Set Item = mCol(vntIndexKey)
End Property

Friend Property Get Count() As Long
    Count = mCol.Count
End Property

Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub

Public Property Get NewEnum() As IUnknown
    Set NewEnum = mCol.[_NewEnum]
End Property

Public Property Get Botones() As Collection
    Set Botones = mCol
End Property

Private Sub Cargar_Imagen(boton As clsBoton, pAdoField As ADODB.Field)
    
    Dim Stream As ADODB.Stream
    
    Set Stream = New ADODB.Stream
    Stream.Type = adTypeBinary
    Stream.Open
    Stream.Write pAdoField.Value
    Stream.SaveToFile App.Path & "\Prueba.bmp", adSaveCreateOverWrite
    Set boton.Imagen = LoadPicture(App.Path & "\Prueba.bmp")
    
    Set Stream = Nothing
      
    Exit Sub
    
End Sub

'******************************************************************

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    Set mCol = Nothing
End Sub
