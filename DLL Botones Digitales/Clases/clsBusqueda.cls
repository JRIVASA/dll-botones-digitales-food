VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsBusqueda"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Enum eOpCampoBusqueda
    opCampoStr
    opCampoBit
    opCampoNum
    opCampoFec
End Enum

Private Type Estruc_Busqueda
    sbEsCampoBusqueda                                               As Boolean
    sbCampoPred                                                     As Boolean
    sbCampoMostrar                                                  As String
    sbCampoTabla                                                    As String
    sbCampoRecordset                                                As String
    sbIdCampoBusqueda                                               As Integer
    sbCampoLongitud                                                 As Long
    sbCampoTipo                                                     As eOpCampoBusqueda
End Type

Private mvarConexion                                                As ADODB.Connection
Private DefBusqueda()                                               As Estruc_Busqueda
Private mvarMultiSeleccion                                          As Boolean
Private mvarPresionoStop                                            As Boolean
Private mvarCodigoSel                                               As String
Private mvarIndiceCampoFecha                                        As Integer
Private mvarContador                                                As Integer
Private mvarTextoCmd                                                As String
Private mvarResultado                                               As Variant
Private mvarAutoBusqueda                                            As Boolean
Private mvarTamanoLetra                                             As Integer
Private mvarNumeroLineaAvance                                       As Integer
Private mvarTitulo                                                  As String
Private mvarTextoBusquedaInicial                                    As String
Private mvarOrdenarPor                                              As String

Private mvarConsultando                                             As Boolean

Event AvanceBusqueda(RegActual As Long, NumRegistros As Long)

'*********************************** Property Set de la Clase ***********************************************

Property Set ConexionBusqueda(pValor As Object)
    Set mvarConexion = pValor
End Property

Property Get ConexionBusqueda() As Object
    Set ConexionBusqueda = mvarConexion
End Property

'*********************************** Property Let de la Clase ***********************************************

Property Let NumeroLineaAvance(pValor As Integer)
    mvarNumeroLineaAvance = pValor
End Property

Property Let Consultando(pValor As Boolean)
    mvarConsultando = pValor
End Property

Property Let OrdenarPor(pValor As String)
    mvarOrdenarPor = pValor
End Property

Property Let TextoBusquedaInicial(pValor As String)
    mvarTextoBusquedaInicial = pValor
End Property

Property Let ResultadoBusqueda(pValor)
    mvarResultado = pValor
End Property

Property Let MultiSeleccion(pValor As Boolean)
    mvarMultiSeleccion = pValor
End Property

Property Let IndiceCampoFecha(pValor As Integer)
    mvarIndiceCampoFecha = pValor
End Property

Property Let TextoComandoSql(pValor As String)
    mvarTextoCmd = pValor
End Property

Property Let PresionoStop(pValor As Boolean)
    mvarPresionoStop = pValor
End Property

Property Let CodigoSeleccionado(pValor As String)
    mvarCodigoSel = pValor
End Property

'*********************************** Property Get de la Clase ***********************************************

Property Get Consultando() As Boolean
    Consultando = mvarConsultando
End Property

Property Get OrdenarPor() As String
    OrdenarPor = mvarOrdenarPor
End Property

Property Get TextoBusquedaInicial() As String
    TextoBusquedaInicial = mvarTextoBusquedaInicial
End Property

Property Get MultiSeleccion() As Boolean
    MultiSeleccion = mvarMultiSeleccion
End Property

Property Get BusquedaManejaFecha() As Boolean
    BusquedaManejaFecha = mvarManejaFecha
End Property

Property Get PresionoStop() As Boolean
    PresionoStop = mvarPresionoStop
End Property

Property Get TextoComandoSql() As String
    TextoComandoSql = mvarTextoCmd
End Property

Property Get IndiceCampoFecha() As Integer
    IndiceCampoFecha = mvarIndiceCampoFecha
End Property

Property Get ResultadoBusqueda()
    ResultadoBusqueda = mvarResultado
End Property

Property Get AutoBusqueda() As Boolean
    AutoBusqueda = mvarAutoBusqueda
End Property

Property Get TamanoLetra() As Integer
    TamanoLetra = mvarTamanoLetra
End Property

Property Get NumeroLineaAvance() As Integer
    NumeroLineaAvance = mvarNumeroLineaAvance
End Property

Property Get Titulo() As String
    Titulo = mvarTitulo
End Property

'*************************************************** Metodos de la Clase *****************************************

Public Sub AgregarCamposBusqueda(pCampoMostrar As String, pCampoTabla As String, pCampoRs As String, pLongitud As Long, Optional pesCampoFiltro As Boolean = False, Optional pTipoCampo As eOpCampoBusqueda = opCampoStr, Optional Predeterminado As Boolean = False)
    ReDim Preserve DefBusqueda(mvarContador)
    DefBusqueda(mvarContador).sbCampoMostrar = pCampoMostrar
    DefBusqueda(mvarContador).sbCampoPred = Predeterminado
    DefBusqueda(mvarContador).sbCampoTabla = pCampoTabla
    DefBusqueda(mvarContador).sbCampoRecordset = pCampoRs
    DefBusqueda(mvarContador).sbCampoLongitud = pLongitud
    DefBusqueda(mvarContador).sbEsCampoBusqueda = pesCampoFiltro
    DefBusqueda(mvarContador).sbCampoTipo = pTipoCampo
    If pesCampoFiltro And pTipoCampo = opCampoFec Then
        If IndiceCampoFecha = -1 Then IndiceCampoFecha = mvarContador
        mvarManejaFecha = True
    End If
    mvarContador = mvarContador + 1
End Sub

Public Function MostrarInterfazBusqueda(pTitulo As String, Optional AutoBusqueda As Boolean = False, Optional MultiSeleccion As Boolean, Optional NumLineasAvance As Integer = 7, Optional TamLetra As Integer = 18, Optional pTextoBusquedaInicial As String = "")
    
    With Frm_Super_Consultas
        Set .mClsBusqueda = Me
        mvarTitulo = pTitulo
        mvarAutoBusqueda = AutoBusqueda
        mvarMultiSeleccion = MultiSeleccion
        mvarNumeroLineaAvance = NumLineasAvance
        mvarTamanoLetra = TamLetra
        mvarTextoBusquedaInicial = pTextoBusquedaInicial
        .Show vbModal
        MostrarInterfazBusqueda = mvarResultado
        Set Frm_Super_Consultas = Nothing
    End With
    
End Function

Friend Sub IniciarFrmBusqueda(ByRef pLv As ListView, ByRef pCbo As ComboBox)
    
    Dim mCampo As Integer
    Dim nIndex As Integer
    
    pLv.ListItems.Clear
    pLv.ColumnHeaders.Clear
    pLv.MultiSelect = MultiSeleccion: pLv.Checkboxes = MultiSeleccion
    
    pCbo.Clear
    
    For mCampo = 0 To mvarContador - 1
        pLv.ColumnHeaders.Add , , DefBusqueda(mCampo).sbCampoMostrar, DefBusqueda(mCampo).sbCampoLongitud
        If DefBusqueda(mCampo).sbEsCampoBusqueda And DefBusqueda(mCampo).sbCampoTipo <> opCampoFec Then
            pCbo.AddItem DefBusqueda(mCampo).sbCampoMostrar, contador
            pCbo.ItemData(contador) = mCampo
            If DefBusqueda(mCampo).sbCampoPred Then nIndex = contador
            contador = contador + 1
        End If
    Next mCampo
    
    pCbo.ListIndex = nIndex
        
End Sub

Friend Function BuscarFiltroConsulta(pCriterio, pDatoBuscar, pIndice As String, pFechaI As Date, pFechaF As Date)
    
    Dim mWhere As String
    
    mWhere = IIf(InStr(1, UCase(pCriterio), "WHERE") > 1, " AND ", " WHERE ")
    
    Select Case DefBusqueda(pIndice).sbCampoTipo
       Case eOpCampoBusqueda.opCampoStr
            mWhere = mWhere & DefBusqueda(pIndice).sbCampoTabla & " LIKE '" & pDatoBuscar & "%' "
       
       Case eOpCampoBusqueda.opCampoNum, eOpCampoBusqueda.opCampoBit
            mWhere = mWhere & DefBusqueda(pIndice).sbCampoTabla & " = " & pDatoBuscar
            
       Case Else
            mWhere = mWhere & DefBusqueda(pIndice).sbCampoTabla & " = '" & pDatoBuscar & "' "
        
    End Select
    
    BuscarFiltroConsulta = mWhere
            
End Function

Friend Sub LlenarLvBusqueda(pLv As ListView, pRs As ADODB.Recordset)
    
    Dim mItem As ListItem
    
    On Error GoTo Errores
    
    PresionoStop = False
    Consultando = True
    
    Do While Not pRs.EOF And Not PresionoStop
        
        DoEvents
        
        If Not Consultando Then GoTo Finally
        
        Set mItem = pLv.ListItems.Add(, , pRs.Fields(DefBusqueda(0).sbCampoRecordset).Value)
        
        For I = 1 To mvarContador - 1
            Select Case DefBusqueda(I).sbCampoTipo
                Case eOpCampoBusqueda.opCampoBit
                    mItem.SubItems(I) = IIf(pRs.Fields(DefBusqueda(I).sbCampoRecordset).Value, "Si", "No")
                Case eOpCampoBusqueda.opCampoNum
                    mItem.SubItems(I) = FormatNumber(pRs.Fields(DefBusqueda(I).sbCampoRecordset).Value, 2)
                Case eOpCampoBusqueda.opCampoFec
                    mItem.SubItems(I) = FormatDateTime(pRs.Fields(DefBusqueda(I).sbCampoRecordset).Value, vbShortDate)
                Case Else
                    mItem.SubItems(I) = pRs.Fields(DefBusqueda(I).sbCampoRecordset).Value
            End Select
        Next I
        
        RaiseEvent AvanceBusqueda(pRs.AbsolutePosition, pRs.RecordCount)
        
        pRs.MoveNext
        
    Loop
    
Finally:
    
    PresionoStop = False
    Consultando = False
    
    Exit Sub
    
Errores:
    
    MensajeSistema Err.Description, False
    Err.Clear
    
    GoTo Finally
    
End Sub

Friend Function BuscarCampoFecha() As String
    BuscarCampoFecha = DefBusqueda(IndiceCampoFecha).sbCampoTabla
End Function

Friend Function TomarDatosSeleccionados(pLv As ListView)
    
    Dim mArrDatos As Variant
    Dim mCadena As String
    Dim mFila As Long, mContador As Long
    
    mContador = 0
    ReDim mArrDatos(mContador)
    
    If MultiSeleccion Then
        For mFila = 1 To pLv.ListItems.Count
            If pLv.ListItems(mFila).Checked Then
                ReDim Preserve mArrDatos(mContador)
                mArrDatos(mContador) = Split(TomarFilaLv(pLv, mFila), "|")
                mContador = mContador + 1
            End If
        Next mFila
    Else
        mArrDatos(mContador) = Split(TomarFilaLv(pLv, pLv.SelectedItem.Index), "|")
    End If
    
    TomarDatosSeleccionados = mArrDatos
        
End Function

Private Function TomarFilaLv(pLv As ListView, pFila As Long) As String
    
    Dim mCadena As String
    Dim mCol As Integer
    
    mCadena = pLv.ListItems(pFila).Text
    
    For mCol = 1 To pLv.ColumnHeaders.Count - 1
        mCadena = mCadena & "|" & pLv.ListItems(pFila).SubItems(mCol)
    Next mCol
    
    TomarFilaLv = mCadena
    
End Function

Friend Function ValidarSeleccion(pLv As ListView) As Boolean
    
    Dim mFila As Long
    Dim mFound As Boolean
    
    mFila = 0
    mFound = Not mvarMultiSeleccion
    
    If mvarMultiSeleccion Then
        While mFila < pLv.ListItems.Count And Not mFound
             mFila = mFila + 1
             mFound = pLv.ListItems(mFila).Checked
        Wend
    End If
    
    ValidarSeleccion = mFound
    
End Function

Friend Function MensajeSistema(Msg As String, BtnCancel As Boolean) As Boolean
    
    Dim cMensaje As clsMensajeria
    
    Set cMensaje = New clsMensajeria
    MensajeSistema = cMensaje.Mensaje(Msg, BtnCancel)
    Set cMensaje = Nothing
    
End Function

'*************************************************** Inicio Clase ********************************************

Private Sub Class_Initialize()
    ReDim DefBusqueda(0)
    IndiceCampoFecha = -1
End Sub

Private Sub Class_Terminate()
    Set mvarConexion = Nothing
End Sub
