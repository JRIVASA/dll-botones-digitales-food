VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsCaja"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mvarCodigo                                                  As String
Private mvarPerfil                                                  As String
Private mvarDescripcion                                             As String

Friend Property Let Descripcion(ByVal vData As String)
    mvarDescripcion = vData
End Property

Friend Property Get Descripcion() As String
    Descripcion = mvarDescripcion
End Property

Friend Property Let Perfil(ByVal vData As String)
    mvarPerfil = vData
End Property

Friend Property Get Perfil() As String
    Perfil = mvarPerfil
End Property

Friend Property Let Codigo(ByVal vData As String)
    mvarCodigo = vData
End Property

Friend Property Get Codigo() As String
    Codigo = mvarCodigo
End Property




