VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsConfigBotones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mConexion                                       As clsConexion
Private mCajas                                          As clsCajas
Private mBotones                                        As clsBotones
Private mCajaSel                                        As clsCaja
Private mBotonSel                                       As clsBoton
Private mPerfiles                                       As clsPerfiles
Private mPerfilSel                                      As clsPerfil
Private mCmdButton                                      As CommandButton
Private mvarCamposNuevos                                As Boolean
Private mvarGrabo                                       As Boolean
Private mvarUsuarioStellar                              As String

Property Get Conexion() As clsConexion
    Set Conexion = mConexion
End Property

Property Get Cajas() As clsCajas
    Set Cajas = mCajas
End Property

Property Get CajaSel() As clsCaja
    Set CajaSel = mCajaSel
End Property

Property Get Botones() As clsBotones
    Set Botones = mBotones
End Property

Property Get BotonSel() As clsBoton
    Set BotonSel = mBotonSel
End Property

Property Get CmdButton() As Object
    Set CmdButton = mCmdButton
End Property

Property Get CamposNuevos() As Boolean
    CamposNuevos = mvarCamposNuevos
End Property

Property Get UsuarioStellar() As String
    UsuarioStellar = mvarUsuarioStellar
End Property

Property Let Grabo(ByVal vData As Boolean)
    mvarGrabo = vData
End Property

Property Get Grabo() As Boolean
    Grabo = mvarGrabo
End Property

Property Get Perfiles() As clsPerfiles
    Set Perfiles = mPerfiles
End Property

Property Get PerfilSel() As clsPerfil
    Set PerfilSel = mPerfilSel
End Property

Property Set PerfilSel(cPerfil As clsPerfil)
    Set mPerfilSel = cPerfil
End Property

Public Sub IniciarInterfaz(Servidor As String, Usuario As String, Clave As String, BD As String, _
CnnTO As Integer, CmdTO As Integer, UsuarioStellar As String, Optional Proveedor As String = "SQLOLEDB.1")
    
    ModoTouch = True
    
    Dim TmpVal As Variant
    
    TmpVal = GetEnvironmentVariable("ProgramW6432")
    
    Select Case Len(Trim(TmpVal))
        Case 0 ' Default / No Especificado.
            WindowsArchitecture = [32Bits]
        Case Else
            WindowsArchitecture = [64Bits]
    End Select
    
    ' -----------------------------
    
    Set mConexion = New clsConexion
    
    mConexion.IniciarClase Servidor, BD, Usuario, Clave, CnnTO, CmdTO, Proveedor
    
    mvarUsuarioStellar = UsuarioStellar
    
    LlenarCajas
    
    Set frmMain.fCls = Me
    
    frmMain.Show vbModal
    
    Set frmMain = Nothing
    
End Sub

Public Function Mensaje(ByVal Msg As String, Optional ByVal BtnCancelar As Boolean = False) As Boolean
    Dim mMensaje As clsMensajeria
    
    Set mMensaje = New clsMensajeria
    Mensaje = mMensaje.Mensaje(Msg, BtnCancelar)
    Set mMensaje = Nothing
End Function

Friend Sub IniciarBotones()
    Set mBotones = New clsBotones
End Sub

Friend Sub IniciarPerfil()
    Set mPerfilSel = New clsPerfil
End Sub

Friend Sub ResetPerfil()
    Set mPerfilSel = Nothing
End Sub

Friend Sub ConfigurarBotonesCaja()
    If BuscarBotones(mPerfilSel.Perfil, True) Then
        Set frmBotones.fCls = Me
        frmBotones.Show vbModal
        Set frmBotones = Nothing
        'esto se coloca por si acaso no grabo los cambios q lo deje como estaba antes
        If Not mvarGrabo Then BuscarBotones mPerfilSel.Perfil, True
        Set mPerfilSel.Botones = mBotones
    End If
End Sub

Friend Sub ConfigurarPerfil(Perfil As String)
        
    IniciarBotones
    
    BuscarPerfil Perfil
    
    If mPerfilSel Is Nothing Then IniciarPerfil
    
    If BuscarBotones(Perfil, True) Then
        Set mPerfilSel.Botones = mBotones
        Set frmPerfil.fCls = Me
        frmPerfil.Show vbModal
        Set frmPerfil = Nothing
    End If
    
End Sub

Friend Sub LlenarCajas()
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    
    mSQl = "SELECT MA_CAJA.C_Codigo, MA_CAJA.C_Desc_Caja, COALESCE (MA_CAJA_BOTONES_DIGITALES.CodigoPerfil, '') AS Perfil " _
        & "FROM MA_CAJA LEFT OUTER JOIN MA_CAJA_BOTONES_DIGITALES ON MA_CAJA.C_Codigo = MA_CAJA_BOTONES_DIGITALES.CAJA "
        
    Set mCajas = New clsCajas
    If mConexion.ConectarBD Then
        Set mRs = New ADODB.Recordset
        mRs.Open mSQl, mConexion.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
        Do While Not mRs.EOF
            mCajas.Add mRs!c_codigo, mRs!c_Desc_Caja, mRs!Perfil
            mRs.MoveNext
        Loop
    End If
End Sub

Public Sub BuscarPerfil(Codigo As String)
    
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    
    If mvarCamposNuevos Then
        mSQl = "SELECT codigoperfil,descripcion,coalesce(horaini,'00:00:00') as horaini, " _
            & "coalesce(horafin,'00:00:00') as horafin,coalesce(dias,0)as dias from MA_CAJA_BOTONES_DIGITALES_PERFIL where codigoperfil='" & Codigo & "'"
    Else
        mSQl = "SELECT codigoperfil,descripcion,'00:00:00' as horaini, " _
            & "'23:59:59' as horafin,127 as dias from MA_CAJA_BOTONES_DIGITALES_PERFIL  where codigoperfil='" & Codigo & "'"
    End If
        
    Set mPerfiles = New clsPerfiles
    Set mPerfilSel = Nothing
    
    If mConexion.ConectarBD Then
        Set mRs = New ADODB.Recordset
        mRs.Open mSQl, mConexion.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
        Do While Not mRs.EOF
            Set mPerfilSel = mPerfiles.Add(mRs!Dias, mRs!HoraFin, mRs!HoraIni, mRs!CodigoPerfil, mRs!Descripcion)
            mRs.MoveNext
        Loop
    End If
    
End Sub

Friend Function BuscarBotones(Codigo As String, Optional EsPErfil As Boolean = False) As Boolean
    
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    Dim MostroMsg As Boolean
    
    If mConexion.ConectarBD Then
        
        On Error GoTo Errores
        Set mBotones = New clsBotones
        'Set mPerfilSel.Botones = New clsBotones
        mSQl = "Select * from TR_Caja_Botones_Digitales" & IIf(EsPErfil, "_Perfil where codigoperfil='", " where caja='") & Codigo & "' " _
            & "order by relacion desc,posicion "
        'MsgBox mSql
        Set mRs = New ADODB.Recordset
        mRs.CursorLocation = adUseClient
        mRs.Open mSQl, mConexion.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
        mRs.ActiveConnection = Nothing
        mvarCamposNuevos = mConexion.ExisteCampoRs(mRs, "imagenbtn")
        Do While Not mRs.EOF
            If Not ExisteBotonEnPosicion(mRs!Posicion, mRs!Relacion) Then
                If mvarCamposNuevos Then
                    mBotones.AddBotonNew mRs
                Else
                    mBotones.AddBoton mRs
                End If
            Else
                If Not MostroMsg Then mMensaje.Mensaje "Existe uno o mas botones en la poscion en este nivel"
                MostroMsg = True
            End If
            mRs.MoveNext
        Loop
        BuscarBotones = True
    End If
    
    Exit Function
    
Errores:
    
    Mensaje Err.Description, False
    
End Function

Private Function ExisteBotonEnPosicion(POS As Integer, Rel As String) As Boolean
    
    Dim mBot As clsBoton
    
    For Each mBot In mBotones.Botones
        If mBot.Posicion = POS And StrComp(Rel, mBot.Relacion, vbTextCompare) = 0 Then
            ExisteBotonEnPosicion = True
            Exit Function
        End If
    Next
    
End Function

Friend Sub AgregarModificarBoton(ClsB As clsBoton, cmd As CommandButton, Optional esNuevo As Boolean = False)
    mvarGrabo = False
    Set mBotonSel = ClsB
    Set mCmdButton = cmd
    Set frmPropiedades.fCls = Me
    frmPropiedades.Show vbModal
    Set frmPropiedades = Nothing
    If esNuevo And mvarGrabo Then
        mBotones.Botones.Add ClsB
    End If
End Sub

Friend Sub CopiarPerfilCaja(caja As String, Perfil As String)
    
    If mConexion.ConectarBD Then
        On Error GoTo Errores
        mConexion.Conexion.BeginTrans
        EliminarDatos mConexion.Conexion, caja, False
        CopiarConfiguracionCaja mConexion.Conexion, caja, Perfil
        mConexion.Conexion.CommitTrans
    End If
    Exit Sub
Errores:
    mConexion.Conexion.RollbackTrans
    Mensaje Err.Description, False
    Err.Clear
    
End Sub

Private Sub EliminarDatos(Cn As ADODB.Connection, Codigo As String, Optional EsPErfil As Boolean = False)
    Dim mSQl As String
    mSQl = "Delete from ma_Caja_botones_digitales" & IIf(EsPErfil, "_Perfil where codigoperfil='", " where caja='") & Codigo & "' "
    Cn.Execute mSQl, reg
    mSQl = "Delete from tr_Caja_botones_digitales" & IIf(EsPErfil, "_Perfil where codigoperfil='", " where caja='") & Codigo & "' "
    Cn.Execute mSQl, reg
End Sub

Private Sub CopiarConfiguracionCaja(Cn As ADODB.Connection, caja As String, Perfil As String)
    
    Dim mSQl As String
    
    If mvarCamposNuevos Then
        mSQl = "Insert into ma_Caja_botones_digitales(caja,codigoperfil,fijar,horaini,horafin,dias) " _
                & "Select '" & caja & "' as caja,'" & Perfil & "' as Perfil,1,horaini,horafin,dias " _
                & "from ma_Caja_botones_digitales_perfil where (codigoperfil='" & Perfil & "')"
    Else
        mSQl = "Insert into ma_Caja_botones_digitales(caja,codigoperfil,fijar) " _
                & "Select '" & caja & "' as caja,'" & Perfil & "' as Perfil,1 " _
                & "from ma_Caja_botones_digitales_perfil where (codigoperfil='" & Perfil & "')"
    End If
    
    Cn.Execute mSQl, reg
    
    If mvarCamposNuevos Then
        mSQl = "Insert into tr_Caja_botones_digitales(caja,tag,texto,clave,relacion,posicion,mascara,tecla,imagen, " _
                & "color,fuentenombre,fuentenegrita,fuentetamano,fuenteitalica,fuentesubrayado,imagenbtn) " _
                & "Select '" & caja & "' as caja,tag,texto,clave,relacion,posicion,mascara,tecla,imagen " _
                & ",color,fuentenombre,fuentenegrita,fuentetamano,fuenteitalica,fuentesubrayado,imagenbtn " _
                & "from tr_Caja_botones_digitales_perfil where (codigoperfil='" & Perfil & "')"
    Else
        mSQl = "Insert into tr_Caja_botones_digitales(caja,tag,texto,clave,relacion,posicion,mascara,tecla,imagen) " _
                & "Select '" & caja & "' as caja,tag,texto,clave,relacion,posicion,mascara,tecla,imagen " _
                & "from tr_Caja_botones_digitales_perfil where (codigoperfil='" & Perfil & "')"
    End If
     
    Cn.Execute mSQl, reg
    
End Sub

Private Sub InsertarCabeceroPerfil(Cn As ADODB.Connection, Codigo As String)
    Dim mSQl As String
    
    mSQl = "Insert into ma_Caja_botones_digitales_perfil(codigoperfil,descripcion,horaini,horafin,dias) " _
        & "values ('" & Codigo & "','" & mPerfilSel.Descripcion & "','" & Format(mPerfilSel.HoraIni, "HH:mm:ss") & "', " _
        & "'" & Format(mPerfilSel.HoraFin, "HH:mm:ss") & "'," & mPerfilSel.Dias & ")"
    Cn.Execute mSQl, reg
End Sub

Private Sub InsertarDetalle(Cn As ADODB.Connection, Codigo As String, Optional EsPErfil As Boolean = False)
    
    Dim Rs As ADODB.Recordset
    Dim mBtn As clsBoton
    Dim mSQl As String
    Dim mClave As String, mTag As String
    
    Set Rs = New ADODB.Recordset
    
    mSQl = "Select * from tr_Caja_botones_digitales" & IIf(EsPErfil, "_Perfil", "") & " where 1=2"
    
    Rs.Open mSQl, Cn, adOpenKeyset, adLockBatchOptimistic, adCmdText
    
    For Each mBtn In mBotones.Botones
        
        If mBtn.esGrupo Then
            If mBtn.Clave Like "K*" Then
                mClave = Correlativo(Cn, "cs_cod_Grupo")
                mTag = mClave
            Else
                mClave = mBtn.Clave
                mTag = mBtn.Clave
            End If
        Else
            If mBtn.Clave Like "K*" Then
                mClave = Correlativo(Cn, "cs_cod_producto")
            Else
                mClave = mBtn.Clave
            End If
            mTag = mBtn.Codigo
        End If
        
        Rs.AddNew
        
            If EsPErfil Then
                Rs!CodigoPerfil = Codigo
            Else
                Rs!caja = Codigo
            End If
            
            Rs!Tag = mTag
            Rs!texto = mBtn.Descripcion
            Rs!Clave = mClave
            Rs!Relacion = mBtn.Relacion
            Rs!Posicion = mBtn.Posicion
            Rs!tecla = mBtn.tecla
            Rs!Mascara = mBtn.TeclaMask
            Rs!Imagen = "DPTO CERRADO"
            
            If mvarCamposNuevos Then
                
                Rs!Color = mBtn.Color
                Rs!fuentenombre = mBtn.Fuente.Name
                Rs!fuentenegrita = mBtn.Fuente.Bold
                Rs!fuentetamano = mBtn.Fuente.Size
                Rs!fuenteitalica = mBtn.Fuente.Italic
                Rs!fuentesubrayado = mBtn.Fuente.Underline
                
                If mBtn.Imagen.Handle <> 0 Then
                    If mBtn.Imagen.Type = vbPicTypeIcon Then
                        nombre = App.Path & "\" & "Prueba.ico"
                    ElseIf mBtn.Imagen.Type = vbPicTypeMetafile Then
                        nombre = App.Path & "\" & "Prueba.wmf"
                    ElseIf mBtn.Imagen.Type = vbPicTypeEMetafile Then
                        nombre = App.Path & "\" & "Prueba.emf"
                    Else
                        nombre = App.Path & "\" & "Prueba.bmp"
                    End If
                    
                    SavePicture mBtn.Imagen, nombre
                    Guardar_Imagen Rs.Fields("imagenbtn"), CStr(nombre)
                        
                End If
                
            End If
            
        Rs.Update
        
    Next
    
    Rs.UpdateBatch
    
End Sub

Private Function Correlativo(Cn As ADODB.Connection, Campo As String) As String
    
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    
    mSQl = "Select * from ma_correlativos where cu_campo='" & Campo & "' "
    
    Set mRs = New ADODB.Recordset
    mRs.Open mSQl, Cn, adOpenStatic, adLockOptimistic, adCmdText
    
    If Not mRs.EOF Then
        mRs!nu_Valor = mRs!nu_Valor + 1
        Correlativo = Format(mRs!nu_Valor, mRs!cu_formato)
        mRs.Update
    Else
        Err.Raise -100000, , "No se encontro el campo correlativo"
    End If
    
End Function

Private Function Guardar_Imagen(pAdoField As ADODB.Field, Optional Path_Imagen As String) As Boolean
    
    Dim Stream As ADODB.Stream
  
    Set Stream = New ADODB.Stream
    
    Stream.Type = adTypeBinary
    Stream.Open
    
    If Len(Path_Imagen) <> 0 Then
        Stream.LoadFromFile Path_Imagen
        pAdoField.Value = Stream.Read
    End If
   
    If Stream.State = adStateOpen Then
        Stream.Close
    End If
    
    Set Stream = Nothing
    
    Guardar_Imagen = True
      
    Exit Function
    
End Function

Public Function GrabarPerfil() As Boolean
    
    Dim mBtn As clsBoton
    Dim mBegin As Boolean
    Dim mCodigoPerfil As String
    
    On Error GoTo Errores
    
    With mConexion
        
        If .ConectarBD Then
            
            .Conexion.BeginTrans
            
            If mPerfilSel.Perfil <> "" Then
                mCodigoPerfil = mPerfilSel.Perfil
            Else
                mCodigoPerfil = Correlativo(.Conexion, "cs_cod_perfil")
            End If
            
            'primero eliminamos
            mBegin = True
            EliminarDatos .Conexion, mCodigoPerfil, True
            ' insertamos cabecero
            InsertarCabeceroPerfil .Conexion, mCodigoPerfil
            'insertamos detalle
            InsertarDetalle .Conexion, mCodigoPerfil, True
            
            For Each mCajaSel In mCajas.Cajas
                If StrComp(mCajaSel.Perfil, mCodigoPerfil, vbTextCompare) = 0 Then
                    EliminarDatos .Conexion, mCajaSel.Codigo, False
                    CopiarConfiguracionCaja .Conexion, mCajaSel.Codigo, mCodigoPerfil
                End If
            Next
            
            .Conexion.CommitTrans
            mBegin = False
            mPerfilSel.Perfil = mCodigoPerfil
            
            GrabarPerfil = True
            
        End If
        
    End With
    
    Exit Function
    
Errores:
    
    If mBegin Then mConexion.Conexion.RollbackTrans
    Mensaje Err.Description, False
    
End Function

Friend Function ValidarEliminar(ByVal Codigo As String) As Boolean
    
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    
    mSQl = "SELECT P.CodigoPerfil, P.Descripcion FROM MA_CAJA_BOTONES_DIGITALES_PERFIL P INNER JOIN MA_CAJA_BOTONES_DIGITALES C ON P.CodigoPerfil = C.CodigoPerfil WHERE C.CodigoPerfil = '" & Codigo & "'"
    
    Set mRs = New ADODB.Recordset
    
    mRs.Open mSQl, mConexion.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        Mensaje "El Perfil [" & mRs!CodigoPerfil & "][" & mRs!Descripcion & "] no puede ser eliminado ya que actualmente esta asociado a las cajas."
        Exit Function
    End If
    
    ValidarEliminar = True
    
End Function

Friend Function EliminarPerfil(ByVal Codigo As String) As Boolean
    
    On Error GoTo Errores
    
    If mConexion.ConectarBD Then
        mConexion.Conexion.BeginTrans
        EliminarDatos mConexion.Conexion, Codigo, True
        mConexion.Conexion.CommitTrans
        EliminarPerfil = True
    End If
    
    Exit Function
    
Errores:
    
    mConexion.Conexion.RollbackTrans
    
End Function

Public Function PoseeItemsRelacionados(Clave As String) As Boolean
    
    PoseeItemsRelacionados = False
    
    Dim mBtn As clsBoton
    
    For Each mBtn In Me.Botones.Botones
        If StrComp(mBtn.Relacion, Clave, vbTextCompare) = 0 Then
            PoseeItemsRelacionados = True
            Exit Function
        End If
    Next
    
End Function

Private Sub Class_Initialize()
    Set mMensaje = New clsMensajeria
End Sub

Private Sub Class_Terminate()
    Set mMensaje = Nothing
End Sub
