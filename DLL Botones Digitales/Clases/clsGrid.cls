VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsGrid"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mvarSelectionCells                               As New Collection
Private mvarUltimoTexto                                  As String
Private mvarUltimaFilaSel                                As Long
Private mvarColorUltFilaSel                            As Long
Private mvarFilaActual                                   As Long
Private mGrid                                            As Object
Private mvarSoloCol                                      As Integer

Property Get UltimaFilaSel() As Long
    UltimaFilaSel = mvarUltimaFilaSel
End Property

Property Get FilaActual() As Long
    FilaActual = mvarFilaActual
End Property

Public Sub IniciarGrid(ByRef pGrd As Object, pCols As Integer, pRows As Integer, _
        pGridLines As GridLineSettings, pGridLinesFix As GridLineSettings, _
        Optional pColFix As Integer = 0, Optional pRowFix As Integer = 1, _
        Optional pColor, Optional pColorBk, Optional pColorBkFix, Optional pColorBkSel, _
        Optional pForeColor, Optional pForeColorFix, Optional pForeColorSel, _
        Optional pColorGrid, Optional pColorGridFix, Optional pScrollbar As ScrollBarsSettings = 3)
        
    pGrd.Clear
    pGrd.Cols = pCols
    pGrd.Rows = pRows
    pGrd.GridLines = pGridLines
    pGrd.GridLinesFixed = pGridLinesFix
    AsignarPropiedadesOpcional pGrd, "FixedCols", pColFix
    AsignarPropiedadesOpcional pGrd, "FixedRows", pRowFix
    AsignarPropiedadesOpcional pGrd, "BackColor", pColor
    AsignarPropiedadesOpcional pGrd, "BackColorBkg", pColorBk
    AsignarPropiedadesOpcional pGrd, "BackColorFixed", pColorBkFix
    AsignarPropiedadesOpcional pGrd, "BackColorSel", pColorBkSel
    AsignarPropiedadesOpcional pGrd, "ForeColor", pForeColor
    AsignarPropiedadesOpcional pGrd, "ForeColorFixed", pForeColorFix
    AsignarPropiedadesOpcional pGrd, "ForeColorSel", pForeColorSel
    AsignarPropiedadesOpcional pGrd, "GridColor", pColorGrid
    AsignarPropiedadesOpcional pGrd, "GridColorFixed", pColorGridFix
    AsignarPropiedadesOpcional pGrd, "ScrollBars", pScrollbar
    For I = 0 To pCols - 1
        pGrd.ColWidth(I) = 0
    Next I
        
End Sub

Private Sub AsignarPropiedadesOpcional(ByRef pGrd As Object, pPropiedad As String, pValor)
    On Error GoTo Errores
    If Not IsMissing(pValor) Then
        CallByName pGrd, pPropiedad, VbLet, pValor
    End If
Errores:
    Err.Clear
End Sub

Public Sub EncabezadoGrid(pGrd As Object, pCol As Integer, pAncho As Long, pTexto As String, pAlineacion As AlignmentSettings, Optional pAplicarCol As Boolean = False, Optional pRow As Integer = 0)
    With pGrd
        .Row = pRow
        .Col = pCol
        .Text = pTexto
        .ColWidth(pCol) = pAncho
        If pAplicarCol Then
            .CellAlignment = pAlineacion
            .ColAlignment(pCol) = pAlineacion
        Else
            .CellAlignment = pAlineacion
        End If
    End With
End Sub

Public Sub MostrarPicture(pFrm As Form, pGrd As Object, pPic As PictureBox, CellRow As Long, cellCol As Long)
    With pGrd
        .Row = CellRow
        .Col = cellCol
        pPic.Move .Left + .CellLeft + .CellWidth - pPic.Width, .Top + .CellTop, _
            pPic.Width, _
            .CellHeight - pFrm.ScaleY(1, vbPixels, vbTwips)
    End With
End Sub

Public Sub MostrarEditorTexto(pFrm As Form, pGrd As Object, ByRef txtEditor As Object, ByRef CellRow As Long, ByRef cellCol As Long, pKeyascii As Integer, Optional pNumerico As Boolean = False, Optional pAutotecla As Boolean = True)
    
    Dim mEscribio As Boolean
    
    If txtEditor.Visible Then Exit Sub
    txtEditor.Alignment = IIf(pNumerico, 1, 0)
    
    With pGrd

        .RowSel = .Row
        .ColSel = .Col
        A = .Left + .CellLeft
        B = .Top + .CellTop
        txtEditor.Move .Left + .CellLeft, .Top + .CellTop, _
                        .CellWidth - pFrm.ScaleX(1, vbPixels, vbTwips), _
                        .CellHeight - pFrm.ScaleY(1, vbPixels, vbTwips)
        txtEditor.Font = .Font
        txtEditor.Text = .Text
        
        If pAutotecla Then
            If pNumerico Then
                If IsNumeric(Chr(pKeyascii)) Then
                    txtEditor.Text = Chr(pKeyascii)
                    mEscribio = True
                End If
            Else
                If pKeyascii > 32 Then
                    txtEditor.Text = Chr(pKeyascii)
                    mEscribio = True
                End If
            End If
        End If
        
        If txtEditor.Text = vbNullString Then txtEditor.Text = "*"
        
        txtEditor.Visible = True
        txtEditor.ZOrder
        
        If PuedeObtenerFoco(txtEditor) Then txtEditor.SetFocus
        
        If mEscribio Then
            txtEditor.SelStart = Len(txtEditor)
        Else
            txtEditor.SelStart = 0
            txtEditor.SelLength = Len(txtEditor)
        End If
        
        CellRow = .Row
        cellCol = .Col
        
    End With
    
End Sub

Public Function OcultarEditorTexto(ByRef pGrd As Object, ByRef pTxt As Object, pRow As Long, pCol As Long, Optional pNumerico As Boolean = False, Optional Cancelar As Boolean = False)
    
    If Not Cancelar Then
        If pNumerico Then
            If Not IsNumeric(pTxt.Text) Then
                OcultarEditorTexto pGrd, pTxt, pRow, pCol, , True
                Exit Function
            End If
        End If
        OcultarEditorTexto = pTxt.Text
    
    Else
        If pNumerico Then
            pTxt.Text = Val(pGrd.TextMatrix(pRow, pCol))
        Else
            pTxt.Text = pGrd.TextMatrix(pRow, pCol)
        End If
            
    End If
    pTxt.Visible = False
    If pGrd.Visible And pGrd.Enabled Then pGrd.SetFocus
    
    
    

End Function

Public Sub MostrarEditorCombo(pFrm As Object, pGrd As Object, ByRef pCbo As Object, ByRef CellRow As Long, ByRef cellCol As Long, Optional pTopFrame As Long = 0)
    If pCbo.Visible Then Exit Sub
    With pGrd

        .RowSel = .Row
        .ColSel = .Col
        pCbo.Move .Left + .CellLeft, .Top + .CellTop, _
                        .CellWidth - pFrm.ScaleX(1, vbPixels, vbTwips)
        'If TypeOf pCbo Is ComboBox Then pCbo.ListIndex = 0
        pCbo.Visible = True
        pCbo.ZOrder
        pCbo.SetFocus
        CellRow = .Row
        cellCol = .Col

     End With

End Sub

Public Function OcultarEditorCombo(ByRef pGrd As Object, ByRef pCbo As Object, pRow As Long, pCol As Long, Optional Cancelar As Boolean = False) As String
    Dim mAux As Variant
    
    If pCbo.Visible Then
        If Not Cancelar Then
            If TypeOf pCbo Is ComboBox Then
                mAux = pCbo.List(pCbo.ListIndex)
            Else
                mAux = pCbo.Value
            End If
        End If
    End If
    pCbo.Visible = False
    OcultarEditorCombo = mAux
    If pGrd.Enabled And pGrd.Visible Then pGrd.SetFocus
    
    
End Function

Public Sub AsignarValorCelda(ByRef pGrd As Object, pCol As Long, pRow As Long, pValor, Optional pFormato)
    On Error GoTo Errores
    If IsMissing(pFormato) Then
        pGrd.TextMatrix(pRow, pCol) = pValor
    Else
        pGrd.TextMatrix(pRow, pCol) = Format(pValor, pFormato)
    End If
Errores:
    Err.Clear
End Sub

Public Function TomarValorCelda(ByRef pGrd As Object, pCol As Long, pRow As Long, pValor)
    On Error GoTo Errores
    TomarValorCelda = pGrd.TextMatrix(pRow, pCol)
    
Errores:
    Err.Clear
End Function

Public Sub SeleccionarCelda(ByRef pGrd As Object, pFila, pCol)
    If Not ExisteFilaSeleccionada(pFila) Then
        pGrd.Col = pCol: pGrd.Row = pFila
        pGrd.CellBackColor = pGrd.BackColorSel
        pGrd.CellForeColor = pGrd.ForeColorSel
        mvarSelectionCells.Add pFila & ":" & pCol
    End If
    
End Sub

Private Function ExisteFilaSeleccionada(pFila) As Boolean
    For I = 1 To mvarSelectionCells.Count
        mPos = Split(mvarSelectionCells(I), ":")
        If pFila = mPos(0) Then
            ExisteFilaSeleccionada = True
            Exit Function
        End If
    Next I
End Function

Public Sub DesSeleccionarCeldas(pGrd As Object)
    Dim mColor As Long
    
    For I = 1 To mvarSelectionCells.Count
        mPos = Split(mvarSelectionCells(I), ":")
        'If mColor = 0 Then
        pGrd.Col = 0: pGrd.Row = mPos(0)
        mColor = pGrd.CellBackColor
        'End If
        pGrd.Col = mPos(1): pGrd.Row = mPos(0)
        pGrd.CellBackColor = mColor
        pGrd.CellForeColor = pGrd.ForeColor
        
    Next I
    Set mvarSelectionCells = New Collection
End Sub

Public Sub AgregarCeldasSeleccionadas(ByRef pGrd As Object, pFila, pCol, pPrimeraFila)
    Dim mFila As Long
    
    For mFila = pPrimeraFila To pFila Step IIf(pPrimeraFila > pFila, -1, 1)
        SeleccionarCelda pGrd, mFila, pCol
    Next mFila

End Sub

Public Sub Buscador(pGrd As Object, pFila As Long, Optional pSoloCol As Integer = -1)
    Set mGrid = pGrd
    mvarSoloCol = pSoloCol
    mvarUltimoTexto = ""
    mvarFilaActual = pFila
    mvarUltimaFilaSel = -1
   ' mvarColorUltFilaSel = -1
    Set frmBuscador.fCls = Me
    frmBuscador.Show vbModal
    If mvarUltimaFilaSel <> -1 Then
        PintarFila mvarUltimaFilaSel, False
    End If
End Sub

Private Sub PintarFila(pFila As Long, Optional pColorSel As Boolean = True)
    Dim mCol As Long
    

    For mCol = 0 To mGrid.Cols - 1
        mGrid.Col = mCol
        mGrid.CellBackColor = IIf(pColorSel, mGrid.BackColorSel, mvarColorUltFilaSel) 'mGrid.BackColor
        mGrid.CellForeColor = IIf(pColorSel, mGrid.ForeColorSel, mGrid.ForeColor)
    Next

End Sub

Private Sub SeleccionarFila(pFila As Long)
    mGrid.Row = pFila
    mGrid.TopRow = pFila
    mvarUltimaFilaSel = pFila
    mvarColorUltFilaSel = mGrid.CellBackColor
    PintarFila mvarUltimaFilaSel

End Sub

Public Sub BuscarTexto(pTexto As String)
    Dim mIni As Long, mFila As Long
    Dim mColIni As Integer, mColFin As Integer
    
    mColIni = IIf(mvarSoloCol >= 0, mvarSoloCol, 0)
    mColFin = IIf(mvarSoloCol >= 0, mvarSoloCol, mGrid.Cols - 1)
    mIni = BuscarFilaIni(pTexto)
    For mFila = mIni To mGrid.Rows - 1
        For mCol = mColIni To mColFin
            If mGrid.ColWidth(mCol) > 0 Then
                If InStr(1, mGrid.TextMatrix(mFila, mCol), pTexto, vbTextCompare) > 0 Or EsMonto(pTexto, mGrid.TextMatrix(mFila, mCol)) Then
                    SeleccionarFila mFila
                    Exit Sub
                End If
            End If
        Next mCol
    Next mFila
    mvarUltimaFilaSel = -1
    gMensajes.MensajeSistema "No se Encontro la Cadena Buscada", msgInformacion, "Busqueda"
    
    

End Sub

Private Function BuscarFilaIni(pTexto As String) As Long
    If mvarUltimaFilaSel <> -1 Then PintarFila mvarUltimaFilaSel, False
    If Trim(UCase(pTexto)) <> Trim(UCase(mvarUltimoTexto)) Then
        mvarUltimoTexto = Trim(UCase(pTexto))
        mvarUltimaFilaSel = -1
    End If
    BuscarFilaIni = IIf(mvarUltimaFilaSel <> -1, mvarUltimaFilaSel, 0) + 1
    
End Function

Private Function EsMonto(pTexto, pMonto) As Boolean
    If IsNumeric(pTexto) And IsNumeric(pMonto) Then
        EsMonto = Fix(Abs(CDbl(pTexto) - CDbl(pMonto))) <= 1
    End If
End Function

Friend Sub PintarFilaGrid(pGrd As Object, pFila As Long, pColor, Optional pCol As Long = 0)
    Dim mCol As Long
    

    For mCol = pGrd.Cols - 1 To 0 Step -1
        pGrd.Col = mCol
        pGrd.Row = pFila
        pGrd.CellBackColor = pColor
        
    Next
    If pCol <> 0 Then pGrd.Col = pCol
End Sub
