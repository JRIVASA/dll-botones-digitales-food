VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsItmGenerico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mvarCodigo                                  As String
Private mvarDescripcion                             As String

Property Get Codigo() As String
    Codigo = mvarCodigo
End Property

Property Get Descripcion() As String
    Descripcion = mvarDescripcion
End Property

Property Let Codigo(ByVal Valor As String)
    mvarCodigo = Valor
End Property

Property Let Descripcion(ByVal Valor As String)
    mvarDescripcion = Valor
End Property

Public Sub NuevoItmGenerico(Codigo As String, Descripcion As String)
    mvarCodigo = Codigo
    mvarDescripcion = Descripcion
End Sub
