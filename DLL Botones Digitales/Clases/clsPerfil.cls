VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPerfil"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mvarPerfil                                                  As String
Private mvarDescripcion                                             As String
Private mvarHoraIni                                                 As Date
Private mvarHoraFin                                                 As Date
Private mvarDias                                                    As Integer
Private mBotones                                                    As clsBotones

Friend Property Let Descripcion(ByVal vData As String)
    mvarDescripcion = vData
End Property

Friend Property Get Descripcion() As String
    Descripcion = mvarDescripcion
End Property

Friend Property Let Perfil(ByVal vData As String)
    mvarPerfil = vData
End Property

Friend Property Get Perfil() As String
    Perfil = mvarPerfil
End Property

Friend Property Let HoraIni(ByVal vData As Date)
    mvarHoraIni = vData
End Property

Friend Property Get HoraIni() As Date
    HoraIni = mvarHoraIni
End Property

Friend Property Let HoraFin(ByVal vData As Date)
    mvarHoraFin = vData
End Property

Friend Property Get HoraFin() As Date
    HoraFin = mvarHoraFin
End Property

Friend Property Let Dias(ByVal vData As Integer)
    mvarDias = vData
End Property

Friend Property Get Dias() As Integer
    Dias = mvarDias
End Property

Property Get Botones() As clsBotones
    Set Botones = mBotones
End Property

Property Set Botones(Btnes As clsBotones)
    Set mBotones = Btnes
End Property

Private Sub Class_Initialize()
    Set mBotones = New clsBotones
End Sub

Private Sub Class_Terminate()
    Set mBotones = Nothing
End Sub
