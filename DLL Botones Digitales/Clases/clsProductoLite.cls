VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsProductoLite"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mvarCodigo                                      As String
Private mvarDescripcion                                 As String
Private mvarBarra                                       As String
Private mvarMarca                                       As String
Private mvarDecimales                                   As Integer
Private mvarPresentacion                                As Single
Private mvarImpuesto                                    As Single
Private mvarPrecio                                      As Currency
Private mvarPrecioO                                     As Currency
Private mvarCosto                                       As Currency
Private mvarFechaI                                      As Date
Private mvarFechaF                                      As Date
Private mvarTipo                                        As eTipoProducto
Private mDepartamento                                   As clsItmGenerico
Private mGrupo                                          As clsItmGenerico
Private mSubGrupo                                       As clsItmGenerico

'************************************ Property Gets *******************************************
Property Get Codigo() As String
    Codigo = mvarCodigo
End Property

Property Get Descripcion() As String
    Descripcion = mvarDescripcion
End Property

Property Get Barra() As String
    Barra = mvarBarra
End Property

Property Get Marca() As String
    Marca = mvarMarca
End Property

Property Get Decimales() As Integer
    Decimales = mvarDecimales
End Property

Property Get Presentacion() As Single
    Presentacion = mvarPresentacion
End Property

Property Get Impuesto() As Single
    Impuesto = mvarImpuesto
End Property

Property Get Costo() As Currency
    Costo = mvarCosto
End Property

Property Get Precio() As Currency
    Precio = mvarPrecio
End Property

Property Get PrecioOferta() As Currency
    PrecioOferta = mvarPrecioO
End Property

Property Get FechaIni() As Date
    FechaIni = mvarFechaI
End Property

Property Get FechaFin() As Date
    FechaFin = mvarFechaF
End Property

Property Get Tipo() As eTipoProducto
    Tipo = mvarTipo
End Property

Property Get Departamento() As clsItmGenerico
    Set Departamento = mDepartamento
End Property

Property Get Grupo() As clsItmGenerico
    Set Grupo = mGrupo
End Property

Property Get SubGrupo() As clsItmGenerico
    Set SubGrupo = mSubGrupo
End Property



'************************************ Property lets *******************************************
Property Let Codigo(ByVal Valor As String)
    mvarCodigo = Valor
End Property

Property Let Descripcion(ByVal Valor As String)
    mvarDescripcion = Valor
End Property

Property Let Barra(ByVal Valor As String)
    mvarBarra = Valor
End Property

Property Let Marca(ByVal Valor As String)
    mvarMarca = Valor
End Property

Property Let Decimales(ByVal Valor As Integer)
    mvarDecimales = Valor
End Property

Property Let Presentacion(ByVal Valor As Single)
    mvarPresentacion = Valor
End Property

Property Let Impuesto(ByVal Valor As Single)
    mvarImpuesto = Valor
End Property

Property Let Costo(ByVal Valor As Currency)
    mvarCosto = Valor
End Property

Property Let Precio(ByVal Valor As Currency)
    mvarPrecio = Valor
End Property

Property Let PrecioOferta(ByVal Valor As Currency)
    mvarPrecioO = Valor
End Property

Property Let FechaIni(ByVal Valor As Date)
    mvarFechaI = Valor
End Property

Property Let FechaFin(ByVal Valor As Date)
    mvarFechaF = Valor
End Property

Property Let Tipo(ByVal Valor As eTipoProducto)
    mvarTipo = Valor
End Property

Private Sub Class_Initialize()
    Set mDepartamento = New clsItmGenerico
    Set mGrupo = New clsItmGenerico
    Set mSubGrupo = New clsItmGenerico
End Sub

Private Sub Class_Terminate()
    Set mDepartamento = Nothing
    Set mGrupo = Nothing
    Set mSubGrupo = Nothing
End Sub
