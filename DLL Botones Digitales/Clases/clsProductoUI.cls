VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsProductoUI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Enum eTipoProducto
    etpUnidad
    etpPesado
    etpPesable
    etpExtendido
    etpInformativo
    etpCompuesto
End Enum

Private mClsCn                                          As clsConexion
Private mProducto                                       As clsProductoLite
Private mvarAutoCodigo                                  As Boolean

Property Get ClsCn() As clsConexion
    Set ClsCn = mClsCn
End Property

Property Get Producto() As clsProductoLite
    Set Producto = mProducto
End Property

Property Get AutoCodigo() As Boolean
    AutoCodigo = mvarAutoCodigo
End Property

Public Sub IniciarProducto()
    Set mProducto = New clsProductoLite
End Sub

Public Sub IniciarInterfaz(Cn As clsConexion, Optional Mostrar As Boolean = True)
    
    Dim mTmp As Variant
    
    Set mClsCn = Cn
    
    If Mostrar Then
        mTmp = BuscarReglaComercial("Auto_Codigo")
        If Not IsEmpty(mTmp) Then mvarAutoCodigo = mTmp
        
        Set frmProductosLite.fCls = Me
        frmProductosLite.Show vbModal
        Set frmProductosLite = Nothing
    End If
    
End Sub

Public Function BuscarProducto() As Boolean
    Dim mBus As clsBusqueda
    
    If mClsCn.ConectarBD Then
        Set mBus = New clsBusqueda
        Set mBus.ConexionBusqueda = mClsCn.Conexion
        mBus.TextoComandoSql = "Select distinct P.c_codigo,P.c_Decri,'' AS BARRA from MA_PRODUCTOS P INNER JOIN MA_CODIGOS C ON C.C_CODNASA=P.C_CODIGO"
        mBus.AgregarCamposBusqueda "Codigo", "p.c_codigo", "c_codigo", 1500
        mBus.AgregarCamposBusqueda "Codigo", "p.c_descri", "c_descri", 4500, True, opCampoStr
        mBus.AgregarCamposBusqueda "Barra", "c.c_codigo", "", 0, True, opCampoStr, True
        mBus.MostrarInterfazBusqueda "P R O D U C T O S", , , , , "%"
        If Not IsEmpty(mBus.ResultadoBusqueda) Then
            BuscarProducto = BuscarDatosProducto(mBus.ResultadoBusqueda(0)(0))
        End If
    End If
End Function

Public Function BuscarDatosProducto(Codigo) As Boolean
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    
    On Error GoTo Errores
    If mClsCn.ConectarBD Then
        mSQl = "Select * from ma_productos where C_codigo='" & Codigo & "' "
        Set mRs = New ADODB.Recordset
        mRs.Open mSQl, mClsCn.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
        If Not mRs.EOF Then
            IniciarProducto
            With mProducto
                '.Barra = BuscarBarra(mRs!c_codigo) ' NO COMPILA
                .Codigo = mRs!c_codigo
                .Descripcion = mRs!c_descri
                .Decimales = mRs!n_cant_decimales
                .Departamento.Codigo = mRs!c_departamento
                .Grupo.Codigo = mRs!c_grupo
                .SubGrupo.Codigo = mRs!c_subgrupo
                .FechaFin = FormatDateTime(mRs!f_final, vbShortDate) & " " & FormatDateTime(mRs!H_final, vbShortTime)
                .FechaIni = FormatDateTime(mRs!f_inicial, vbShortDate) & " " & FormatDateTime(mRs!H_inicial, vbShortTime)
                .Impuesto = mRs!n_impuesto1
                .Precio = mRs!n_precio1
                .PrecioOferta = mRs!n_precioo
                .Marca = mRs!c_marca
                .Tipo = mRs!n_tipopeso
                .Presentacion = mRs!n_cantibul
                BuscarDatosProducto = True
            End With
        End If
    End If
    Exit Function
Errores:
    Mensaje Err.Description, False
End Function

Public Function Mensaje(Msg As String, Cancel As Boolean) As Boolean
    Dim mMsg As clsMensajeria
    
    Set mMsg = New clsMensajeria
    Mensaje = mMsg.Mensaje(Msg, Cancel)
    Set mMsg = Nothing
End Function

'************************************ METODOS PRIVADOS ****************************************

Private Function BuscarReglaComercial(Campo As String)
    
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    
    mSQl = "Select " & Campo & " from Reglas_Comerciales"
    Set mRs = New ADODB.Recordset
    
    If mClsCn.ConectarBD Then
        mRs.Open mSQl, mClsCn.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
        If Not mRs.EOF Then
            BuscarReglaComercial = mRs.Fields(0).Value
            Exit Function
        End If
    End If
    
End Function
