VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls_CostoxImpuesto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mFlagFormCostoLoad As Boolean
Private valor_antiguo As Double

Property Let FlagFormCostoLoad(pValor As Boolean)
    mFlagFormCostoLoad = pValor
End Property

Property Get FlagFormCostoLoad() As Boolean
    FlagFormCostoLoad = mFlagFormCostoLoad
End Property

Public Function Cargar_FormCostoxImpuesto(pValor As String) As Double
    Dim mForma As Object
    
    Set mForma = New frm_CostoxImpuesto
    mForma.montoinicial = CDbl(pValor)
    FlagFormCostoLoad = True
    mForma.Show vbModal
    Cargar_FormCostoxImpuesto = mForma.resultado
    Set mForma = Nothing
    FlagFormCostoLoad = False
End Function
