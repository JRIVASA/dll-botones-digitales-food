VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls_grupos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mVarGrupo                                                               As String
Private mvarTipoGrupo                                                           As String
Private mvarGrabo                                                               As Boolean

Property Let cGrupo(pValor As String)
    mVarGrupo = pValor
End Property

Property Get cGrupo() As String
    cGrupo = mVarGrupo
End Property

Property Let cTipoGrupo(pValor As String)
    mvarTipoGrupo = pValor
End Property

Property Get cTipoGrupo() As String
    cTipoGrupo = mvarTipoGrupo
End Property

Property Let GraboGrupo(pValor As Boolean)
    mvarGrabo = pValor
End Property

Property Get GraboGrupo() As Boolean
    GraboGrupo = mvarGrabo
End Property

Public Function CargarGrupos(pCn, ByRef pLv As ListView) As Boolean
    
    Dim mRs As New ADODB.Recordset
    Dim mSQl As String, mItm As ListItem
    
    On Error GoTo Errores
    
    pLv.ListItems.Clear
    
    mSQl = "Select * from ma_aux_grupo where cs_tipo='" & Me.cTipoGrupo & "' order by id"
    
    mRs.Open mSQl, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Do While Not mRs.EOF
        Set mItm = pLv.ListItems.Add(, , mRs!cs_grupo)
        mRs.MoveNext
    Loop
    
    mRs.Close
    
    CargarGrupos = True
    
    Exit Function
    
Errores:
    
    MsgBox Err.Description
    Err.Clear
    
End Function

Public Sub CargarComboGrupos(pCn, pCbo As ComboBox, Optional pPermitirAgregar As Boolean = True)
    
    Dim mRs As New ADODB.Recordset
    Dim mSQl As String
    
    pCbo.Clear
    pCbo.AddItem ""
    
    mSQl = "Select DISTINCT CS_GRUPO from (Select case cs_grupo when 'Ninguno' then 'a'+cs_grupo else 'z'+cs_grupo end as cs_grupo from ma_aux_grupo where cs_tipo IN ('SYS','" & cTipoGrupo & "'))tb order by cs_grupo"
    mRs.Open mSQl, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Do While Not mRs.EOF
        pCbo.AddItem Mid(mRs!cs_grupo, 2, Len(mRs!cs_grupo))
        mRs.MoveNext
    Loop
    
    pCbo.AddItem "Ninguno"
    
    If pPermitirAgregar Then pCbo.AddItem "Nuevo Grupo"
    
    pCbo.ListIndex = 0
    
End Sub

Public Sub BuscarGruposCbo(ByRef pCbo As ComboBox, pTexto As String)
    
    Dim mItm As Long
    
    For mItm = 0 To pCbo.ListCount - 1
        If (pTexto = "" And Trim(UCase(pCbo.List(mItm))) = "NINGUNO") Or Trim(UCase(pCbo.List(mItm))) = Trim(UCase(pTexto)) Then
            pCbo.ListIndex = mItm
            Exit Sub
        End If
    Next mItm
    
End Sub

Public Sub AgregarModificarGrupo(pCn, ByRef pCbo As ComboBox)
    
    Dim mPosAnterior As Long
    
    mPosAnterior = pCbo.ListIndex
    
    Set frm_GruposSeleccion.mClsGrupos = Me
    
    frm_GruposSeleccion.Show vbModal
    
    If GraboGrupo Then
        CargarComboGrupos pCn, pCbo
        pCbo.ListIndex = 0
    End If
    
End Sub

Public Function GrabarModificarGrupo(pCn, pTexto As String, Optional pAnterior As String = "") As Boolean
    Dim mSQl As String
    
    On Error GoTo Errores
    If pAnterior <> "" Then
        pCn.Execute = "Delete from ma_aux_grupo where cs_grupo='" & pAnterior & "' and cs_tipo='" & Me.cTipoGrupo & "' "
    End If
    mSQl = "Insert into ma_aux_grupo (cs_grupo,cs_tipo) values ('" & pTexto & "','" & Me.cTipoGrupo & "') "
    pCn.Execute mSQl
    GrabarModificarGrupo = True
Errores:
    Err.Clear
    
End Function

Public Sub EliminarGrupos(pCn, pGrupo As String)
    Dim mTabla As String, mValor As String
    Dim mSujeto As String, mCampo As String
    mValor = "''"
    Select Case Me.cTipoGrupo
        Case "CLI"
            mTabla = "MA_CLIENTES"
            mSujeto = "Clientes"
            mCampo = "c_grupo"
            
        Case "UNI"
            mTabla = "MA_UNIDADES"
            mSujeto = "Unidades"
            mCampo = "cs_grupo"
        Case "PROV"
            mTabla = "MA_PROVEEDORES"
            mSujeto = "Proveedores"
            mCampo = "c_grupo"
        Case "BAN"
            mTabla = "MA_BANCOS"
            mSujeto = "Bancos"
            mCampo = "c_grupo"
        Case "IMP"
            mTabla = "MA_PRODUCTOS"
            mSujeto = "PRODUCTOS"
            mCampo = "N_IMPUESTO1"
            mValor = "0"
    End Select
    
    If UCase(pGrupo) <> "NINGUNO" Or (mTabla = "MA_PRODUCTOS" And UCase(pGrupo) <> "0") Then
        If Not ExisteGrupoSel(pCn, pGrupo, mTabla) Then
            'If mTabla <> "MA_PRODUCTOS" Then
                'mResp = MsgBox("Algunos (" & mSujeto & ") estan relacionados con el Grupo & " & pGrupo & ", Desea Eliminar esta Relacion? ", vbYesNo)
                'If mResp = vbNo Then Exit Sub
                'pCn.Execute "Update from " & mTabla & " set " & mcampo & "=" & mValor & "  where " & mcampo & "='" & pGrupo & "' "
            'Else
                'MsgBox "Se Eliminara El Impuesto del Grupo. Para Cambiar los Impuestos debera dirigirse al Modulo Tributario del Sistema ", vbInformation
            'End If
            pCn.Execute "Delete from ma_aux_grupo where cs_grupo='" & pGrupo & "' and cs_tipo='" & Me.cTipoGrupo & "' "
        Else
            MsgBox "Algunos (" & mSujeto & ") estan relacionados con el Grupo " & pGrupo, vbInformation
        End If
    End If
            
End Sub

Private Function ExisteGrupoSel(pCn, pGrupo As String, pTabla) As Boolean
    Dim mRs As New ADODB.Recordset
    
    If pTabla = "" Then pTabla = "Ma_AUX_GRUPO"
    mRs.Open "Select c_grupo from " & pTabla & " where c_grupo='" & pGrupo & "' group by c_grupo ", pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    ExisteGrupoSel = Not mRs.EOF
    mRs.Close
    
End Function
