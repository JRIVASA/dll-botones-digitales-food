VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "obj_listview"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Sub AdicionarLw(ByRef lw As ListView, Valores As Variant)
    
    Dim ItmX As ListItem
    
    Set ItmX = lw.ListItems.Add(, , CStr(IIf(IsNull(Valores(0)), "", Valores(0))))
    
    Dim I
    
    LonValores = UBound(Valores)
    
    If LonValores > 0 Then
        For I = 1 To LonValores
            ItmX.SubItems(I) = IIf(IsNull(Valores(I)), "", Valores(I))
        Next
    End If
    
End Sub
   
Sub BorrarLw(ByRef lw As ListView, ByVal POS As Long)
    lw.ListItems.Remove (POS)
End Sub

Sub ActualizarLw(ByRef lw As ListView, ByVal POS As Long, Valores As Variant)
    
    Dim ItmX As ListItem
    
    lw.ListItems(POS).Text = Valores(0)
    
    Set ItmX = lw.ListItems(POS)
    
    LonValores = UBound(Valores)
    
    If LonValores > 0 Then
        For I = 1 To LonValores
            ItmX.SubItems(I) = Valores(I)
        Next
    End If
    
End Sub

Function TomarDataLw(ByRef lw As ListView, ByVal POS As Long) As Variant
    
    Dim ItmX As ListItem
    Dim LonCol As Integer
    Dim Tmp()
    
    LonCol = lw.ColumnHeaders.Count
    
    ReDim Tmp(LonCol - 1)
    
    Set ItmX = lw.ListItems(POS)
    
    Tmp(0) = lw.ListItems(POS)
    
    If LonCol > 1 Then
        For I = 1 To LonCol - 1
            Tmp(I) = ItmX.SubItems(I)
        Next
    End If
    
    TomarDataLw = Tmp
    
End Function
