VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Begin VB.Form Ficha_ProductosLite 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8505
   ClientLeft      =   810
   ClientTop       =   1575
   ClientWidth     =   15330
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Ficha_ProductosLite.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8505
   ScaleWidth      =   15330
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   115
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   13150
         TabIndex        =   117
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   116
         Top             =   75
         Width           =   5295
      End
   End
   Begin VB.PictureBox CoolBar1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   0
      ScaleHeight     =   1050
      ScaleWidth      =   15330
      TabIndex        =   84
      Top             =   420
      Width           =   15360
      Begin MSComDlg.CommonDialog caja 
         Left            =   14700
         Top             =   45
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.Frame frame_toolbar 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   810
         Left            =   30
         TabIndex        =   85
         Top             =   140
         Width           =   15240
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   810
            Left            =   120
            TabIndex        =   86
            Top             =   0
            Width           =   10575
            _ExtentX        =   18653
            _ExtentY        =   1429
            ButtonWidth     =   2408
            ButtonHeight    =   1429
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "Iconos_Encendidos"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   10
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "(F3) Agr&egar"
                  Key             =   "Agregar"
                  Object.ToolTipText     =   "Agregar una Nueva Ficha"
                  ImageIndex      =   2
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   3
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "APRODUCTO"
                        Object.Tag             =   "APRODUCTO"
                        Text            =   "Agregar"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "AIMPORTAR"
                        Object.Tag             =   "AIMPORTAR"
                        Text            =   "Agregar Importar "
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "imparchivo"
                        Text            =   "Importar Archivo"
                     EndProperty
                  EndProperty
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "(F2) &Buscar"
                  Key             =   "Buscar"
                  Object.ToolTipText     =   "Buscar una Ficha"
                  ImageIndex      =   6
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "(F5) &Modificar"
                  Key             =   "Modificar"
                  Object.ToolTipText     =   "Modificar esta Ficha"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "(F7) &Cancelar"
                  Key             =   "Cancelar"
                  Object.ToolTipText     =   "Cancelar esta Ficha"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "(F6) Bo&rrar"
                  Key             =   "Eliminar"
                  Object.ToolTipText     =   "Borrar esta Ficha"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "(F4) &Grabar"
                  Key             =   "Grabar"
                  Object.ToolTipText     =   "Grabar esta Ficha"
                  ImageIndex      =   7
               EndProperty
               BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "(F12) &Salir"
                  Key             =   "Salir"
                  Object.ToolTipText     =   "Salir de Ficheros"
                  ImageIndex      =   5
               EndProperty
               BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Object.Visible         =   0   'False
                  Caption         =   "&Ayuda"
                  Key             =   "Ayuda"
                  Object.ToolTipText     =   "Ayuda"
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComDlg.CommonDialog dialogo 
      Left            =   14880
      Top             =   1320
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      DialogTitle     =   "Buscar Imagenes"
   End
   Begin TabDlg.SSTab Sstab1 
      Height          =   6900
      Left            =   15
      TabIndex        =   33
      Top             =   1575
      Width           =   15300
      _ExtentX        =   26988
      _ExtentY        =   12171
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   5
      TabHeight       =   520
      ForeColor       =   5790296
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "&General"
      TabPicture(0)   =   "Ficha_ProductosLite.frx":628A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "f_general"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "FrmDepGruSub"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Command2"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "f_costos"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "f_precios"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "f_Impuestos"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).ControlCount=   6
      TabCaption(1)   =   "&Detalle"
      TabPicture(1)   =   "Ficha_ProductosLite.frx":62A6
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame9"
      Tab(1).Control(1)=   "Frame10"
      Tab(1).Control(2)=   "Frame4"
      Tab(1).Control(3)=   "f_observacion"
      Tab(1).Control(4)=   "Frame7"
      Tab(1).Control(5)=   "Frame2"
      Tab(1).Control(6)=   "Frame1"
      Tab(1).ControlCount=   7
      Begin VB.Frame Frame9 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   1080
         Left            =   -74760
         TabIndex        =   159
         Top             =   8520
         Visible         =   0   'False
         Width           =   14865
         Begin VB.TextBox Volumen 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   12645
            MaxLength       =   30
            TabIndex        =   165
            Text            =   "0.000"
            Top             =   525
            Width           =   1800
         End
         Begin VB.TextBox Peso 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   7440
            MaxLength       =   30
            TabIndex        =   164
            Text            =   "0.000"
            Top             =   525
            Width           =   1800
         End
         Begin MSDataListLib.DataCombo Presentacion 
            Bindings        =   "Ficha_ProductosLite.frx":62C2
            CausesValidation=   0   'False
            Height          =   390
            Left            =   1920
            TabIndex        =   162
            Top             =   525
            Width           =   2385
            _ExtentX        =   4207
            _ExtentY        =   688
            _Version        =   393216
            Appearance      =   0
            BackColor       =   16777215
            ForeColor       =   5790296
            ListField       =   "c_PRESENTACIO"
            Text            =   ""
            Object.DataMember      =   "Presentacion"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Line Line4 
            BorderColor     =   &H00AE5B00&
            X1              =   1920
            X2              =   14640
            Y1              =   240
            Y2              =   240
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Datos de la Unidad"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00AE5B00&
            Height          =   240
            Index           =   70
            Left            =   120
            TabIndex        =   169
            Top             =   120
            Width           =   1605
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Peso de la Unidad"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   10
            Left            =   5640
            TabIndex        =   167
            Top             =   600
            Width           =   1530
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Volumen de la Unidad"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   12
            Left            =   10485
            TabIndex        =   166
            Top             =   600
            Width           =   1875
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Unidad de Medida"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   29
            Left            =   120
            TabIndex        =   163
            Top             =   600
            Width           =   1530
         End
      End
      Begin VB.Frame Frame10 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   1200
         Left            =   -74760
         TabIndex        =   152
         Top             =   480
         Width           =   14865
         Begin VB.TextBox Decimales 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   12645
            MaxLength       =   5
            TabIndex        =   160
            Text            =   "0"
            Top             =   525
            Width           =   1800
         End
         Begin VB.ComboBox Procedencia 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            ItemData        =   "Ficha_ProductosLite.frx":62D4
            Left            =   1320
            List            =   "Ficha_ProductosLite.frx":62DE
            Style           =   2  'Dropdown List
            TabIndex        =   155
            Top             =   525
            Width           =   2385
         End
         Begin VB.TextBox txt_StockMax 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   8565
            TabIndex        =   154
            Text            =   "0"
            Top             =   525
            Width           =   1800
         End
         Begin VB.TextBox txt_StockMin 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   5280
            TabIndex        =   153
            Text            =   "0"
            Top             =   525
            Width           =   1800
         End
         Begin VB.Line Line5 
            BorderColor     =   &H00AE5B00&
            X1              =   1920
            X2              =   14640
            Y1              =   240
            Y2              =   240
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Datos del Producto"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00AE5B00&
            Height          =   240
            Index           =   71
            Left            =   120
            TabIndex        =   170
            Top             =   120
            Width           =   1605
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "N�mero de Decimales"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   27
            Left            =   10560
            TabIndex        =   161
            Top             =   600
            Width           =   1875
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Procedencia"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Index           =   64
            Left            =   120
            TabIndex        =   158
            Top             =   600
            Width           =   1275
         End
         Begin VB.Label Label6 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Stock M�ximo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   7245
            TabIndex        =   157
            Top             =   600
            Width           =   1185
         End
         Begin VB.Label Label7 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Stock M�nimo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   3960
            TabIndex        =   156
            Top             =   600
            Width           =   1140
         End
      End
      Begin VB.Frame Frame4 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Caption         =   "Procedencia"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   1200
         Left            =   -74760
         TabIndex        =   126
         Top             =   9000
         Visible         =   0   'False
         Width           =   14865
         Begin VB.CheckBox chk_Teclado 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00E7E8E8&
            Caption         =   "Permite Teclado"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   12480
            TabIndex        =   145
            Top             =   720
            Width           =   2085
         End
         Begin VB.CheckBox chk_Cantidad 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00E7E8E8&
            Caption         =   "Permite Cantidad"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   10080
            TabIndex        =   144
            Top             =   720
            Width           =   2085
         End
         Begin VB.ComboBox cmb_NivelProducto 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   390
            ItemData        =   "Ficha_ProductosLite.frx":62F7
            Left            =   4080
            List            =   "Ficha_ProductosLite.frx":630D
            Style           =   2  'Dropdown List
            TabIndex        =   142
            Top             =   700
            Width           =   2385
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00AE5B00&
            X1              =   3240
            X2              =   14655
            Y1              =   240
            Y2              =   240
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Configuraci�n para Punto de Venta"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00AE5B00&
            Height          =   240
            Index           =   67
            Left            =   120
            TabIndex        =   150
            Top             =   120
            Width           =   2985
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Nivel m�nimo de usuario para venta en POS"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   65
            Left            =   120
            TabIndex        =   143
            Top             =   720
            Width           =   3720
         End
      End
      Begin VB.Frame f_observacion 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Caption         =   "Observaciones"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   2295
         Left            =   -74760
         TabIndex        =   118
         Top             =   7455
         Visible         =   0   'False
         Width           =   14865
         Begin VB.TextBox Observacion 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   1620
            Left            =   120
            MultiLine       =   -1  'True
            TabIndex        =   119
            Top             =   480
            Width           =   14625
         End
         Begin VB.Line Line6 
            BorderColor     =   &H00AE5B00&
            X1              =   1320
            X2              =   14760
            Y1              =   200
            Y2              =   200
         End
         Begin VB.Label C�digo 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Observaci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00AE5B00&
            Height          =   240
            Index           =   66
            Left            =   120
            TabIndex        =   147
            Top             =   80
            Width           =   1050
         End
      End
      Begin VB.Frame Frame7 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Caption         =   "Presentaci�n  del Empaque"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   1080
         Left            =   -74760
         TabIndex        =   75
         Top             =   7800
         Visible         =   0   'False
         Width           =   14865
         Begin VB.TextBox PesoBulto 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   7440
            MaxLength       =   30
            TabIndex        =   35
            Text            =   "0.000"
            Top             =   525
            Width           =   1800
         End
         Begin VB.TextBox VolumenBulto 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   12645
            MaxLength       =   30
            TabIndex        =   36
            Text            =   "0.000"
            Top             =   525
            Width           =   1800
         End
         Begin VB.TextBox CantidadBulto 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   2280
            MaxLength       =   30
            TabIndex        =   34
            Text            =   "1"
            Top             =   525
            Width           =   2025
         End
         Begin VB.Line Line3 
            BorderColor     =   &H00AE5B00&
            X1              =   1920
            X2              =   14640
            Y1              =   240
            Y2              =   240
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Datos del Empaque"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00AE5B00&
            Height          =   240
            Index           =   69
            Left            =   120
            TabIndex        =   168
            Top             =   120
            Width           =   1650
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Peso del Empaque"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   30
            Left            =   5640
            TabIndex        =   78
            Top             =   600
            Width           =   1575
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Volumen del Empaque"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   31
            Left            =   10485
            TabIndex        =   77
            Top             =   600
            Width           =   1920
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Unidades por Empaque"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   15
            Left            =   120
            TabIndex        =   76
            Top             =   600
            Width           =   1980
         End
      End
      Begin VB.Frame Frame2 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Caption         =   "Presentaci�n Unitaria"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   105
         Left            =   -74880
         TabIndex        =   73
         Top             =   9135
         Visible         =   0   'False
         Width           =   150
         Begin VB.CheckBox Seriales 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00E7E8E8&
            Caption         =   "Administra Seriales"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   210
            Left            =   120
            TabIndex        =   146
            Top             =   960
            Visible         =   0   'False
            Width           =   2325
         End
         Begin VB.TextBox msk_factor 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   120
            MaxLength       =   10
            TabIndex        =   140
            TabStop         =   0   'False
            Top             =   345
            Visible         =   0   'False
            Width           =   1935
         End
         Begin VB.Label C�digo 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Factor"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   1
            Left            =   150
            TabIndex        =   141
            Top             =   120
            Visible         =   0   'False
            Width           =   540
         End
      End
      Begin VB.Frame Frame1 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Caption         =   "Datos Arancelarios"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   1215
         Left            =   -74760
         TabIndex        =   56
         Top             =   10320
         Visible         =   0   'False
         Width           =   14865
         Begin VB.TextBox Costo_Original 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   11220
            TabIndex        =   148
            Text            =   "0.00"
            Top             =   720
            Width           =   2100
         End
         Begin VB.TextBox imp_Arancel 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   13380
            TabIndex        =   39
            Text            =   "0.00"
            Top             =   705
            Width           =   1005
         End
         Begin VB.TextBox des_Arancel 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   4995
            TabIndex        =   38
            Top             =   705
            Width           =   6170
         End
         Begin VB.TextBox cod_Arancel 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   150
            TabIndex        =   37
            Top             =   705
            Width           =   4785
         End
         Begin VB.Line Line2 
            BorderColor     =   &H00AE5B00&
            X1              =   2160
            X2              =   14700
            Y1              =   240
            Y2              =   240
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Datos de Importaci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00AE5B00&
            Height          =   240
            Index           =   68
            Left            =   150
            TabIndex        =   151
            Top             =   120
            Width           =   1830
         End
         Begin VB.Label Label8 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Costo Original"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   11220
            TabIndex        =   149
            Top             =   420
            Width           =   1200
         End
         Begin VB.Label Label9 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H00E7E8E8&
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Left            =   14490
            TabIndex        =   60
            Top             =   750
            Width           =   225
         End
         Begin VB.Label Label4 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H00E7E8E8&
            Caption         =   "Impuesto"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   13620
            TabIndex        =   59
            Top             =   420
            Width           =   795
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   4995
            TabIndex        =   58
            Top             =   420
            Width           =   975
         End
         Begin VB.Label Label2 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "C�digo Arancelario"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   150
            TabIndex        =   57
            Top             =   420
            Width           =   1620
         End
      End
      Begin VB.Frame f_Impuestos 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Caption         =   "% Impuesto"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   3180
         Left            =   13275
         TabIndex        =   31
         Top             =   10020
         Visible         =   0   'False
         Width           =   1620
         Begin VB.TextBox Imp3 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#.##0,00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   8202
               SubFormatType   =   1
            EndProperty
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   270
            MaxLength       =   5
            TabIndex        =   17
            Text            =   "0.00"
            Top             =   1425
            Visible         =   0   'False
            Width           =   900
         End
         Begin VB.TextBox Imp2 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#.##0,00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   8202
               SubFormatType   =   1
            EndProperty
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   270
            MaxLength       =   5
            TabIndex        =   16
            Text            =   "0.00"
            Top             =   1020
            Visible         =   0   'False
            Width           =   900
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "%3"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Index           =   9
            Left            =   1245
            TabIndex        =   51
            Top             =   1470
            Visible         =   0   'False
            Width           =   345
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "%2"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Index           =   8
            Left            =   1245
            TabIndex        =   50
            Top             =   1065
            Visible         =   0   'False
            Width           =   345
         End
      End
      Begin VB.Frame f_precios 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Caption         =   "Precios"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   2460
         Left            =   360
         TabIndex        =   30
         Top             =   4020
         Width           =   10050
         Begin VB.TextBox Existencia 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#.##0,00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   8202
               SubFormatType   =   1
            EndProperty
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   350
            Left            =   7680
            MaxLength       =   18
            TabIndex        =   187
            Top             =   480
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton Cod_Alt 
            Appearance      =   0  'Flat
            Caption         =   "&C�digos Alternos"
            CausesValidation=   0   'False
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   945
            Left            =   8280
            Picture         =   "Ficha_ProductosLite.frx":634F
            Style           =   1  'Graphical
            TabIndex        =   184
            ToolTipText     =   "C�digos para este Producto"
            Top             =   1440
            Width           =   1620
         End
         Begin VB.TextBox CActual 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#.##0,00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   8202
               SubFormatType   =   1
            EndProperty
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   350
            Left            =   6375
            MaxLength       =   18
            TabIndex        =   175
            Top             =   480
            Width           =   1005
         End
         Begin VB.TextBox Imp12 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#.##0,00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   8202
               SubFormatType   =   1
            EndProperty
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   4920
            MaxLength       =   5
            TabIndex        =   174
            Text            =   "0.00"
            Top             =   480
            Visible         =   0   'False
            Width           =   900
         End
         Begin VB.ComboBox Imp1 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "Ficha_ProductosLite.frx":80D1
            Left            =   4950
            List            =   "Ficha_ProductosLite.frx":80D3
            Style           =   2  'Dropdown List
            TabIndex        =   171
            Top             =   480
            Width           =   900
         End
         Begin VB.CheckBox chk_PrecioRegulado 
            Appearance      =   0  'Flat
            BackColor       =   &H00E7E8E8&
            Caption         =   "Precio Regulado"
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   330
            Left            =   105
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   4470
            Visible         =   0   'False
            Width           =   1800
         End
         Begin MSComCtl2.DTPicker OfertaDesde 
            Height          =   345
            Left            =   795
            TabIndex        =   90
            Top             =   1905
            Width           =   2685
            _ExtentX        =   4736
            _ExtentY        =   609
            _Version        =   393216
            Enabled         =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CalendarTitleBackColor=   -2147483624
            CalendarTrailingForeColor=   -2147483624
            CustomFormat    =   "dd/MM/yyyy hh:mm:ss tt"
            Format          =   102039555
            CurrentDate     =   38106
         End
         Begin VB.TextBox Precio2 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#.##0,00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   8202
               SubFormatType   =   0
            EndProperty
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   1755
            MaxLength       =   18
            TabIndex        =   12
            Top             =   5445
            Visible         =   0   'False
            Width           =   1725
         End
         Begin VB.TextBox Precio1 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#.##0,00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   8202
               SubFormatType   =   0
            EndProperty
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   1755
            MaxLength       =   18
            TabIndex        =   11
            Top             =   480
            Width           =   1725
         End
         Begin VB.TextBox PrecioO 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#.##0,00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   8202
               SubFormatType   =   0
            EndProperty
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   1755
            Locked          =   -1  'True
            MaxLength       =   18
            TabIndex        =   14
            Top             =   975
            Width           =   1725
         End
         Begin VB.TextBox Precio3 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#.##0,00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   8202
               SubFormatType   =   0
            EndProperty
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   1755
            MaxLength       =   18
            TabIndex        =   13
            Top             =   5850
            Visible         =   0   'False
            Width           =   1725
         End
         Begin MSComCtl2.DTPicker OfertaHasta 
            Height          =   345
            Left            =   4275
            TabIndex        =   91
            Top             =   1905
            Width           =   2685
            _ExtentX        =   4736
            _ExtentY        =   609
            _Version        =   393216
            Enabled         =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CustomFormat    =   "dd/MM/yyyy hh:mm:ss tt"
            Format          =   102039555
            CurrentDate     =   38106
         End
         Begin VB.Label C�digo 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Cantidad"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   73
            Left            =   7575
            TabIndex        =   188
            Top             =   150
            Visible         =   0   'False
            Width           =   1200
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Costo Actual"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   18
            Left            =   7320
            TabIndex        =   177
            Top             =   5880
            Visible         =   0   'False
            Width           =   1065
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Costo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   40
            Left            =   6630
            TabIndex        =   176
            Top             =   150
            Width           =   480
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "% "
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Index           =   7
            Left            =   5925
            TabIndex        =   173
            Top             =   450
            Width           =   300
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Impuestos"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   55
            Left            =   4920
            TabIndex        =   172
            Top             =   150
            Width           =   885
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Con Merma"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   54
            Left            =   8445
            TabIndex        =   132
            Top             =   5325
            Visible         =   0   'False
            Width           =   990
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Sin Merma"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   53
            Left            =   7050
            TabIndex        =   131
            Top             =   5325
            Visible         =   0   'False
            Width           =   930
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Utilidad"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   52
            Left            =   3720
            TabIndex        =   130
            Top             =   150
            Width           =   630
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Precio"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   51
            Left            =   2280
            TabIndex        =   129
            Top             =   165
            Width           =   525
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Vigencia de la Oferta"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   50
            Left            =   120
            TabIndex        =   128
            Top             =   1575
            Width           =   1800
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Index           =   45
            Left            =   9540
            TabIndex        =   106
            Top             =   6450
            Visible         =   0   'False
            Width           =   225
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Index           =   44
            Left            =   9540
            TabIndex        =   105
            Top             =   6045
            Visible         =   0   'False
            Width           =   225
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Index           =   43
            Left            =   9540
            TabIndex        =   104
            Top             =   6855
            Visible         =   0   'False
            Width           =   225
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Index           =   42
            Left            =   9540
            TabIndex        =   103
            Top             =   5640
            Visible         =   0   'False
            Width           =   225
         End
         Begin VB.Label Margen3C 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   8490
            TabIndex        =   102
            Top             =   6450
            Visible         =   0   'False
            Width           =   900
         End
         Begin VB.Label Margen2C 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   8490
            TabIndex        =   101
            Top             =   6045
            Visible         =   0   'False
            Width           =   900
         End
         Begin VB.Label MargenOC 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   8490
            TabIndex        =   100
            Top             =   6855
            Visible         =   0   'False
            Width           =   900
         End
         Begin VB.Label Margen1C 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   8490
            TabIndex        =   99
            Top             =   5640
            Visible         =   0   'False
            Width           =   900
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Hasta"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Index           =   35
            Left            =   3585
            TabIndex        =   89
            Top             =   1980
            Width           =   660
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Desde"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Index           =   34
            Left            =   105
            TabIndex        =   88
            Top             =   1980
            Width           =   660
         End
         Begin VB.Label Margen1 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   3540
            TabIndex        =   24
            Top             =   480
            Width           =   900
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Precio de Venta 1"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   23
            Left            =   105
            TabIndex        =   81
            Top             =   555
            Width           =   1515
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Index           =   16
            Left            =   4560
            TabIndex        =   80
            Top             =   480
            Width           =   225
         End
         Begin VB.Label MargenO 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   3540
            TabIndex        =   27
            Top             =   975
            Width           =   900
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Precio de Oferta"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   17
            Left            =   105
            TabIndex        =   55
            Top             =   1035
            Width           =   1395
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Index           =   14
            Left            =   4560
            TabIndex        =   54
            Top             =   975
            Width           =   225
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Index           =   21
            Left            =   4560
            TabIndex        =   49
            Top             =   5445
            Visible         =   0   'False
            Width           =   225
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Index           =   22
            Left            =   4560
            TabIndex        =   48
            Top             =   5850
            Visible         =   0   'False
            Width           =   225
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Precio de Venta 2"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   24
            Left            =   120
            TabIndex        =   47
            Top             =   5520
            Visible         =   0   'False
            Width           =   1515
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Precio de Venta 3"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   25
            Left            =   105
            TabIndex        =   46
            Top             =   5910
            Visible         =   0   'False
            Width           =   1515
         End
         Begin VB.Label Margen2 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   3540
            TabIndex        =   25
            Top             =   5445
            Visible         =   0   'False
            Width           =   900
         End
         Begin VB.Label Margen3 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   3540
            TabIndex        =   26
            Top             =   5850
            Visible         =   0   'False
            Width           =   900
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H00FFFFFF&
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00E0E0E0&
            FillStyle       =   0  'Solid
            Height          =   1050
            Index           =   3
            Left            =   0
            Top             =   1440
            Width           =   7335
         End
      End
      Begin VB.Frame f_costos 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Caption         =   "Costos"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   3180
         Left            =   300
         TabIndex        =   29
         Top             =   10380
         Width           =   5910
         Begin VB.TextBox DBMONEDA 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   350
            Left            =   1680
            MaxLength       =   10
            TabIndex        =   123
            Top             =   6195
            Visible         =   0   'False
            Width           =   1585
         End
         Begin VB.CommandButton btn_Moneda 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Times New Roman"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   350
            Left            =   3335
            Picture         =   "Ficha_ProductosLite.frx":80D5
            Style           =   1  'Graphical
            TabIndex        =   122
            Top             =   6195
            Visible         =   0   'False
            Width           =   435
         End
         Begin VB.TextBox txt_PorcentajeMerma 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#.##0,00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   8202
               SubFormatType   =   1
            EndProperty
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   350
            Left            =   3810
            MaxLength       =   18
            TabIndex        =   120
            Top             =   5145
            Visible         =   0   'False
            Width           =   1965
         End
         Begin VB.TextBox CPromedio_Merma 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#.##0,00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   8202
               SubFormatType   =   1
            EndProperty
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   350
            Left            =   3810
            Locked          =   -1  'True
            MaxLength       =   18
            TabIndex        =   98
            Top             =   4335
            Visible         =   0   'False
            Width           =   1965
         End
         Begin VB.TextBox CAnterior_Merma 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#.##0,00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   8202
               SubFormatType   =   1
            EndProperty
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   350
            Left            =   3810
            Locked          =   -1  'True
            MaxLength       =   18
            TabIndex        =   97
            Top             =   3930
            Visible         =   0   'False
            Width           =   1965
         End
         Begin VB.TextBox CActual_Merma 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#.##0,00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   8202
               SubFormatType   =   1
            EndProperty
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   350
            Left            =   3810
            Locked          =   -1  'True
            MaxLength       =   18
            TabIndex        =   96
            Top             =   3525
            Visible         =   0   'False
            Width           =   1965
         End
         Begin VB.TextBox CReposicion_merma 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#.##0,00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   8202
               SubFormatType   =   1
            EndProperty
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   350
            Left            =   3810
            Locked          =   -1  'True
            MaxLength       =   18
            TabIndex        =   95
            Top             =   4740
            Visible         =   0   'False
            Width           =   1965
         End
         Begin VB.TextBox CReposicion 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#.##0,00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   8202
               SubFormatType   =   1
            EndProperty
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   350
            Left            =   1665
            MaxLength       =   18
            TabIndex        =   10
            Top             =   6180
            Visible         =   0   'False
            Width           =   2085
         End
         Begin VB.TextBox CAnterior 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#.##0,00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   8202
               SubFormatType   =   1
            EndProperty
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   350
            Left            =   1665
            MaxLength       =   18
            TabIndex        =   8
            Top             =   4770
            Width           =   2085
         End
         Begin VB.TextBox CPromedio 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#.##0,00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   8202
               SubFormatType   =   1
            EndProperty
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   350
            Left            =   1665
            MaxLength       =   18
            TabIndex        =   9
            Top             =   5415
            Visible         =   0   'False
            Width           =   2085
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "+ Merma"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   41
            Left            =   4320
            TabIndex        =   127
            Top             =   3240
            Visible         =   0   'False
            Width           =   780
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Moneda"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   26
            Left            =   120
            TabIndex        =   125
            Top             =   6240
            Visible         =   0   'False
            Width           =   675
         End
         Begin VB.Label lbl_moneda 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   345
            Left            =   3825
            TabIndex        =   124
            Top             =   6195
            Visible         =   0   'False
            Width           =   1965
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "% de Merma"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Index           =   39
            Left            =   2400
            TabIndex        =   121
            Top             =   5220
            Visible         =   0   'False
            Width           =   1275
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Costo Reposici�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   11
            Left            =   90
            TabIndex        =   53
            Top             =   6240
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Costo Anterior"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   19
            Left            =   90
            TabIndex        =   45
            Top             =   4830
            Width           =   1230
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Costo Promedio"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   20
            Left            =   90
            TabIndex        =   44
            Top             =   5475
            Visible         =   0   'False
            Width           =   1350
         End
      End
      Begin VB.CommandButton Command2 
         Caption         =   "&Grabar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3960
         Left            =   46440
         TabIndex        =   40
         Top             =   0
         Width           =   75
      End
      Begin VB.Frame FrmDepGruSub 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   1575
         Left            =   300
         TabIndex        =   28
         Top             =   2325
         Width           =   10110
         Begin VB.CommandButton Btn_SubGrupo 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Times New Roman"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   350
            Left            =   3315
            Picture         =   "Ficha_ProductosLite.frx":88D7
            Style           =   1  'Graphical
            TabIndex        =   21
            Top             =   1080
            Width           =   435
         End
         Begin VB.TextBox Departamento 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   1890
            MaxLength       =   5
            TabIndex        =   5
            Top             =   240
            Width           =   1305
         End
         Begin VB.CommandButton Btn_Grupo 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Times New Roman"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   350
            Left            =   3315
            Picture         =   "Ficha_ProductosLite.frx":90D9
            Style           =   1  'Graphical
            TabIndex        =   20
            Top             =   660
            Width           =   435
         End
         Begin VB.CommandButton Btn_Dpto 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Times New Roman"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   350
            Left            =   3300
            Picture         =   "Ficha_ProductosLite.frx":98DB
            Style           =   1  'Graphical
            TabIndex        =   19
            Top             =   255
            Width           =   435
         End
         Begin VB.TextBox SubGrupo 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   1890
            MaxLength       =   6
            TabIndex        =   7
            Top             =   1080
            Width           =   1305
         End
         Begin VB.TextBox Grupo 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   1890
            MaxLength       =   5
            TabIndex        =   6
            Top             =   660
            Width           =   1305
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "*"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   240
            Index           =   60
            Left            =   1350
            TabIndex        =   137
            Top             =   255
            Width           =   120
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Utilidad"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   57
            Left            =   8760
            TabIndex        =   134
            Top             =   4020
            Width           =   630
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Index           =   49
            Left            =   9630
            TabIndex        =   114
            Top             =   4335
            Width           =   225
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Index           =   48
            Left            =   9630
            TabIndex        =   113
            Top             =   4755
            Width           =   225
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Index           =   47
            Left            =   9630
            TabIndex        =   112
            Top             =   5205
            Width           =   225
         End
         Begin VB.Label Lbl_Departamento_por 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   8670
            TabIndex        =   111
            Top             =   4320
            Width           =   900
         End
         Begin VB.Label Lbl_Grupo_por 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   8670
            TabIndex        =   110
            Top             =   4740
            Width           =   900
         End
         Begin VB.Label Lbl_SubGrupo_por 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   8670
            TabIndex        =   109
            Top             =   5160
            Width           =   900
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Grupo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   3
            Left            =   135
            TabIndex        =   68
            Top             =   675
            Width           =   1335
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Departamento"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   240
            Index           =   2
            Left            =   120
            TabIndex        =   67
            Top             =   255
            Width           =   1215
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Sub Grupo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Index           =   4
            Left            =   135
            TabIndex        =   66
            Top             =   1125
            Width           =   1215
         End
         Begin VB.Label Lbl_SubGrupo 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   3930
            TabIndex        =   65
            Top             =   1080
            Width           =   6060
         End
         Begin VB.Label Lbl_Grupo 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   3930
            TabIndex        =   64
            Top             =   660
            Width           =   6060
         End
         Begin VB.Label Lbl_Departamento 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   3930
            TabIndex        =   63
            Top             =   240
            Width           =   6060
         End
      End
      Begin VB.Frame f_general 
         Appearance      =   0  'Flat
         Caption         =   "Datos Generales"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   6405
         Left            =   150
         TabIndex        =   41
         Top             =   330
         Width           =   15045
         Begin VB.TextBox BarCode 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   240
            MaxLength       =   40
            TabIndex        =   185
            Top             =   1320
            Width           =   2415
         End
         Begin VB.Frame Frame6 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Caption         =   "Informaci�n General"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   1185
            Left            =   10320
            TabIndex        =   178
            Top             =   4440
            Width           =   4530
            Begin MSComCtl2.DTPicker Actualizado 
               Height          =   330
               Left            =   2430
               TabIndex        =   179
               Top             =   570
               Width           =   2025
               _ExtentX        =   3572
               _ExtentY        =   582
               _Version        =   393216
               Enabled         =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               CalendarTitleBackColor=   -2147483624
               CalendarTrailingForeColor=   -2147483624
               CustomFormat    =   "dd/MM/yyyy"
               Format          =   102039555
               CurrentDate     =   38106
            End
            Begin MSComCtl2.DTPicker Ingresado 
               Height          =   330
               Left            =   120
               TabIndex        =   180
               Top             =   570
               Width           =   2025
               _ExtentX        =   3572
               _ExtentY        =   582
               _Version        =   393216
               Enabled         =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               CalendarTitleBackColor=   -2147483624
               CalendarTrailingForeColor=   -2147483624
               CustomFormat    =   "dd/MM/yyyy"
               Format          =   102039555
               CurrentDate     =   38106
            End
            Begin VB.Label C�digo 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BackStyle       =   0  'Transparent
               Caption         =   "�ltima Actualizaci�n"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   255
               Index           =   37
               Left            =   2430
               TabIndex        =   183
               Top             =   330
               Width           =   1710
            End
            Begin VB.Label C�digo 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BackStyle       =   0  'Transparent
               Caption         =   "Fecha de Creaci�n"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   255
               Index           =   36
               Left            =   120
               TabIndex        =   182
               Top             =   330
               Width           =   1665
            End
            Begin VB.Label C�digo 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               BackStyle       =   0  'Transparent
               Caption         =   "* Datos Obligatorios"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H000000FF&
               Height          =   240
               Index           =   61
               Left            =   120
               TabIndex        =   181
               Top             =   0
               Width           =   1740
            End
         End
         Begin VB.Frame Frame5 
            Appearance      =   0  'Flat
            BackColor       =   &H00E7E8E8&
            BorderStyle     =   0  'None
            Caption         =   "Imagen"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   3930
            Left            =   10400
            TabIndex        =   74
            Top             =   270
            Width           =   4455
            Begin VB.Image Imagen 
               Appearance      =   0  'Flat
               Height          =   3795
               Left            =   75
               Stretch         =   -1  'True
               Top             =   75
               Width           =   4275
            End
            Begin VB.Label C�digo 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               BackStyle       =   0  'Transparent
               Caption         =   "Imagen"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   15.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00C0C0C0&
               Height          =   375
               Index           =   62
               Left            =   1650
               TabIndex        =   138
               Top             =   1440
               Width           =   1080
            End
            Begin VB.Label C�digo 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               BackStyle       =   0  'Transparent
               Caption         =   "Doble Click para cambiar"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00C0C0C0&
               Height          =   240
               Index           =   63
               Left            =   1155
               TabIndex        =   139
               Top             =   1920
               Width           =   2130
            End
            Begin VB.Label C�digo 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               BackStyle       =   0  'Transparent
               Caption         =   "Click Derecho para eliminar"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00C0C0C0&
               Height          =   240
               Index           =   74
               Left            =   1155
               TabIndex        =   189
               Top             =   2280
               Width           =   2370
            End
         End
         Begin VB.TextBox txt_descripcionCorta 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   240
            MaxLength       =   50
            TabIndex        =   107
            Top             =   10440
            Visible         =   0   'False
            Width           =   4590
         End
         Begin VB.CheckBox chk_insumo 
            Appearance      =   0  'Flat
            BackColor       =   &H00E7E8E8&
            Caption         =   "Si"
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   6300
            TabIndex        =   92
            TabStop         =   0   'False
            Top             =   11025
            Visible         =   0   'False
            Width           =   480
         End
         Begin VB.Frame FrmPlantilla 
            Appearance      =   0  'Flat
            BackColor       =   &H00E7E8E8&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   975
            Left            =   10400
            TabIndex        =   70
            Top             =   3195
            Visible         =   0   'False
            Width           =   4470
            Begin VB.TextBox txtCodCarExt 
               Appearance      =   0  'Flat
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   0  'None
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   360
               Left            =   2595
               MaxLength       =   10
               TabIndex        =   22
               Top             =   60
               Width           =   1320
            End
            Begin VB.CommandButton Command1 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   350
               Left            =   3990
               Picture         =   "Ficha_ProductosLite.frx":A0DD
               Style           =   1  'Graphical
               TabIndex        =   23
               Top             =   60
               Width           =   435
            End
            Begin VB.Label C�digo 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               BackStyle       =   0  'Transparent
               Caption         =   "Plantilla Producto Extendido"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   240
               Index           =   33
               Left            =   75
               TabIndex        =   72
               Top             =   105
               Width           =   2655
               WordWrap        =   -1  'True
            End
            Begin VB.Label lblDesCarExt 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   360
               Left            =   75
               TabIndex        =   71
               Top             =   480
               Width           =   4335
            End
         End
         Begin VB.Frame Frame8 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   1185
            Left            =   4770
            TabIndex        =   79
            Top             =   7560
            Width           =   10200
            Begin VB.CommandButton cmd_Mascara 
               Appearance      =   0  'Flat
               Caption         =   "Ubicaci�n"
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   945
               Left            =   8490
               Picture         =   "Ficha_ProductosLite.frx":A8DF
               Style           =   1  'Graphical
               TabIndex        =   94
               TabStop         =   0   'False
               ToolTipText     =   "Importa los productos a su planilla de trabajo"
               Top             =   1605
               Visible         =   0   'False
               Width           =   1620
            End
            Begin VB.CommandButton Command3 
               Appearance      =   0  'Flat
               Caption         =   "Imp. Etiquetas"
               CausesValidation=   0   'False
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   945
               Left            =   8490
               Picture         =   "Ficha_ProductosLite.frx":C661
               Style           =   1  'Graphical
               TabIndex        =   87
               TabStop         =   0   'False
               ToolTipText     =   "Importa los productos a su planilla de trabajo"
               Top             =   1365
               Visible         =   0   'False
               Width           =   1620
            End
            Begin VB.CommandButton Proveedor 
               Appearance      =   0  'Flat
               Caption         =   "&Proveedores "
               CausesValidation=   0   'False
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   945
               Left            =   8490
               Picture         =   "Ficha_ProductosLite.frx":E3E3
               Style           =   1  'Graphical
               TabIndex        =   83
               ToolTipText     =   "Proveedores de este Producto"
               Top             =   1365
               Visible         =   0   'False
               Width           =   1620
            End
            Begin VB.CommandButton btn_Compuesto 
               Appearance      =   0  'Flat
               Caption         =   "Partes"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   945
               Left            =   8490
               Picture         =   "Ficha_ProductosLite.frx":10165
               Style           =   1  'Graphical
               TabIndex        =   82
               Top             =   1845
               Visible         =   0   'False
               Width           =   1620
            End
            Begin VB.CommandButton Btn_ProExt 
               Appearance      =   0  'Flat
               Caption         =   "P. Extendidos"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   945
               Left            =   8490
               Picture         =   "Ficha_ProductosLite.frx":11EE7
               Style           =   1  'Graphical
               TabIndex        =   32
               Top             =   1725
               Visible         =   0   'False
               Width           =   1620
            End
         End
         Begin VB.ComboBox Tipo_Producto 
            BackColor       =   &H00FFFFFF&
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "Ficha_ProductosLite.frx":13C69
            Left            =   6750
            List            =   "Ficha_ProductosLite.frx":13C7F
            Style           =   2  'Dropdown List
            TabIndex        =   2
            Top             =   1320
            Width           =   2490
         End
         Begin VB.CheckBox Activo 
            Appearance      =   0  'Flat
            BackColor       =   &H00E7E8E8&
            Caption         =   "Activo"
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   9300
            TabIndex        =   18
            TabStop         =   0   'False
            Top             =   1350
            Value           =   1  'Checked
            Width           =   930
         End
         Begin VB.TextBox Modelo 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   4755
            MaxLength       =   100
            TabIndex        =   4
            Top             =   1320
            Width           =   1935
         End
         Begin VB.TextBox Marca 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   2745
            MaxLength       =   40
            TabIndex        =   3
            Top             =   1320
            Width           =   1935
         End
         Begin VB.TextBox Codigo 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   225
            MaxLength       =   15
            TabIndex        =   0
            Top             =   585
            Width           =   1770
         End
         Begin VB.TextBox Descripcion 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   2055
            MaxLength       =   255
            TabIndex        =   1
            Top             =   585
            Width           =   8085
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "C�digo de Barra"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   72
            Left            =   240
            TabIndex        =   186
            Top             =   1065
            Width           =   1380
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "*"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   240
            Index           =   59
            Left            =   3170
            TabIndex        =   136
            Top             =   360
            Width           =   120
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "*"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   240
            Index           =   58
            Left            =   840
            TabIndex        =   135
            Top             =   360
            Width           =   120
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "*"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   240
            Index           =   56
            Left            =   8670
            TabIndex        =   133
            Top             =   1065
            Width           =   120
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Descripci�n Corta"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   46
            Left            =   240
            TabIndex        =   108
            Top             =   10185
            Visible         =   0   'False
            Width           =   1500
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Insumo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   38
            Left            =   4980
            TabIndex        =   93
            Top             =   11025
            Visible         =   0   'False
            Width           =   630
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   240
            Index           =   6
            Left            =   2055
            TabIndex        =   69
            Top             =   330
            Width           =   975
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Status"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   32
            Left            =   9300
            TabIndex        =   62
            Top             =   1065
            Width           =   540
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Tipo de Producto"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   28
            Left            =   6750
            TabIndex        =   61
            Top             =   1065
            Width           =   1455
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Modelo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   13
            Left            =   4755
            TabIndex        =   52
            Top             =   1065
            Width           =   615
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Marca"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   5
            Left            =   2745
            TabIndex        =   43
            Top             =   1065
            Width           =   525
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   240
            Index           =   0
            Left            =   225
            TabIndex        =   42
            Top             =   330
            Width           =   585
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H00FFFFFF&
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00E7E8E8&
            FillStyle       =   0  'Solid
            Height          =   1635
            Index           =   0
            Left            =   150
            Top             =   270
            Width           =   10110
         End
      End
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   14310
      Top             =   1200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_ProductosLite.frx":13CC8
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_ProductosLite.frx":15A5A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_ProductosLite.frx":16734
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_ProductosLite.frx":1740E
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_ProductosLite.frx":191A0
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_ProductosLite.frx":1AF32
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_ProductosLite.frx":1CCC4
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "Ficha_ProductosLite"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public mMask As Object 'cls_MascarasUbicacion
Public Dir_File As String
Public Imagen_Prod As String
Public porcentaje As Integer
Public lCod_Cos As String
Public lGru_Cos As String
Public lCod_Mon As String
Public LibreArc As Integer
Public imagen_name As String
Public imagen_handle As Double
'Tipo de Productos
'Public IntTipoUnidad As Integer '1
'Public IntTipoPesado As Integer '2
'Public IntTipoPesable As Integer '3
'Public IntTipoCarExt As Integer '4
'Public IntTipoInformativo As Integer '5
'Public IntTipoCompuesto As Integer '6 -2
Public ccodplan As String
Public ccodproext As String
Public strNombreCompania As String
' Public Frm_Super_Consultas.arrResultado
Private CodTemp As String
Dim fImportarPro As Boolean
Dim mPrecioInicial As Double
Dim mCantidadDecimales As Integer
'Dim fClsProCom As New cls_productosCompuesto 'recsuna.cls_productos
Dim mClsGrupos As New cls_grupos
Dim mPermitirCostoxImpuesto As Boolean
Dim clsCostoxImpuesto As cls_CostoxImpuesto
Dim mNoRedondear As Boolean
Dim mCargandoProducto As Boolean
Dim mGrabar As String
'Dim mRestringirRedondeoRegulados As Boolean
'Dim EsProductoRegulado As Boolean

Public WithEvents TmpPreviewPic As Image
Attribute TmpPreviewPic.VB_VarHelpID = -1
Public TmpPreviewPicContainer As PictureBox
Private Const TmpImgFileName = "StellarProductPicTmp.jpg"

Private Sub TmpPreviewPic_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    On Error Resume Next
    
    If Button = vbRightButton Or Button = vbMiddleButton Then
        
        If dialogo.FileName <> "" Then
            ShellEx 0, vbNullString, "" & dialogo.FileName & "", vbNullString, vbNullString, 1
        Else
            If mGrabar = "update" Then
                ShowPic
            End If
        End If
        
        TmpPreviewPic_MouseUp vbLeftButton, Shift, X, Y
        
    ElseIf Button = vbLeftButton Then
        Me.Controls.Remove TmpPreviewPic
        Me.Controls.Remove TmpPreviewPicContainer
    End If
    
End Sub

Private Sub ShowPic()

    On Error Resume Next
    
    KillSecure TmpImgFileName
    
    Dim mSQl As String, mRs As Recordset
    
    Set mRs = New Recordset
    
    mSQl = "SELECT c_FileImagen FROM MA_PRODUCTOS WHERE c_Codigo = '" & Codigo.Text & "'"
    
    mRs.Open mSQl, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        
        If Not IsNull(mRs!c_FileImagen) Then
            
            Call PopImageOfDb(Imagen, mRs, "c_FileImagen", TmpImgFileName)
            
            Dim TmpFS As New Scripting.FileSystemObject
            
            If TmpFS.FileExists(TmpImgFileName) Then
                ShellEx 0, vbNullString, "" & TmpFS.GetAbsolutePathName(TmpImgFileName) & "", vbNullString, vbNullString, 1
            End If
            
        End If
        
    End If
    
End Sub

Private Sub BarCode_LostFocus()
    If BarCode = Codigo Then 'Ficha_Productos.codigo Then
        'MsgBox "El C�digo alterno no puede ser el mismo del producto", vbInformation + vbOKOnly, "Mensaje Stellar"
        Mensaje True, StellarMensaje(16218)
        BarCode = vbNullString
        If PuedeObtenerFoco(BarCode) Then BarCode.SetFocus
    Else
        If BarCode.Enabled = True Then
            'csql = "select * from ma_codigos where c_codnasa = '" & Trim(Ficha_Productos.codigo.Text) & "' and c_codigo = '" & Trim(codigo.Text) & "'"
            csql = "SELECT * FROM MA_CODIGOS WHERE c_Codigo = '" & Trim(BarCode.Text) & "'"
            
            Call Apertura_Recordset(rscodigos)
            
            rscodigos.Open csql, Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            If Not rscodigos.EOF Then
                'MsgBox "C�digo ya Existe", vbInformation + vbOKOnly, "Mensaje Stellar"
                Mensaje True, StellarMensaje(16052)
                BarCode = vbNullString
                If PuedeObtenerFoco(BarCode) Then BarCode.SetFocus
            End If
        End If
    End If
End Sub

Private Sub btn_compuesto_Click()
    Set Ficha_productos_Compuesto = Nothing
    Ficha_productos_Compuesto.Show vbModal
End Sub

Private Sub CActual_Click()
    If ModoTouch Then TecladoAvanzado CActual
End Sub

Private Sub Cactual_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case Shift
        Case vbCtrlMask
            Select Case KeyCode
                Case vbKeyF2
                    If mPermitirCostoxImpuesto Then
                        If IsNumeric(CActual.Text) Then
                            Set clsCostoxImpuesto = New cls_CostoxImpuesto
                            CActual.Text = clsCostoxImpuesto.Cargar_FormCostoxImpuesto(CActual.Text)
                            Cactual_LostFocus
                            Set clsCostoxImpuesto = Nothing
                        End If
                    End If
            End Select
        Case Else
            If KeyCode = vbKeyF2 Then
                CActual = FormatNumber(Seleccion_Tipo_Costo(Codigo), 2)
            End If
    End Select
End Sub

Private Sub CAnterior_Click()
    If ModoTouch Then TecladoAvanzado CAnterior
End Sub

Private Sub Canterior_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case Shift
        Case vbCtrlMask
            Select Case KeyCode
                Case vbKeyF2
                    If mPermitirCostoxImpuesto Then
                        If IsNumeric(CAnterior.Text) Then
                            Set clsCostoxImpuesto = New cls_CostoxImpuesto
                            CAnterior.Text = clsCostoxImpuesto.Cargar_FormCostoxImpuesto(CAnterior.Text)
                            Canterior_LostFocus
                            Set clsCostoxImpuesto = Nothing
                        End If
                    End If
            End Select
        Case Else
            If KeyCode = vbKeyF2 Then
                CAnterior = FormatNumber(Seleccion_Tipo_Costo(Codigo), 2)
            End If
    End Select
End Sub

Private Sub CantidadBulto_Click()
    If ModoTouch Then TecladoAvanzado CantidadBulto
End Sub

Private Sub CMD_MASCARA_Click()
    If Trim(Codigo.Text) <> "" And (Descripcion.Text) <> "" Then
        Call MostrarUbicacionProducto
    End If
End Sub

Private Sub cod_Arancel_Click()
    If ModoTouch Then TecladoAvanzado cod_Arancel
End Sub

Private Sub Costo_Original_Click()
    If ModoTouch Then TecladoAvanzado Costo_Original
End Sub

Private Sub CPromedio_Click()
    If ModoTouch Then TecladoAvanzado CPromedio
End Sub

Private Sub Cpromedio_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case Shift
        Case vbCtrlMask
            Select Case KeyCode
                Case vbKeyF2
                    If mPermitirCostoxImpuesto Then
                        If IsNumeric(CPromedio.Text) Then
                            Set clsCostoxImpuesto = New cls_CostoxImpuesto
                            CPromedio.Text = clsCostoxImpuesto.Cargar_FormCostoxImpuesto(CPromedio.Text)
                            Cpromedio_LostFocus
                            Set clsCostoxImpuesto = Nothing
                        End If
                    End If
            End Select
        Case Else
            If KeyCode = vbKeyF2 Then
                CPromedio = FormatNumber(Seleccion_Tipo_Costo(Codigo), 2)
            End If
    End Select
End Sub

Private Sub CReposicion_Click()
    If ModoTouch Then TecladoAvanzado CReposicion
End Sub

Private Sub CReposicion_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case Shift
        Case vbCtrlMask
            Select Case KeyCode
                Case vbKeyF2
                    If mPermitirCostoxImpuesto Then
                        If IsNumeric(CReposicion.Text) Then
                            Set clsCostoxImpuesto = New cls_CostoxImpuesto
                            CReposicion.Text = clsCostoxImpuesto.Cargar_FormCostoxImpuesto(CReposicion.Text)
                            CReposicion_LostFocus
                            Set clsCostoxImpuesto = Nothing
                        End If
                    End If
            End Select
        Case Else
            If KeyCode = vbKeyF2 Then
                CReposicion = FormatNumber(Seleccion_Tipo_Costo(Codigo), 2)
            End If
    End Select
End Sub

Private Sub DBMONEDA_Click()
    If ModoTouch Then TecladoAvanzado DBMONEDA
End Sub

Private Sub Decimales_Click()
    If ModoTouch Then TecladoAvanzado Decimales
End Sub

Private Sub Departamento_Click()
    If ModoTouch Then TecladoAvanzado Departamento
End Sub

Private Sub Departamento_GotFocus()
    CodTemp = Me.Departamento
    If Toolbar1.Buttons(1).Enabled = False Then Toolbar1.Buttons(7).Enabled = True
End Sub

Private Sub departamento_LostFocus()

    On Error GoTo Errores
    'Set forma = Ficha_Productos
    
    If Departamento.Text <> CodTemp Then
        Buscar_Departamento " C_CODIGO = '" & Me.Departamento & "'", False
        Me.Grupo = ""
        Me.Lbl_Grupo = ""
        Me.Lbl_Grupo_por = ""
        Me.SubGrupo = ""
        Me.Lbl_SubGrupo = ""
        Me.Lbl_SubGrupo_por = ""
    ElseIf Trim(Departamento.Text) = "" Then
        Call departamento_KeyDown(vbKeyDelete, 0)
    End If
    
    CodTemp = ""
    
    Exit Sub
    
Errores:

    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".Departamento_LostFocus", Err.Source)
    Unload Me

End Sub

Private Sub departamento_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF2
            Buscar_Departamento
'            Tabla = "ma_departamentos"
'            Titulo = "D E P A R T A M E N T O S"
'            Call dgs
'            If lcdepartamento <> lcdptogrupo Then Call grupo_KeyDown(vbKeyDelete, 0)
        
        Case Is = vbKeyDelete
            Departamento.Text = ""
            Lbl_Departamento.Caption = ""
            Grupo.Text = ""
            Lbl_Grupo.Caption = ""
            SubGrupo.Text = ""
            Lbl_SubGrupo.Caption = ""
            lcDepartamento = ""
            lcGrupo = ""
            lcsubgrupo = ""
    End Select
End Sub

Private Sub departamento_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case Is = vbKeyReturn
            oTeclado.Key_Tab
    End Select
End Sub

Private Sub btn_dpto_Click()
    
    On Error GoTo Errores
    
    Departamento.SetFocus
    Buscar_Departamento
    'Call departamento_KeyDown(vbKeyF2, 0)
    
    Exit Sub
    
Errores:

    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".btn_dpto_Click", Err.Source)
    Unload Me
    
End Sub

Private Sub Buscar_Departamento(Optional Condicion As String = "", Optional Mostrar As Boolean = True)
    
    'Ivan Espinoza 31/08/02 Inicio
    
    Dim Rs As New ADODB.Recordset
    Dim SQL As String
    
    On Error GoTo Errores
    
    SQL = "select C_CODIGO, C_DESCRIPCIO, C_GRUPO,NU_PORCUTILIDAD From MA_Departamentos"
    
    Titulo = StellarMensaje(361) '"D E P A R T A M E N T O"
    
    Frm_Super_Consultas.Inicializar SQL, Titulo, Ent.BDD
    Frm_Super_Consultas.Add_ItemLabels StellarMensaje(142), "C_CODIGO", 1860, 0 'CODIGO
    Frm_Super_Consultas.Add_ItemLabels StellarMensaje(15003), "C_DESCRIPCIO", 6495, 0 'DESCRIPCION
    Frm_Super_Consultas.Add_ItemLabels StellarMensaje(161), "C_GRUPO", 2750, 0 'GRUPO
    Frm_Super_Consultas.Add_ItemLabels "%", "NU_PORCUTILIDAD", 0, 0
    Frm_Super_Consultas.Add_ItemSearching StellarMensaje(15003), "C_DESCRIPCIO" 'DESCRIPCION
    Frm_Super_Consultas.Add_ItemSearching StellarMensaje(142), "C_CODIGO" 'CODIGO
    Frm_Super_Consultas.Add_ItemSearching StellarMensaje(161), "C_GRUPO" 'GRUPO
    
    If Trim(Condicion) <> "" Then Frm_Super_Consultas.StrCondicion = Condicion
    
    Set Frm_Super_Consultas.FormPadre = Me
    'Set Frm_Super_Consultas. = Ent.BDD

    If Mostrar = True Then
        Frm_Super_Consultas.txtDato.Text = "%"
        Frm_Super_Consultas.Show vbModal
    Else
        Frm_Super_Consultas.ArrResultado = Frm_Super_Consultas.Buscar
    End If
    
    If Not IsEmpty(Frm_Super_Consultas.ArrResultado) Then
        If Trim(Frm_Super_Consultas.ArrResultado(0)) <> Trim(Me.Departamento.Text) Then
            Me.Grupo.Text = ""
            Me.Lbl_Grupo.Caption = ""
            Me.Lbl_Grupo_por.Caption = ""
            Me.SubGrupo.Text = ""
            Me.Lbl_SubGrupo = ""
            Me.Lbl_SubGrupo_por = ""
        End If
        
        Me.Departamento.Text = Trim(Frm_Super_Consultas.ArrResultado(0))
        Me.Lbl_Departamento.Caption = Trim(Frm_Super_Consultas.ArrResultado(1))
        Me.Lbl_Departamento_por.Caption = Trim(Frm_Super_Consultas.ArrResultado(2))
    End If
    
    Exit Sub
    
Errores:
    
    Err.Clear
    ' Ivan Espinoza 31/08/02 Final
    
End Sub

Private Sub des_Arancel_Click()
    If ModoTouch Then TecladoAvanzado des_Arancel
End Sub

Private Sub Descripcion_Click()
    If ModoTouch Then TecladoAvanzado Descripcion
End Sub

Private Sub Grupo_Click()
    If ModoTouch Then TecladoAvanzado Grupo
End Sub

Private Sub Grupo_GotFocus()
    CodTemp = Me.Grupo
    If Tecla_pulsada <> True Then Tecla_pulsada = False
End Sub

Private Sub grupo_LostFocus()

    If Grupo.Text <> CodTemp And Trim(Grupo.Text) <> "" Then
        Buscar_Grupo " C_CODIGO = '" & Me.Grupo & "'", False
        Me.SubGrupo = ""
        Me.Lbl_SubGrupo = ""
    ElseIf Trim(Grupo.Text) = "" Then
        Call grupo_KeyDown(vbKeyDelete, 0)
    End If
    
    CodTemp = ""

End Sub

Private Sub grupo_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF2
            Buscar_Grupo
'            tecla_pulsada = True
'            Tabla = "ma_grupos"
'            Titulo = "G R U P O S"
'            Call dgs
'            tecla_pulsada = False
                    
        Case Is = vbKeyDelete
            Grupo.Text = ""
            Lbl_Grupo.Caption = ""
            SubGrupo.Text = ""
            Lbl_SubGrupo.Caption = ""
            lcGrupo = ""
            lcsubgrupo = ""
    End Select
End Sub

Private Sub grupo_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case Is = vbKeyReturn
            oTeclado.Key_Tab
    End Select
End Sub

Private Sub Btn_Grupo_Click()
    
    Dim Condicion As String
    
    On Error GoTo Errores
    
    Grupo.SetFocus
    Tecla_pulsada = True
    Buscar_Grupo
    
    'Call grupo_KeyDown(vbKeyF2, 0)

    Exit Sub
    
Errores:

    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".btn_grupo_Click", Err.Source)
    Unload Me
    
End Sub

Private Sub Buscar_Grupo(Optional Condicion As String = "", Optional Mostrar As Boolean = True)
    
    ' Ivan Espinoza 31/08/02 Inicio
    
    Dim Rs As New ADODB.Recordset
    Dim SQL As String
    
    On Error GoTo Errores
    
    SQL = "select C_CODIGO, C_DESCRIPCIO, C_GRUPO,NU_PORCUTILIDAD From MA_Grupos"
    
    Titulo = StellarMensaje(362) '"G R U P O"
    
    Frm_Super_Consultas.Inicializar SQL, Titulo, Ent.BDD
    Frm_Super_Consultas.Add_ItemLabels StellarMensaje(142), "C_CODIGO", 1860, 0 'CODIGO
    Frm_Super_Consultas.Add_ItemLabels StellarMensaje(15003), "C_DESCRIPCIO", 6495, 0 'DESCRIPCION
    Frm_Super_Consultas.Add_ItemLabels StellarMensaje(161), "C_GRUPO", 2750, 0 'GRUPO
    Frm_Super_Consultas.Add_ItemLabels "%", "NU_PORCUTILIDAD", 0, 0
    Frm_Super_Consultas.Add_ItemSearching StellarMensaje(15003), "C_DESCRIPCIO" 'DESCRIPCION
    Frm_Super_Consultas.Add_ItemSearching StellarMensaje(142), "C_CODIGO" 'CODIGO
    Frm_Super_Consultas.Add_ItemSearching StellarMensaje(161), "C_GRUPO" 'GRUPO
    
    If Trim(Me.Departamento) <> "" Then Condicion = Condicion & IIf(Len(Trim(Condicion)), " AND", "") & " c_departamento = '" & Trim(Me.Departamento) & "'"
    Frm_Super_Consultas.StrCondicion = Condicion
    
    Set Frm_Super_Consultas.FormPadre = Me
    'Set Frm_Super_Consultas.Connection = Ent.BDD

    If Mostrar = True Then
        Frm_Super_Consultas.txtDato.Text = "%"
        Frm_Super_Consultas.Show vbModal
    Else
        Frm_Super_Consultas.ArrResultado = Frm_Super_Consultas.Buscar
    End If
    
    If Not IsEmpty(Frm_Super_Consultas.ArrResultado) Then
        If Trim(Frm_Super_Consultas.ArrResultado(0)) <> Trim(Me.Grupo.Text) Then
            Me.SubGrupo.Text = ""
            Me.Lbl_SubGrupo = ""
            Me.Lbl_SubGrupo_por = ""
        End If
        
        Me.Grupo.Text = Trim(Frm_Super_Consultas.ArrResultado(0))
        Me.Lbl_Grupo.Caption = Trim(Frm_Super_Consultas.ArrResultado(1))
        Me.Lbl_Grupo_por.Caption = Trim(Frm_Super_Consultas.ArrResultado(3))
    End If
    
    Exit Sub
    
Errores:
    
    Err.Clear
    'Ivan Espinoza 31/08/02 Final
    
End Sub

Private Sub Imagen_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbRightButton Then
        'If gObjMensajeria.Mensaje(Stellar_Mensaje(16192), True) Then '"Si desea eliminar la foto presione Aceptar.", True) Then
        Mensaje False, StellarMensaje(16192)
        If Retorno Then
            Imagen.Picture = LoadPicture()
        End If
    ElseIf Button = vbLeftButton Then
        If Me.Imagen.Picture.Handle <> 0 Then
            ShowPicturePreviewOnForm Me, Imagen.Picture
        Else
            Imagen_DblClick
        End If
    ElseIf Button = vbMiddleButton Then
        TmpPreviewPic_MouseUp vbRightButton, 0, 0, 0
    End If
End Sub

Private Sub imp_Arancel_Click()
    If ModoTouch Then TecladoAvanzado imp_Arancel
End Sub

Private Sub Imp1_Click()
    
    Static PosAnterior As Long
    
    If PosAnterior <> Imp1.ListIndex And Imp1.ListIndex = Imp1.ListCount - 1 And f_costos.Enabled Then
         PosAnterior = Imp1.ListIndex
        mClsGrupos.AgregarModificarGrupo Ent.BDD, Imp1
        mClsGrupos.CargarComboGrupos Ent.BDD, Imp1
    ElseIf PosAnterior <> Imp1.ListIndex And Imp1.ListIndex < Imp1.ListCount - 1 And f_costos.Enabled Then
        PosAnterior = Imp1.ListIndex
    End If
    
End Sub

Private Sub marca_Click()
    If ModoTouch Then TecladoAvanzado Marca
End Sub

Private Sub Modelo_Click()
    If ModoTouch Then TecladoAvanzado Modelo
End Sub

Private Sub Observacion_Click()
    If ModoTouch Then TecladoAvanzado Observacion
End Sub

Private Sub Peso_Click()
    If ModoTouch Then TecladoAvanzado Peso
End Sub

Private Sub PesoBulto_Click()
    If ModoTouch Then TecladoAvanzado PesoBulto
End Sub

Private Sub Precio1_Click()
    If ModoTouch Then TecladoAvanzado Precio1
End Sub

Private Sub Precio1_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Dim mPrecio1 As Double
    
    If KeyCode = vbKeyF2 Then
        mPrecio1 = FormatNumber(Seleccion_Tipo_Precio(Codigo, 1, , mRestringirRedondeoRegulados), 2)
        If mPrecio1 <> 0 Then
            Precio1 = mPrecio1
        End If
    End If
    
End Sub

Private Sub Precio2_Click()
    If ModoTouch Then TecladoAvanzado Precio2
End Sub

Private Sub Precio2_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Dim mPrecio2 As Double
    
    If KeyCode = vbKeyF2 Then
        mPrecio2 = FormatNumber(Seleccion_Tipo_Precio(Codigo, 2, , mRestringirRedondeoRegulados), 2)
        If mPrecio2 <> 0 Then
            Precio2 = mPrecio2
        End If
    End If
    
End Sub

Private Sub Precio3_Click()
    If ModoTouch Then TecladoAvanzado Precio3
End Sub

Private Sub Precio3_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Dim mPrecio3 As Double
    
    If KeyCode = vbKeyF2 Then
        mPrecio3 = FormatNumber(Seleccion_Tipo_Precio(Codigo, 3, , mRestringirRedondeoRegulados), 2)
        If mPrecio3 <> 0 Then
            Precio3 = mPrecio3
        End If
    End If
    
End Sub

Private Sub Presentacion_Change()
    Presentacion.Text = Left(Presentacion.Text, 8)
    Presentacion.SelLength = 0
    Presentacion.SelStart = Len(Presentacion.Text)
End Sub

Private Sub SubGrupo_Click()
    If ModoTouch Then TecladoAvanzado SubGrupo
End Sub

Private Sub SubGrupo_GotFocus()
    CodTemp = Me.SubGrupo
    If Tecla_pulsada <> True Then Tecla_pulsada = False
End Sub

Private Sub subgrupo_LostFocus()
    
    On Error GoTo Errores
    
    If SubGrupo.Text <> CodTemp And Trim(Grupo.Text) <> "" Then
        Buscar_Subgrupo " C_CODIGO = '" & Me.SubGrupo & "'", False
    ElseIf Trim(Grupo.Text) = "" Then
        Call subgrupo_KeyDown(vbKeyDelete, 0)
    End If
    
    CodTemp = ""
    
    Exit Sub
    
Errores:

    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".btn_dpto_Click", Err.Source)
    '    Unload Me
    
End Sub

Private Sub subgrupo_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF2
            Buscar_Subgrupo
'            Tabla = "ma_subgrupos"
'            Titulo = "S U B - G R U P O S"
'            Call dgs
        Case Is = vbKeyDelete
            SubGrupo.Text = ""
            Lbl_SubGrupo.Caption = ""
            lcsubgrupo = ""
    End Select
End Sub

Private Sub subgrupo_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case Is = vbKeyReturn
            oTeclado.Key_Tab
    End Select
End Sub

Private Sub Btn_SubGrupo_Click()
    
    On Error GoTo Errores
    
    Dim Condicion As String
    
    SubGrupo.SetFocus
    Buscar_Subgrupo
    'Call subgrupo_KeyDown(vbKeyF2, 0)

    Exit Sub
    
Errores:

    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".btn_subgrupo_Click", Err.Source)
    Unload Me

End Sub

Private Sub Buscar_Subgrupo(Optional Condicion As String = "", Optional Mostrar As Boolean = True)
    
    'Ivan Espinoza 31/08/02 Inicio
    
    Dim Rs As New ADODB.Recordset
    Dim SQL As String
    
    On Error GoTo Errores
    
    SQL = "select C_CODIGO, C_DESCRIPCIO, C_GRUPO,NU_PORCUTILIDAD From MA_SubGrupos"
    
    Titulo = StellarMensaje(363) '"S U B G R U P O"
    
    Frm_Super_Consultas.Inicializar SQL, Titulo, Ent.BDD
    Frm_Super_Consultas.Add_ItemLabels StellarMensaje(142), "C_CODIGO", 1860, 0 'CODIGO
    Frm_Super_Consultas.Add_ItemLabels StellarMensaje(15003), "C_DESCRIPCIO", 6495, 0 'DESCRIPCION
    Frm_Super_Consultas.Add_ItemLabels StellarMensaje(161), "C_GRUPO", 2750, 0 'GRUPO
    Frm_Super_Consultas.Add_ItemLabels "%", "NU_PORCUTILIDAD", 0, 0
    Frm_Super_Consultas.Add_ItemSearching StellarMensaje(15003), "C_DESCRIPCIO" 'DESCRIPCION
    Frm_Super_Consultas.Add_ItemSearching StellarMensaje(142), "C_CODIGO" 'CODIGO
    Frm_Super_Consultas.Add_ItemSearching StellarMensaje(161), "C_GRUPO" 'GRUPO
    
    If Trim(Me.Departamento) <> "" Then Condicion = Condicion & IIf(Len(Trim(Condicion)), " AND", "") & " c_in_departamento = '" & Trim(Me.Departamento) & "' AND c_in_grupo = '" & Trim(Me.Grupo) & "'"
    
    If Trim(Me.Grupo) <> "" Then
        If Trim(Condicion = "") Then
            Condicion = Condicion + " c_in_grupo = '" & Trim(Me.Grupo) & "'"
        Else
            Condicion = Condicion + " AND c_in_grupo = '" & Trim(Me.Grupo) & "'"
        End If
    End If
    
    Frm_Super_Consultas.StrCondicion = Condicion
    
    Set Frm_Super_Consultas.FormPadre = Me
    'Set Frm_Super_Consultas.Connection = Ent.BDD
    
    If Mostrar = True Then
        Frm_Super_Consultas.txtDato.Text = "%"
        Frm_Super_Consultas.Show vbModal
    Else
        Frm_Super_Consultas.ArrResultado = Frm_Super_Consultas.Buscar
    End If
    
    If Not IsEmpty(Frm_Super_Consultas.ArrResultado) Then
        Me.SubGrupo.Text = Trim(Frm_Super_Consultas.ArrResultado(0))
        Me.Lbl_SubGrupo.Caption = Trim(Frm_Super_Consultas.ArrResultado(1))
        Me.Lbl_SubGrupo_por.Caption = Trim(Frm_Super_Consultas.ArrResultado(3))
    End If
    
    Exit Sub
    
Errores:
    
    Err.Clear
    'Ivan Espinoza 31/08/02 Final
    
End Sub

Private Sub btn_Moneda_Click()
    
    On Error GoTo Errores
    
    DBMONEDA.SetFocus
    
    Call dbmoneda_KeyDown(vbKeyF2, 0)
    
Exit Sub

Errores:

    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".btn_moneda_Click", Err.Source)
    Unload Me

End Sub

Private Sub Btn_ProExt_Click()

'--------------------Caracteristicas extendidas-----------------

    On Error GoTo Errores
            
'           la comente porque Stellar siempre estan los campos de caracateristicas Ext
'           If MyExte.bln_Usa_ProdExt(rsProductos) Then
'               MsgBox "Opcion no puede ejecutarse por tablas"
'               Exit Sub
'           End If
        
        Set MyExte = New ClsExtendidas
        MyExte.Set_Data_Only_Connections Ent.BDD, Ent.POS
        
        Me.Codigo.Text = MyExte.Get_Codigo_Tipo_Producto(Trim(Me.Codigo.Text))
        MyExte.CodigoMoneda = Me.DBMONEDA.Text
        MyExte.intNumeroDecimales = Std_Decm
        MyExte.Plantilla_set_Father_Cod_Des Me.Codigo, Me.Descripcion, Me.Departamento.Text, Me.Lbl_Departamento.Caption, Me.Grupo.Text, Me.Lbl_Grupo.Caption, Me.SubGrupo.Text, Me.Lbl_SubGrupo.Caption, Me.CActual.Text, Me.CAnterior.Text, Me.CPromedio.Text, Me.CReposicion.Text, Precio1.Text, Precio2.Text, Precio3.Text, Val(Imp1.Text)
                
        ' COM PARA QUE SALGA EL CAMPO CANTIDAD
        
        MyExte.Show_Productos_Ext Me.txtCodCarExt, Me.Codigo, "COM"
        
        'si posee productos extendidos
        
        If MyExte.Plantilla_Had_Products(Me.Codigo) Then
            Tipo_Producto.Enabled = False
        Else
            Tipo_Producto.Enabled = True
        End If
        
        'tipo_producto.Enabled = True
        Set MyExte = Nothing
        
    Exit Sub

Errores:
    
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".btn_ProExt", Err.Source)
    Unload Me
    
End Sub

Private Sub Cactual_Change()
    Resp = CheckCad(CActual, Std_Decm, , False)
    CalculodeCostosconMerma
End Sub

Private Sub Canterior_Change()
    Resp = CheckCad(CAnterior, Std_Decm, , False)
    CalculodeCostosconMerma
End Sub

Private Sub CantidadBulto_Change()
    If Decimales = "" Then Decimales.Text = "0"
    Resp = CheckCad(CantidadBulto, CInt(Decimales), , False)
End Sub

Private Sub Cod_Alt_Click()
    If Codigo <> vbNullString Then
        Screen.MousePointer = 11
        Ficha_Productos_cod_alt.Show vbModal
    End If
End Sub

Private Sub Command1_Click()

    Dim MiConsulta As New ClsConsultas
    
    On Error GoTo GetError
    
    MiConsulta.strTitulo = "P L A N T I L L A S"
    MiConsulta.StrCadBusCod = "C_COD_PLANTILLA" 'campo de la tabla para buscar
    MiConsulta.StrCadBusDes = "Texto" 'campo de la tabla para buscar
    MiConsulta.StrOrderBy = "C_COD_PLANTILLA"
    
    MiConsulta.Consulta_Inicializar _
    "select C_COD_PLANTILLA,Texto from MA_PLANTILLAS where  TAG = 'Plantilla' ", Ent.BDD
    
    MiConsulta.Consulta_AgregarCol "CODIGO", 3800, 0
    MiConsulta.Consulta_AgregarCol "DESCRIPCION", 5800, 0
    MiConsulta.Consulta_Show
    
    Select Case MiConsulta.StrBotonPresionado
        Case "Aceptar"
            DoEvents
            a = MiConsulta.strItemC1 ' primer campo del SQL
            Me.txtCodCarExt.Text = MiConsulta.strItemC1
            Me.lblDesCarExt.Caption = MiConsulta.strItemC2
        Case "Informacion"
        Case "Proveedores"
        Case "Salir"
    End Select
    
    MiConsulta.Consulta_Hide
    
    Exit Sub
    
GetError:

    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".Command1_Click", Err.Source)
    Unload Me
    
End Sub

Private Sub Compuesto_Click()
    If Compuesto.Value = 1 Then
        btn_Compuesto.Enabled = True
        'btn_compuesto.SetFocus
    Else
        btn_Compuesto.Enabled = False
    End If
End Sub

Private Sub Command3_Click()
    
    On Error GoTo Error1
    
    If Not Len(Trim(Me.Codigo) = 0) Then
        
        Dim MyExte As New ClsExtendidas
        
        'Form_Far_Monto.C�digo(1) = "Indique el N�mero de Etiquetas"
        'mMcantidad = Form_Far_Monto.Caja_Entrar(1, True, , "Indique el N�mero de Etiquetas", False)
        
        mMcantidad = Fix(Val(QuickInputRequest(StellarMensaje(16220), True, "0", "1", "###", , vbCenter, vbCenter, vbCenter)))
        
        mCant = CDbl(mMcantidad)
        
        For i = 1 To mCant
            mSaleCiclo = MyExte.PrintLabelGen(False, Me.strNombreCompania, Descripcion.Text, Codigo.Text, IIf(bln_Etiqueta_Impresora_ConModelo, Modelo.Text, IIf(Me.txt_descripcionCorta <> "", txt_descripcionCorta.Text, Descripcion.Text)), , CInt(mCant))
            
            If mSaleCiclo Then Exit For
        Next
        
        Set MyExte = Nothing
        
    End If
    
    Exit Sub
    
Error1:

    Mensaje True, Err.Description
    
End Sub

Private Sub Cpromedio_Change()
    Resp = CheckCad(CPromedio, Std_Decm, , False)
    CalculodeCostosconMerma
End Sub

Private Sub CReposicion_Change()
    Resp = CheckCad(CReposicion, Std_Decm, , False)
    CalculodeCostosconMerma
End Sub

Private Sub CReposicion_GotFocus()
    CReposicion.SelStart = 0
    CReposicion.SelLength = Len(CPromedio)
End Sub

Private Sub CReposicion_LostFocus()
    If CReposicion.Text = "" Then CReposicion.Text = 0
    CReposicion = FormatNumber(CDbl(CReposicion), Std_Decm)
End Sub

Private Sub dbmoneda_KeyDown(KeyCode As Integer, Shift As Integer)
    
    On Error GoTo Errores
    
    Select Case KeyCode
        Case Is = vbKeyF2
            Call CONSULTA_F2(Ficha_ProductosLite, "MONEDA_PRODUCTOS", "MONEDAS")

        Case Is = vbKeyEscape
            Call Form_KeyDown(vbKeyF7, 0)
    End Select

    Exit Sub
Errores:

    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".btn_dpto_Click", Err.Source)
    Unload Me
    
End Sub

Private Sub dbmoneda_LostFocus()

    On Error GoTo Errores
    
    If DBMONEDA.Text <> "" Then
        If Tecla_pulsada = False Then
            Call Apertura_Recordset(rsEureka)
            
            rsEureka.Open "select * from ma_monedas where c_codmoneda = '" & DBMONEDA.Text & "'", _
            Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
            
            If Not rsEureka.EOF Then
                msk_factor.Text = FormatNumber(rsEureka!n_factor, 2)
                lbl_moneda.Caption = rsEureka!c_descripcion
                Std_Decm = rsEureka!n_decimales
                Call Cerrar_Recordset(rsEureka)
'                decimales.SetFocus
            Else
                Call Cerrar_Recordset(rsEureka)
                DBMONEDA.Text = ""
                lbl_moneda.Caption = ""
                msk_factor.Text = FormatNumber(0, 2)
                Call Mensaje(True, Stellar_Mensaje(16193)) '"El c�digo de la moneda no existe.")
                DBMONEDA.SetFocus
            End If
            
            Call Reformatear_Campos
        End If
    End If
    
    Exit Sub
    
Errores:

    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".dbmoneda_LosatFocus", Err.Source)
    Unload Me
    
End Sub

Private Sub Decimales_Change()
    Resp = CheckInt(Decimales)
End Sub

Private Sub Decimales_GotFocus()
    Decimales.SelStart = 0
    Decimales.SelLength = Len(Decimales)
End Sub

Private Sub Decimales_LostFocus()
    If Decimales = "" Then
        Decimales = 0
    End If
    
    CantidadBulto = FormatNumber(CDbl(CantidadBulto), CInt(Decimales))
End Sub

Private Sub Form_Activate()
    Tipo_Producto_LostFocus
    'Call Apertura_Recordset(rsProductos)
End Sub

Private Sub Form_Load()

    BuscadorProductos_FichaProductosSimple = True
    
    Call AjustarPantalla(Me)
    
    lbl_Organizacion.Caption = StellarMensaje(1235) 'Titulo
    
    Toolbar1.Buttons(1).Caption = Stellar_Mensaje(197) 'agregar
    Toolbar1.Buttons(1).ButtonMenus(1).Text = Stellar_Mensaje(197) 'agregar
    Toolbar1.Buttons(1).ButtonMenus(2).Text = Stellar_Mensaje(3020) 'Agregar Importar
    Toolbar1.Buttons(1).ButtonMenus(3).Text = Stellar_Mensaje(3013) 'Importar Archivo
    Toolbar1.Buttons(2).Caption = Stellar_Mensaje(102) 'buscar
    Toolbar1.Buttons(3).Caption = Stellar_Mensaje(207) 'modificar
    Toolbar1.Buttons(5).Caption = Stellar_Mensaje(6) 'cancelar
    Toolbar1.Buttons(6).Caption = Stellar_Mensaje(208) 'borrar
    Toolbar1.Buttons(7).Caption = Stellar_Mensaje(103) 'grabar
    Toolbar1.Buttons(9).Caption = Stellar_Mensaje(107) 'salir
    
    Sstab1.TabCaption(0) = Stellar_Mensaje(3019) 'general
    Sstab1.TabCaption(1) = Stellar_Mensaje(2516) 'detalle
    
    f_general.Caption = Stellar_Mensaje(186) 'datos generales
    C�digo(0).Caption = Stellar_Mensaje(142) 'codigo
    C�digo(6).Caption = Stellar_Mensaje(143) 'descripcion
    C�digo(46).Caption = Stellar_Mensaje(3021) 'descripcion corta
    C�digo(5).Caption = Stellar_Mensaje(3022) 'marca
    C�digo(13).Caption = Stellar_Mensaje(222) 'modelo
    C�digo(28).Caption = Stellar_Mensaje(3023) 'tipo de producto
    C�digo(38).Caption = Stellar_Mensaje(3024) 'insumo
    chk_insumo.Caption = Stellar_Mensaje(3025) 'si
    C�digo(32).Caption = Stellar_Mensaje(2019) 'status
    Activo.Caption = Stellar_Mensaje(165) 'activo
    
    C�digo(62).Caption = Stellar_Mensaje(3026) ' Imagen
    C�digo(63).Caption = Stellar_Mensaje(3027) ' Doble Clic para cambiar
    C�digo(74).Caption = Stellar_Mensaje(3135) ' Clic Derecho para eliminar
    
    C�digo(2).Caption = Stellar_Mensaje(3028) 'departamento
    C�digo(3).Caption = Stellar_Mensaje(161) 'grupo
    C�digo(4).Caption = Stellar_Mensaje(3029) 'subgrupo
    C�digo(57).Caption = Stellar_Mensaje(3030) 'utilidad
    
    C�digo(33).Caption = Stellar_Mensaje(3031) 'plantilla producto extendido
    
    C�digo(26).Caption = Replace(Stellar_Mensaje(134), ":", "") 'moneda
    C�digo(18).Caption = Stellar_Mensaje(3032) 'costo actual
    C�digo(19).Caption = Stellar_Mensaje(3033) 'costo anterior
    C�digo(20).Caption = Stellar_Mensaje(3034) 'costo promedio
    C�digo(11).Caption = Stellar_Mensaje(3035) 'costo reposicion
    C�digo(40).Caption = Replace(Stellar_Mensaje(133), ":", "") 'costo
    C�digo(41).Caption = "+ " & Stellar_Mensaje(3036) '+ merma
    C�digo(39).Caption = Stellar_Mensaje(3037) '% de merma
    
    chk_PrecioRegulado.Caption = Stellar_Mensaje(3038) 'precio regulado
    C�digo(23).Caption = Stellar_Mensaje(3039) & " 1" 'precio de venta 1
    C�digo(24).Caption = Stellar_Mensaje(3039) & " 2" 'precio de venta 2
    C�digo(25).Caption = Stellar_Mensaje(3039) & " 3" 'precio de venta 3
    C�digo(17).Caption = Stellar_Mensaje(3040) 'precio de oferta
    C�digo(51).Caption = Stellar_Mensaje(3041) 'precio
    C�digo(52).Caption = Stellar_Mensaje(3030) 'utilidad
    C�digo(53).Caption = Stellar_Mensaje(3042) 'sin merma
    C�digo(54).Caption = Stellar_Mensaje(3043) 'con merma
    C�digo(55).Caption = Stellar_Mensaje(140) 'impuestos
    C�digo(50).Caption = Stellar_Mensaje(3044) 'vigencia de la oferta
    C�digo(34).Caption = Stellar_Mensaje(3045) 'desde
    C�digo(35).Caption = Stellar_Mensaje(3046) 'hasta
    
    C�digo(61).Caption = Stellar_Mensaje(3047) 'datos obligatorios
    C�digo(36).Caption = Stellar_Mensaje(3048) 'fecha de creacion
    C�digo(37).Caption = Stellar_Mensaje(3049) 'ultima actualizacion
    Cod_Alt.Caption = Stellar_Mensaje(3050) 'codigos alternos
    Proveedor.Caption = Stellar_Mensaje(66) 'proveedores
    btn_Compuesto.Caption = Stellar_Mensaje(3051) 'partes
    Btn_ProExt.Caption = Stellar_Mensaje(3052) 'p.extendidos
    Command3.Caption = Stellar_Mensaje(3053) 'imp.etiquetas
    cmd_Mascara.Caption = Stellar_Mensaje(3054) 'ubicacion
    
    C�digo(71).Caption = Stellar_Mensaje(3055) 'datos del producto
    C�digo(64).Caption = Stellar_Mensaje(3056) 'procedencia
    Label7.Caption = Stellar_Mensaje(3057) 'stock minimo
    Label6.Caption = Stellar_Mensaje(3058) 'stock maximo
    C�digo(27).Caption = Stellar_Mensaje(3059) 'numero de decimales
    
    C�digo(70).Caption = Stellar_Mensaje(3060) 'datos de la unidad
    C�digo(29).Caption = Stellar_Mensaje(3061) 'unidad de medida
    C�digo(10).Caption = Stellar_Mensaje(3062) 'peso de la unidad
    C�digo(12).Caption = Stellar_Mensaje(3063) 'volumen de la unidad
    
    C�digo(69).Caption = Stellar_Mensaje(3064) 'datos del empaque
    C�digo(15).Caption = Stellar_Mensaje(3065) 'unidades por empaque
    C�digo(30).Caption = Stellar_Mensaje(3066) 'peso del empaque
    C�digo(31).Caption = Stellar_Mensaje(3067) 'volumen del empaque
    
    C�digo(67).Caption = Stellar_Mensaje(3068) 'configuracion para punto de venta
    C�digo(65).Caption = Stellar_Mensaje(3069) 'nivel minimo de usuario para venta en pos
    chk_Cantidad.Caption = Stellar_Mensaje(3070) 'permite cantidad
    chk_Teclado.Caption = Stellar_Mensaje(3071) 'permite teclado
    
    C�digo(68).Caption = Stellar_Mensaje(3072) 'datos de importacion
    Label2.Caption = Stellar_Mensaje(3073) 'codigo arancelario
    Label3.Caption = Stellar_Mensaje(143) 'descripcion
    Label8.Caption = Stellar_Mensaje(3074) 'costo original
    Label4.Caption = Stellar_Mensaje(140) 'impuesto
    
    C�digo(72).Caption = Stellar_Mensaje(3133) 'Codigo de Barra
    C�digo(73).Caption = Stellar_Mensaje(3134) 'Inventario
    
    C�digo(66).Caption = Replace(Stellar_Mensaje(137), ":", "") 'observacion

    mNoRedondear = BuscarReglaNegocioBoolean("Productos_NoRedondear", "1")
    CANT = Me.Width / (Len(Titulo) + Len(Stellar))
    CANT = CANT / 4
    'Me.Caption = Stellar & Space(CANT) & Titulo

    mGrabar = ""
    
    Set Forma = Ficha_ProductosLite
    
    Screen.MousePointer = 0
    mClsGrupos.cTipoGrupo = "IMP"
    mClsGrupos.CargarComboGrupos Ent.BDD, Me.Imp1
    
    Call Habilitar_Datos(False)
'    Call BUSCAR_IMAGEN
    Call buscar_moneda(DBMONEDA, lbl_moneda, msk_factor, True, , True, True)
    '992
        
    Procedencia.Text = Procedencia.List(0)
    mCantidadDecimales = 2
    
    'Me.IntTipoUnidad = 0
    'Me.IntTipoPesado = 1
    'Me.IntTipoPesable = 2
    'Me.IntTipoCarExt = 3
    'Me.IntTipoInformativo = 4
    'Me.IntTipoCompuesto = 5
    
    ' Ahora utilizando dichas variables en mod_EnumeracionesVarias
    
    ' Traduccion Tipo_Producto (ComboBox)
    
    With Tipo_Producto
        
        .Clear
        
        .AddItem gTipoProducto.StrTipoUnidad, IntTipoUnidad
        .AddItem gTipoProducto.StrTipoPesado, IntTipoPesado
        .AddItem gTipoProducto.StrTipoPesable, IntTipoPesable
        .AddItem gTipoProducto.StrTipoCarExt, intTipoCarExt
        .AddItem gTipoProducto.StrTipoInformativo, IntTipoInformativo
        .AddItem gTipoProducto.StrTipoCompuesto, IntTipoCompuesto
        
        .Text = .List(0)
        
    End With
    
    Dim Rs As New ADODB.Recordset
    
    Call Apertura_Recordset(Rs)
    
    Rs.Open "select * from estruc_sis", _
    Ent.BDD, adOpenDynamic, adLockReadOnly, adCmdText
    
    Me.strNombreCompania = Rs!nom_org
    Rs.Close
    
    Select Case Tipo_Comercio
        Case "LIBRERIA"
            C�digo(5) = Stellar_Mensaje(3091) '"Autor"
            C�digo(13) = Stellar_Mensaje(3092) '"Editorial"
        Case Else
            C�digo(5) = Stellar_Mensaje(3022) '"Marca"
            C�digo(13) = Stellar_Mensaje(222) '"Modelo"
    End Select
    
   
    cmb_NivelProducto.Clear
    
    For mConta_i = 0 To 9
        Me.cmb_NivelProducto.AddItem mConta_i
    Next
    
    Me.cmb_NivelProducto.ListIndex = 0
    Me.Frame9.Visible = gCambioHechos
    Me.chk_insumo.Visible = gCambioHechos
    Me.chk_PrecioRegulado.Visible = gCambioHechos
    Me.Frame10.Visible = gCambioHechos
    C�digo(38).Visible = gCambioHechos
    
    mPermitirCostoxImpuesto = BuscarReglaNegocioBoolean("FichaPro_PermitirCostoxImpuesto", "1")
    'mRestringirRedondeoRegulados = BuscarReglaNegocioBoolean("RestringirRedondeoRegulados", "1")
    
    If AjustesInformativos_GuiadeLicores Then
        C�digo(12).Caption = Stellar_Mensaje(3093) '"Volumen Und. / Capacidad (l)"
        C�digo(13).Caption = Stellar_Mensaje(3094) '"Modelo / Grado Alcoh�lico"
        Label3.Caption = Stellar_Mensaje(3095) '"Fabricante"
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    'On Error GoTo Errores
    
    If Shift = vbAltMask Then
        Select Case KeyCode
            Case Is = vbKeyE
                Call Form_KeyDown(vbKeyF3, 0)
            Case Is = vbKeyB
                Call Form_KeyDown(vbKeyF2, 0)
            Case Is = vbKeyM
                Call Form_KeyDown(vbKeyF5, 0)
            Case Is = vbKeyC
                Call Form_KeyDown(vbKeyF7, 0)
            Case Is = vbKeyR
                Call Form_KeyDown(vbKeyF6, 0)
            Case Is = vbKeyG
                Call Form_KeyDown(vbKeyF4, 0)
            Case Is = vbKeyS
                Call Form_KeyDown(vbKeyF12, 0)
        End Select
    Else
        Select Case KeyCode
            Case Is = vbKeyReturn
                KeyAscii = 0
                oTeclado.Key_Tab
            
            Case Is = vbKeyF1
                'Llamar Ayuda
                
            Case Is = vbKeyF2
                If Toolbar1.Buttons(2).Enabled = False Then Exit Sub
                'Set forma = Ficha_ProductosLite
                Call buscar_registro
                mPrecioInicial = 0
                KeyCode = -1
                
            Case Is = vbKeyF3
                If Toolbar1.Buttons(1).Enabled = True Then
                    Call agregar_registro
                    mPrecioInicial = 0
                End If
            
            Case Is = vbKeyF4
                'MsgBox Imagen.Picture.Handle
                If Toolbar1.Buttons(7).Enabled = True Then
                    Call grabar_registro
                    mPrecioInicial = 0
                End If
            
            Case Is = vbKeyF5
                If Toolbar1.Buttons(3).Enabled = True Then
                    Call modificar_registro
                    mPrecioInicial = 0
                End If
            
            Case Is = vbKeyF6
                If Toolbar1.Buttons(6).Enabled = True Then
                    Call eliminar_registro
                    mPrecioInicial = 0
                End If
            
            Case Is = vbKeyF7
                Toolbar1.Buttons(3).Enabled = False
                Call Limpiar_Datos
                mPrecioInicial = 0
                If Toolbar1.Buttons(5).Enabled = True Then Call cancelar_registro
            
            Case Is = vbKeyF12 'F12
                Set Ficha_ProductosLite = Nothing
                Unload Me
        End Select
    End If
    
    Exit Sub
    
Errores:
    
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".Form_KeyDown", Err.Source)
    Unload Me
    
End Sub

Private Sub Limpiar_Datos()

    fImportarPro = False
    Set fClsProCom = Nothing
    Me.chk_PrecioRegulado.Value = 0
    Me.chk_insumo.Value = 0
    txt_PorcentajeMerma = FormatNumber(0, 2)
    Me.txt_descripcionCorta = ""
    Me.cmb_NivelProducto.ListIndex = 0
   
    Codigo.Text = ""
    Descripcion.Text = ""
    Me.Command3.Enabled = False
    
    Tipo_Producto.Text = Tipo_Producto.List(0)
    Activo.Value = 1
    Departamento.Text = ""
    Lbl_Departamento.Caption = ""
    Lbl_Departamento_por.Caption = ""
    lcDepartamento = ""
    
    Grupo.Text = ""
    Lbl_Grupo.Caption = ""
    Lbl_Grupo_por.Caption = ""
    lcGrupo = ""
    
    SubGrupo.Text = ""
    Lbl_SubGrupo.Caption = ""
    Lbl_SubGrupo_por.Caption = ""
    lcsubgrupo = ""
    
    Marca.Text = ""
    Modelo = ""
    
    OfertaDesde.Value = "01/01/1980 00:00:00"
    OfertaHasta.Value = "01/01/1980 23:59:59"
    Ingresado.Value = "01/01/1980 00:00:00"
    Actualizado.Value = "01/01/1980 23:59:59"
    
    ' Alternos.Text = "" ************* AVERIGUAR ***************
    
    Procedencia.Text = Procedencia.List(0)
    Me.txt_PorcentajeMerma.Text = FormatNumber(0, 2)
    CActual.Text = FormatNumber(0, 2)
    CAnterior.Text = FormatNumber(0, 2)
    CPromedio.Text = FormatNumber(0, 2)
    CReposicion.Text = FormatNumber(0, 2)
    CActual_Merma.Text = FormatNumber(0, 2)
    CAnterior_Merma.Text = FormatNumber(0, 2)
    CPromedio_Merma.Text = FormatNumber(0, 2)
    CReposicion_merma.Text = FormatNumber(0, 2)
    Precio1.Text = FormatNumber(0, 2)
    Margen1.Caption = FormatNumber(0, 2)
    Precio2.Text = FormatNumber(0, 2)
    Margen2.Caption = FormatNumber(0, 2)
    Precio3.Text = FormatNumber(0, 2)
    Margen3.Caption = FormatNumber(0, 2)
    PrecioO.Text = FormatNumber(0, 2)
    MargenO.Caption = FormatNumber(0, 2)
     
    Margen1C.Caption = FormatNumber(0, 2)
    Margen2C.Caption = FormatNumber(0, 2)
    Margen3C.Caption = FormatNumber(0, 2)
    MargenOC.Caption = FormatNumber(0, 2)
    BuscarIndiceComboImpuesto (0)
    
    'imp1.Text = FormatNumber(0, 2)
    Imp2.Text = FormatNumber(0, 2)
    Imp3.Text = FormatNumber(0, 2)
    Seriales.Value = 0
    chk_Cantidad.Value = 0
    chk_Teclado.Value = 0

    'Compuesto.value = 0
    Observacion.Text = ""
    Presentacion.Text = ""
    Peso.Text = FormatNumber(0, 3)
    Volumen.Text = FormatNumber(0, 3)
    CantidadBulto.Text = "1"
    PesoBulto.Text = FormatNumber(0, 3)
    VolumenBulto.Text = FormatNumber(0, 3)
    Imagen.Picture = LoadPicture()
    KillSecure TmpImgFileName
    
    Me.txtCodCarExt = ""
    Me.lblDesCarExt = ""
    
    Me.BarCode.Text = ""
    
End Sub

Public Sub Habilitar_Datos(Hab_Des As Boolean)

    f_costos.Enabled = Hab_Des
    f_general.Enabled = Hab_Des
    'f_ubicacion.Enabled = hab_desm
    f_costos.Enabled = Hab_Des
    f_Impuestos.Enabled = Hab_Des
    f_precios.Enabled = Hab_Des
    'f_seriales.Enabled = hab_des
    'f_unidad.Enabled = hab_des
    'f_empaque.Enabled = hab_des
    f_observacion.Enabled = Hab_Des
    'f_pesarpor.Enabled = hab_des
    
    'If xunidad(2).value = True Then
        'Load ficha_productos_compuesto
        'Compuesto.Enabled = True
    'End If

    C�digo(73).Visible = True
    Existencia.Visible = True
    
    If mGrabar = "update" Or mGrabar = "" Then
        Tipo_Producto.Enabled = False
        C�digo(73).Visible = False
        Existencia.Visible = False
    End If
    
    'tipo_producto.Enabled = True
    
    Me.Descripcion.Enabled = Hab_Des
    
    If mGrabar = "update" Then
        Me.Codigo.Enabled = False
    Else
        Me.Codigo.Enabled = Hab_Des
    End If
    
    Me.Marca.Enabled = Hab_Des
    Me.Frame4.Enabled = Hab_Des
    Me.Modelo.Enabled = Hab_Des
    Me.Activo.Enabled = Hab_Des
    
    Departamento.Enabled = Hab_Des
    Me.Grupo.Enabled = Hab_Des
    SubGrupo.Enabled = Hab_Des
    Btn_Dpto.Enabled = Hab_Des
    Btn_Grupo.Enabled = Hab_Des
    Btn_SubGrupo.Enabled = Hab_Des
    FrmDepGruSub.Enabled = Hab_Des
    Imagen.Enabled = Hab_Des
    
    'unidad.Enabled = hab_des
    'Directorio.Enabled = hab_des
    'File.Enabled = hab_des

    'cod_Arancel.Enabled = Hab_Des
    'des_Arancel.Enabled = Hab_Des
    'imp_Arancel.Enabled = Hab_Des
    
    'Costo_Original.Enabled = Hab_Des
    
    Toolbar1.Buttons(1).Enabled = IIf(Hab_Des = True, False, True)
    Toolbar1.Buttons(2).Enabled = IIf(Hab_Des = True, False, True)
    Toolbar1.Buttons(5).Enabled = IIf(Hab_Des = False, False, True)
    Toolbar1.Buttons(6).Enabled = IIf(Hab_Des = False, False, True)
    Toolbar1.Buttons(7).Enabled = IIf(Hab_Des = False, False, True)
    Toolbar1.Buttons(10).Enabled = IIf(Hab_Des = True, True, False)
    
    Me.chk_PrecioRegulado.Enabled = Hab_Des
    Me.chk_insumo.Enabled = Hab_Des
    Me.txt_PorcentajeMerma.Enabled = Hab_Des
    Me.txt_descripcionCorta.Enabled = Hab_Des
    Me.cmb_NivelProducto.Enabled = Hab_Des
    
    Me.BarCode.Enabled = Hab_Des
    C�digo(72).Enabled = Hab_Des
    
End Sub

Public Sub Cargar_Campos(Optional pRsProductos As ADODB.Recordset)
    
    On Error GoTo Errores
    
    If pRsProductos Is Nothing Then Exit Sub
    If Not pRsProductos.BOF Then
    
    Dim MyExte As New ClsExtendidas
    
    If MyExte.bln_Usa_ProdExt(pRsProductos) Then
        Me.ccodplan = pRsProductos!C_COD_PLANTILLA
        Me.ccodproext = pRsProductos!c_codigo
        Me.txtCodCarExt = Me.ccodplan
        ' 14/09/2004
        Call Apertura_RecordsetC(rsEureka)
        rsEureka.Open "select * from ma_PLANTILLAS where C_COD_PLANTILLA = '" & pRsProductos!C_COD_PLANTILLA & "'", Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        Set rsEureka.ActiveConnection = Nothing
        If Not rsEureka.EOF Then
            Me.lblDesCarExt = rsEureka!texto
        End If
    End If
    
    If Not fImportarPro Then
        Codigo = pRsProductos!c_codigo
    End If
    
    Descripcion = pRsProductos!c_descri
    
    'Ivan Espinoza 28/09/02
    
       Me.f_general.Enabled = True
       Me.Command3.Enabled = True
       
       '*******************
       '****** DEPARTAMENTO
       '*******************
       
       ' 14/09/2004
       'Call apertura_recordsetc(rseureka)
       
       Call Apertura_Recordset(rsEureka)
       
       rsEureka.Open "select * from ma_departamentos where c_codigo = '" & pRsProductos!c_departamento & "'", _
       Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
       
       Set rsEureka.ActiveConnection = Nothing
       
       If Not rsEureka.EOF Then
           Departamento = rsEureka!c_codigo
           Lbl_Departamento = rsEureka!c_descripcio
           Lbl_Departamento_por = rsEureka!NU_PORCUTILIDAD
           lcDepartamento = rsEureka!c_codigo
       Else
           Departamento = ""
           Lbl_Departamento = ""
           lcDepartamento = ""
           Lbl_Departamento_por = ""
       End If
       
       '******************
       '****** GRUPO
       '******************
       
       ' 14/09/2004
       'Call apertura_recordsetc(rseureka)
       
       Call Apertura_Recordset(rsEureka)
       
       rsEureka.Open "select * from ma_grupos where c_codigo = '" & pRsProductos!c_grupo & "' AND C_DEPARTAMENTO = '" & lcDepartamento & "'", _
       Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
       
       Set rsEureka.ActiveConnection = Nothing
       
       If Not rsEureka.EOF Then
           Grupo = rsEureka!c_codigo
           Lbl_Grupo = rsEureka!c_descripcio
           lcGrupo = rsEureka!c_codigo
           lcDptoGrupo = rsEureka!c_departamento
           Lbl_Grupo_por = rsEureka!NU_PORCUTILIDAD
       Else
           Grupo = ""
           Lbl_Grupo = ""
           lcGrupo = ""
           lcDptoGrupo = ""
           Lbl_Grupo_por = ""
       End If
       
       '******************
       '****** SUB-GRUPO
       '******************
       
      ' 14/09/2004
       'Call apertura_recordsetc(rseureka)
       
       Call Apertura_Recordset(rsEureka)
       
       rsEureka.Open "select * from ma_subgrupos where c_codigo = '" & pRsProductos!c_subgrupo & "' AND C_IN_GRUPO = '" & pRsProductos!c_grupo & "' AND C_IN_DEPARTAMENTO = '" & pRsProductos!c_departamento & "'", _
       Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
       
       Set rsEureka.ActiveConnection = Nothing
       
       If Not rsEureka.EOF Then
           SubGrupo = rsEureka!c_codigo
           Lbl_SubGrupo = rsEureka!c_descripcio
           lcsubgrupo = rsEureka!c_codigo
           Lbl_SubGrupo_por = rsEureka!NU_PORCUTILIDAD
       Else
           SubGrupo = ""
           Lbl_SubGrupo = ""
           lcsubgrupo = ""
           Lbl_SubGrupo_por = ""
       End If
       
       '********************************
       '****** Moneda
       '********************************
       
       'Call apertura_recordsetc(rseureka)
       
       Call Apertura_Recordset(rsEureka)
       
       rsEureka.Open "select * from ma_monedas where c_codmoneda = '" & pRsProductos!C_CODMONEDA & "'", _
       Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
       
       Set rsEureka.ActiveConnection = Nothing
       
       If Not rsEureka.EOF Then
           msk_factor.Text = FormatNumber(rsEureka!n_factor, 2)
           DBMONEDA.Text = rsEureka!C_CODMONEDA
           lbl_moneda.Caption = rsEureka!c_descripcion
           Std_Decm = rsEureka!n_decimales
       Else
           Call Mensaje(True, Stellar_Mensaje(16194)) '"La moneda del producto no existe.")
           Std_Decm = 2
       End If
       
       Call Cerrar_Recordset(rsEureka)
       
       '************************
       'POSICIONES DECIMALES
       '************************
       
       Decimales = pRsProductos!CANT_DECIMALES
       
       '******************
       'MODELO
       '******************
       
       Modelo = IIf(IsNull(pRsProductos!c_modelo), " ", pRsProductos!c_modelo)
       
       '******************
       'MARCA
       '******************
       
       Marca = IIf(IsNull(pRsProductos!c_marca), " ", pRsProductos!c_marca)
       
       ' Alternos = rec("") ********* averiguar ****************
       
       '******************
       'PROCEDENCIA
       '******************
       
       If pRsProductos!c_procede = False Then
           Procedencia.Text = Trim(Procedencia.List(0))
       Else
           Procedencia.Text = Trim(Procedencia.List(1))
       End If
       
       'Sstab1.TabEnabled(1) = IIf(prsProductos!c_procede = 0, False, True)
       
       '******************
       'IMPUESTOS
       '******************
       
       'imp1 = FormatNumber(CDbl(prsProductos!N_Impuesto1), 2)
       
       BuscarIndiceComboImpuesto (CDbl(pRsProductos!n_impuesto1))
       Imp2 = FormatNumber(CDbl(pRsProductos!N_Impuesto2), 2)
       Imp3 = FormatNumber(CDbl(pRsProductos!N_Impuesto3), 2)
       
       '*********************************************************
       
       '******************
       'COSTOS
       '******************
       
       CActual = FormatNumber(CDbl(pRsProductos!N_CostoAct), Std_Decm)
       CAnterior = FormatNumber(CDbl(pRsProductos!N_CostoAnt), Std_Decm)
       CPromedio = FormatNumber(CDbl(pRsProductos!N_CostoPro), Std_Decm)
       CReposicion = FormatNumber(CDbl(pRsProductos!N_Costorep), Std_Decm)
       
       '******************
       'PRECIOS
       '******************
       
       Precio1 = FormatNumber(CDbl(pRsProductos!n_precio1), Std_Decm)
       Precio2 = FormatNumber(CDbl(pRsProductos!N_Precio2), Std_Decm)
       Precio3 = FormatNumber(CDbl(pRsProductos!N_Precio3), Std_Decm)
       PrecioO = FormatNumber(CDbl(pRsProductos!n_precioo), 2)
       
       If pRsProductos!n_precioo <> 0 And Now >= CDate(Format(pRsProductos!f_inicial, "dd/MM/yyyy") & " " & Format(pRsProductos!H_inicial, "HH:mm:ss")) _
           And Now <= CDate(Format(pRsProductos!f_final, "dd/MM/yyyy") & " " & Format(pRsProductos!H_final, "HH:mm:ss")) Then
           OfertaDesde.Value = FormatDateTime(pRsProductos!f_inicial, vbShortDate) & " " & FormatDateTime(pRsProductos!H_inicial, vbShortTime)
           OfertaHasta.Value = FormatDateTime(pRsProductos!f_final, vbShortDate) & " " & FormatDateTime(pRsProductos!H_final, vbShortTime)
       Else
           OfertaDesde.Value = Format("01/01/1980 00:00:00", "dd/MM/yyyy HH:mm:ss")
           OfertaHasta.Value = Format("01/01/1980 23:59:59", "dd/MM/yyyy HH:mm:ss")
       End If
       
       Ingresado.Value = Format(pRsProductos!add_date, "short date")
       Actualizado.Value = Format(pRsProductos!update_date, "short date")
       
       If gCambioHechos Then
           Me.chk_PrecioRegulado.Value = pRsProductos!nu_precioregulado
           Me.chk_insumo.Value = pRsProductos!nu_insumointerno
           Me.txt_PorcentajeMerma = pRsProductos!nu_pocentajemerma
           Me.txt_descripcionCorta = pRsProductos!cu_descripcion_corta
           Me.cmb_NivelProducto.ListIndex = pRsProductos!nu_nivelClave
       End If
       
       Ent.rsReglasComerciales.Requery
       
       Select Case Ent.rsReglasComerciales!Estimacion_PV
           '**** COSTO ACTUAL
           Case Is = 0
           
               'If CDbl(Precio1) <> 0 Then Margen1 = FormatNumber((((CDbl(Cactual) / CDbl(Precio1)) * 100) - 100) * -1, 2)
               'If CDbl(Precio2) <> 0 Then Margen2 = FormatNumber((((CDbl(Cactual) / CDbl(Precio2)) * 100) - 100) * -1, 2)
               'If CDbl(Precio3) <> 0 Then Margen3 = FormatNumber((((CDbl(Cactual) / CDbl(Precio3)) * 100) - 100) * -1, 2)
               ' Nueva Rutina para distintas formas de calculo. Igual m�s adelante con los otros costos.
           
               Margen1 = FormatNumber(CalcularPrecios(Precio_CalculoPorcentaje, CDbl(CActual), CDbl(Precio1), False), 2)
               Margen2 = FormatNumber(CalcularPrecios(Precio_CalculoPorcentaje, CDbl(CActual), CDbl(Precio2), False), 2)
               Margen3 = FormatNumber(CalcularPrecios(Precio_CalculoPorcentaje, CDbl(CActual), CDbl(Precio3), False), 2)
               
'                If prsProductos!N_PrecioO <> 0 And Date >= prsProductos!F_Inicial _
'                    And Date <= prsProductos!F_Final Then
'                    MargenO = FormatNumber((((CDbl(Cactual) / CDbl(PrecioO)) * 100) - 100) * -1, 2)
'                Else
'                    MargenO = FormatNumber(0, 2)
'                End If
               ' Nueva Rutina para distintas formas de calculo. Igual m�s adelante con los otros costos.
               
               If Date >= pRsProductos!f_inicial And Date <= pRsProductos!f_final Then
                   MargenO = FormatNumber(CalcularPrecios(Precio_CalculoPorcentaje, CDbl(CActual), CDbl(PrecioO), False), 2)
               End If
               
               CostoTrabajar = CActual_Merma
               
           '**** COSTO ANTERIOR
           
           Case Is = 1
           
               Margen1 = FormatNumber(CalcularPrecios(Precio_CalculoPorcentaje, CDbl(CAnterior), CDbl(Precio1), False), 2)
               Margen2 = FormatNumber(CalcularPrecios(Precio_CalculoPorcentaje, CDbl(CAnterior), CDbl(Precio2), False), 2)
               Margen3 = FormatNumber(CalcularPrecios(Precio_CalculoPorcentaje, CDbl(CAnterior), CDbl(Precio3), False), 2)
               
               If Date >= pRsProductos!f_inicial And Date <= pRsProductos!f_final Then
                   MargenO = FormatNumber(CalcularPrecios(Precio_CalculoPorcentaje, CDbl(CAnterior), CDbl(PrecioO), False), 2)
               End If
               
               CostoTrabajar = CAnterior_Merma
           
           '**** COSTO PROMEDIO
           
           Case Is = 2
           
               Margen1 = FormatNumber(CalcularPrecios(Precio_CalculoPorcentaje, CDbl(CPromedio), CDbl(Precio1), False), 2)
               Margen2 = FormatNumber(CalcularPrecios(Precio_CalculoPorcentaje, CDbl(CPromedio), CDbl(Precio2), False), 2)
               Margen3 = FormatNumber(CalcularPrecios(Precio_CalculoPorcentaje, CDbl(CPromedio), CDbl(Precio3), False), 2)
               
               If Date >= pRsProductos!f_inicial And Date <= pRsProductos!f_final Then
                   MargenO = FormatNumber(CalcularPrecios(Precio_CalculoPorcentaje, CDbl(CPromedio), CDbl(PrecioO), False), 2)
               End If
               
               CostoTrabajar = CPromedio_Merma
           
           '**** COSTO REPOSICION
           
           Case Is = 3
           
               Margen1 = FormatNumber(CalcularPrecios(Precio_CalculoPorcentaje, CDbl(CReposicion), CDbl(Precio1), False), 2)
               Margen2 = FormatNumber(CalcularPrecios(Precio_CalculoPorcentaje, CDbl(CReposicion), CDbl(Precio2), False), 2)
               Margen3 = FormatNumber(CalcularPrecios(Precio_CalculoPorcentaje, CDbl(CReposicion), CDbl(Precio3), False), 2)
               
               If Date >= pRsProductos!f_inicial And Date <= pRsProductos!f_final Then
                   MargenO = FormatNumber(CalcularPrecios(Precio_CalculoPorcentaje, CDbl(CReposicion), CDbl(PrecioO), False), 2)
               End If
               
               CostoTrabajar = CReposicion_merma
               
       End Select
       
       ' CREO QUE SOLO HACE FALTA SABER EL COSTO POR ESO NO LO METO ARRIBA
       ' y es el calculo de costo mas merma

       Margen1C = FormatNumber(CalcularPrecios(Precio_CalculoPorcentaje, CDbl(CostoTrabajar), CDbl(Precio1), False), 2)
       Margen2C = FormatNumber(CalcularPrecios(Precio_CalculoPorcentaje, CDbl(CostoTrabajar), CDbl(Precio2), False), 2)
       Margen3C = FormatNumber(CalcularPrecios(Precio_CalculoPorcentaje, CDbl(CostoTrabajar), CDbl(Precio3), False), 2)
       
       If Date >= pRsProductos!f_inicial And Date <= pRsProductos!f_final Then
           MargenOC = FormatNumber(CalcularPrecios(Precio_CalculoPorcentaje, CDbl(CostoTrabajar), CDbl(PrecioO), False), 2)
       End If
              
'        '******************
'        'IMPUESTOS
'        '******************

       If ExisteCampoTabla("bs_permiteteclado", pRsProductos) Then
           chk_Teclado = IIf(pRsProductos!bs_permiteteclado, 1, 0)
       End If
       
       If ExisteCampoTabla("bs_permitecantidad", pRsProductos) Then
           chk_Cantidad = IIf(pRsProductos!bs_permitecantidad, 1, 0)
       End If
       
       '*********************************************************
       Seriales = IIf(pRsProductos!C_SERIALES = "S", True, False)
       
       '*********************
       ' PRODUCTOS COMPUESTOS
       '*********************
       
        'Compuesto.value = IIf(prsProductos!c_compuesto = "1", 1, 0)
        'Compuesto.Enabled = IIf(prsProductos!c_compuesto = "1", True, False)
        btn_Compuesto.Enabled = False

        Observacion = IIf(IsNull(pRsProductos!c_OBSERVACIO), "", pRsProductos!c_OBSERVACIO)
       
       'Identificacion por caja
       Presentacion = pRsProductos!c_presenta
       Peso = FormatNumber(CDbl(pRsProductos!n_peso), 3)
       Volumen = FormatNumber(CDbl(pRsProductos!n_volumen), 3)
       CantidadBulto = FormatNumber(CDbl(pRsProductos!n_cantibul), pRsProductos!CANT_DECIMALES)
       PesoBulto = FormatNumber(CDbl(pRsProductos!n_pesobul), 3)
       VolumenBulto = FormatNumber(CDbl(pRsProductos!n_volbul), 3)
       
       'If ExisteCampoTabla("nu_StockMin", , "SELECT nu_StockMin FROM MA_PRODUCTOS") Then txt_StockMin = pRsProductos!nu_StockMin
       'If ExisteCampoTabla("nu_StockMax", , "SELECT nu_StockMax FROM MA_PRODUCTOS") Then txt_StockMax = pRsProductos!nu_StockMax
       
       If ExisteCampoTabla("nu_StockMin", pRsProductos) Then txt_StockMin = pRsProductos!nu_stockmin
       If ExisteCampoTabla("nu_StockMax", pRsProductos) Then txt_StockMax = pRsProductos!nu_stockmax
       
       'ESTADO DEL PRODUCTO
       Activo.Value = pRsProductos!n_activo
       
       'TIPO DE PRODUCTO
       
       Tipo_Producto.Text = Tipo_Producto.List(pRsProductos!n_tipopeso)
       
       Tipo_Producto_LostFocus
       
       If pRsProductos!n_tipopeso = IntTipoCompuesto Then
           partes = True
       End If
       
       'SITUACION INTERNACIONAL
       
       cod_Arancel = IIf(IsNull(pRsProductos!c_cod_arancel), "", pRsProductos!c_cod_arancel)
       des_Arancel = IIf(IsNull(pRsProductos!c_des_arancel), "", pRsProductos!c_des_arancel)
       imp_Arancel = FormatNumber(pRsProductos!n_por_arancel, 2)
       Costo_Original = FormatNumber(IIf(IsNull(pRsProductos!n_costo_original), 0, pRsProductos!n_costo_original), 2)
       
       If Not IsNull(pRsProductos!c_FileImagen) Then
           Call PopImageOfDb(Imagen, pRsProductos, "c_fileimagen")
           imagen_hanled = Imagen.Picture.Handle
           
        '  Debug.Print Imagen.Width & " ," & Imagen.Height & " , " & Imagen.Picture.Height & "," & Imagen.Picture.Width
       Else
           imagen_hanled = 0
           Imagen.Picture = LoadPicture()
       End If
       
       'On Error GoTo 0
       
        Dim RsCodigo As New Recordset
    
        RsCodigo.Open "SELECT * FROM MA_CODIGOS WHERE c_CodNasa = '" & pRsProductos!c_codigo & "' AND nu_Intercambio = 1", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If Not RsCodigo.EOF Then
            BarCode.Text = RsCodigo!c_codigo
        End If
        
        Call Cerrar_Recordset(RsCodigo)
        
    End If
    
    Call Cerrar_Recordset(pRsProductos)

    Exit Sub

Errores:

    mCargandoProducto = False
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".Cargar_Campos", Err.Source)
    Unload Me
    
End Sub

Private Sub Descripcion_LostFocus()

    On Error GoTo Errores
    
    If Codigo = "" Then
        If Codigo.Enabled = True Then Codigo.SetFocus
    End If
    
    Exit Sub
    
Errores:

    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & "Descripcion_LostFocus", Err.Source)
    Unload Me

End Sub

Private Sub Cactual_GotFocus()
    CActual.SelStart = 0
    CActual.SelLength = Len(CActual)
End Sub

Private Sub Canterior_GotFocus()
    CAnterior.SelStart = 0
    CAnterior.SelLength = Len(CAnterior)
End Sub

Private Sub Cpromedio_GotFocus()
    CPromedio.SelStart = 0
    CPromedio.SelLength = Len(CPromedio)
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If Cancel = 0 Then Call Form_KeyDown(vbKeyF12, 0)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If RsProductos.State = adStateOpen Then RsProductos.Close
    Set RsProductos = Nothing
    Set rscodigos = Nothing
    Set Ficha_ProductosLite = Nothing
    KillSecure TmpImgFileName
End Sub

Private Sub Imagen_DblClick()

    On Error GoTo Errores
    
    TmpPreviewPic_MouseUp vbLeftButton, 0, 0, 0
    
    dialogo.Filter = "Im�genes (*.jpg)|*.jpg"
   
    dialogo.ShowOpen
    
    If dialogo.FileName <> "" Then
        Imagen.Picture = LoadPicture(dialogo.FileName)
        imagen_handle = Imagen.Picture.Handle
        imagen_name = dialogo.FileName
    Else
        imagen_name = ""
    End If
    
    Exit Sub
    
Errores:
    
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".Imagen_DblClick", Err.Source)
    Unload Me
    
End Sub

Private Sub imp_Arancel_GotFocus()
    imp_Arancel.SelStart = 0
    imp_Arancel.SelLength = Len(imp_Arancel)
End Sub

Private Sub imp_Arancel_KeyPress(KeyAscii As Integer)
    Call Ingreso_Numero(KeyAscii)
End Sub

Private Sub imp_arancel_LostFocus()
    
    On Error GoTo Errores
    
    If IsNumeric(imp_Arancel) Then
        imp_Arancel = FormatNumber(CDbl(imp_Arancel), 2)
    Else
        Call Mensaje(True, Stellar_Mensaje(16202)) '"Expresi�n no v�lida.")
        imp_Arancel.SetFocus
    End If
    
    Exit Sub
    
Errores:
    
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".imp_arancel_LostFocus", Err.Source)
    Unload Me
    
End Sub

Private Sub Imp2_Change()
    Resp = CheckCad(Imp2, 2, , False)
End Sub

Private Sub Imp2_GotFocus()
    Imp2.SelStart = 0
    Imp2.SelLength = Len(Imp2)
End Sub

Private Sub Imp2_LostFocus()
    If Not IsNumeric(Imp2) Then
        Imp2 = FormatNumber(0, 2)
    Else
        Imp2 = FormatNumber(Imp2, 2)
    End If
End Sub

Private Sub Imp3_Change()
    Resp = CheckCad(Imp3, 2, , False)
End Sub

Private Sub Imp3_GotFocus()
    Imp3.SelStart = 0
    Imp3.SelLength = Len(Imp3)
End Sub

Private Sub Imp3_LostFocus()
    If Not IsNumeric(Imp3) Then
        Imp3 = FormatNumber(0, 2)
    Else
        Imp3 = FormatNumber(Imp3, 2)
    End If
End Sub

Private Sub Modelo_KeyPress(KeyAscii As Integer)
    'unidad
    'pesado
    'Pesable
    'Carac.Exte
    'Informativo
    'Compuesto
    
    If KeyAscii = vbKeyReturn Then
        oTeclado.Key_Tab
    End If
End Sub

Private Sub Observacion_LostFocus()
    
    On Error GoTo Errores
    
    If Codigo.Text = "" Or Descripcion.Text = "" Then
        If Descripcion.Enabled = True Then Descripcion.SetFocus
    End If
    
    Exit Sub
    
Errores:
    
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".Observacion_LostFocus", Err.Source)
    Unload Me
    
End Sub

Private Sub Peso_Change()
    Resp = CheckCad(Peso, 3, , False)
End Sub

Private Sub PesoBulto_Change()
    Resp = CheckCad(PesoBulto, 3, , False)
End Sub

Private Sub Precio1_Change()
    Call CheckCad(Precio1, Std_Decm, True)
'    If IsNumeric(PRECIO1.Text) Then PRECIO1.Text = FormatNumber(PRECIO1.Text, mCantidadDecimales)
'    If IsNumeric(PRECIO1.Text) Then
'        PRECIO1.SelStart = Len(PRECIO1.Text)
'    End If
End Sub

Private Sub Precio1_GotFocus()
    porcentaje = 0
    mCantidadDecimales = 0
    Precio1.SelStart = 0
    Precio1.SelLength = Len(Precio1)
    
    If IsNumeric(Precio1.Text) Then
        mPrecioInicial = CDbl(Precio1.Text)
        
    Else
        mPrecioInicial = Val(Precio1.Text)
        
    End If
End Sub

Private Sub Precio2_Change()
    Call CheckCad(Precio2, Std_Decm, True)
End Sub

Private Sub Precio2_GotFocus()
    porcentaje = 0
    Precio2.SelStart = 0
    Precio2.SelLength = Len(Precio2)
    
    If IsNumeric(Precio2.Text) Then
        mPrecioInicial = CDbl(Precio2.Text)
    Else
        mPrecioInicial = Val(Precio2.Text)
    End If
End Sub

Private Sub Precio3_Change()
    Call CheckCad(Precio3, Std_Decm, True)
End Sub

Private Sub Precio3_GotFocus()
    porcentaje = 0
    Precio3.SelStart = 0
    Precio3.SelLength = Len(Precio3)
    
    If IsNumeric(Precio3.Text) Then
        mPrecioInicial = CDbl(Precio3.Text)
    Else
        mPrecioInicial = Val(Precio3.Text)
    End If
End Sub

Private Sub Peso_GotFocus()
    Peso.SelStart = 0
    Peso.SelLength = Len(Peso)
End Sub

Private Sub Procedencia_Click()
    
    On Error GoTo Errores
    
    Select Case Procedencia.ListIndex
        Case Is = -1
            Procedencia.Text = Procedencia.List(0)
            cod_Arancel.Enabled = False
            des_Arancel.Enabled = False
            Costo_Original.Enabled = False
            imp_Arancel.Enabled = False
        Case Is = 0
            'Call Botones(False, False, False, False, False)
            '712
            'Sstab1.TabEnabled(1) = False
            cod_Arancel.Enabled = False
            des_Arancel.Enabled = False
            Costo_Original.Enabled = False
            imp_Arancel.Enabled = False
        Case Is = 1
            ' Sstab1.TabEnabled(1) = True
            If mGrabar = "update" Or mGrabar = "insert" Then
                'Call Botones(True, False, False, False, False)
            End If
            cod_Arancel.Enabled = True
            des_Arancel.Enabled = True
            Costo_Original.Enabled = True
            imp_Arancel.Enabled = True
    End Select
        
    Exit Sub
    
Errores:

    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".btn_dpto_Click", Err.Source)
    Unload Me
    
End Sub

Private Sub Proveedor_Click()
    
    On Error GoTo Errores
    
    If Codigo <> "" Then
        Screen.MousePointer = 11
        '        csql = "select * from ma_prodxprov where c_codigo = '" & Trim(Codigo.Text) & "'"
        '        If rsprodxprov.State = adStateOpen Then rsprodxprov.Close
        '        rsprodxprov.Open csql, Ent.BDD, adOpenDynamic, adLockBatchOptimistic
        Ficha_Productos_pxp.numero_producto = Trim(Codigo.Text)
        Ficha_Productos_pxp.flag_visible = True
        Ficha_Productos_pxp.Show vbModal
    End If
    
    Exit Sub
    
Errores:
    
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".btn_dpto_Click", Err.Source)
    Unload Me
    
End Sub

Private Sub Sstab1_GotFocus()
    
    On Error GoTo Errores
    
    If Sstab1.Tab <> 0 Then
        If Codigo.Text = "" Then
            Sstab1.Tab = 0
            If Codigo.Enabled = True Then Codigo.SetFocus
        ElseIf Descripcion.Text = "" Then
            Sstab1.Tab = 0
            Descripcion.SetFocus
        End If
    End If
    
    Exit Sub
    
Errores:
    
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".btn_dpto_Click", Err.Source)
    Unload Me
    
End Sub

Private Sub Tipo_Producto_Click()
    Tipo_Producto_LostFocus
End Sub

Private Sub Tipo_Producto_LostFocus()
    
    On Error GoTo Errores
    
    Select Case Tipo_Producto.ListIndex
        
        Case IntTipoUnidad
            Me.FrmDepGruSub.Enabled = True
            Me.FrmPlantilla.Enabled = False
        
        Case IntTipoPesado
            Me.FrmDepGruSub.Enabled = True
            Me.FrmPlantilla.Enabled = False
        
        Case IntTipoPesable
            Me.FrmDepGruSub.Enabled = True
            Me.FrmPlantilla.Enabled = False
        
        Case intTipoCarExt
            Me.FrmDepGruSub.Enabled = True
            Me.FrmPlantilla.Enabled = True
        
        Case IntTipoInformativo
            Me.FrmDepGruSub.Enabled = True
            Me.FrmPlantilla.Enabled = False
        
        Case IntTipoCompuesto
            Me.FrmDepGruSub.Enabled = True
            Me.FrmPlantilla.Enabled = False
    
    End Select
    
    Exit Sub
    
Errores:
    
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".btn_dpto_Click", Err.Source)
    Unload Me
    
End Sub

Private Sub Toolbar1_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
    
    Select Case UCase(ButtonMenu.Key)
        Case Is = "APRODUCTO"
            Call Form_KeyDown(vbKeyF3, 0)
        Case Is = "AIMPORTAR"
            Call Form_KeyDown(vbKeyF3, 0)
            
            fImportarPro = True
            
            buscar_registro
            
            Toolbar1.Buttons(1).Enabled = False
            Toolbar1.Buttons(2).Enabled = False
            Toolbar1.Buttons(3).Enabled = False
            Toolbar1.Buttons(6).Enabled = False
            
            Me.Tipo_Producto.Enabled = True
            
            If Me.Descripcion.Text = "" Then
                Call Form_KeyDown(vbKeyF7, 0)
            End If
        Case Is = UCase("imparchivo")
            ImportarArchivoTexto
    End Select
    
    fImportarPro = False
    
End Sub

Private Sub txt_descripcionCorta_Click()
    If ModoTouch Then TecladoAvanzado txt_descripcionCorta
End Sub

Private Sub txt_PorcentajeMerma_Change()
    If InStr(1, txt_PorcentajeMerma, "%") Then txt_PorcentajeMerma = Replace(txt_PorcentajeMerma, "%", "")
    CheckCad txt_PorcentajeMerma
    CalculodeCostosconMerma
End Sub

Private Sub txt_PorcentajeMerma_Click()
    If ModoTouch Then TecladoAvanzado txt_PorcentajeMerma
End Sub

Private Sub txt_PorcentajeMerma_LostFocus()
    If CDbl(txt_PorcentajeMerma) > 100 Or CDbl(txt_PorcentajeMerma) < 0 Then
        txt_PorcentajeMerma = 0
    End If
    
    txt_PorcentajeMerma = FormatNumber(txt_PorcentajeMerma, 2)
    CalculodeCostosconMerma
End Sub

Private Sub txt_StockMax_Change()
    
    Dim mCantidadDecimales As Integer
    
    Select Case Tipo_Producto.ListIndex
        Case 1, 2
            mCantidadDecimales = 3
        Case Else
            mCantidadDecimales = 0
    End Select
    
    Resp = CheckCad(txt_StockMax, mCantidadDecimales, , False)
    
End Sub

Private Sub txt_StockMax_Click()
    If ModoTouch Then TecladoAvanzado txt_StockMax
End Sub

Private Sub txt_StockMax_GotFocus()
    Me.txt_StockMax.SelStart = 0
    Me.txt_StockMax.SelLength = Len(txt_StockMax)
End Sub

Private Sub txt_StockMin_Change()
    
    Dim mCantidadDecimales As Integer
    
    Select Case Tipo_Producto.ListIndex
        Case 1, 2
            mCantidadDecimales = 3
        Case Else
            mCantidadDecimales = 0
    End Select
    
    Resp = CheckCad(txt_StockMin, mCantidadDecimales, , False)
    
End Sub

Private Sub txt_StockMin_Click()
    If ModoTouch Then TecladoAvanzado txt_StockMin
End Sub

Private Sub txt_StockMin_GotFocus()
    Me.txt_StockMin.SelStart = 0
    Me.txt_StockMin.SelLength = Len(txt_StockMin)
End Sub

Private Sub txtCodCarExt_Change()
    
    If Trim(txtCodCarExt) <> "" Then
        Me.f_costos.Enabled = False
        Me.f_precios.Enabled = False
        Me.f_costos.Enabled = False
    Else
        If Me.Descripcion.Enabled Then
            Me.f_costos.Enabled = True
            Me.f_precios.Enabled = True
            Me.f_costos.Enabled = True
            Me.lblDesCarExt = ""
        Else
            Me.f_costos.Enabled = False
            Me.f_precios.Enabled = False
            Me.f_costos.Enabled = False
        End If
    End If
    
End Sub

Private Sub Volumen_Change()
    Resp = CheckCad(Volumen, 3, , False)
End Sub

Private Sub Volumen_Click()
    If ModoTouch Then TecladoAvanzado Volumen
End Sub

Private Sub Volumen_GotFocus()
    Volumen.SelStart = 0
    Volumen.SelLength = Len(Volumen)
End Sub

Private Sub CantidadBulto_GotFocus()
    CantidadBulto.SelStart = 0
    CantidadBulto.SelLength = Len(CantidadBulto)
End Sub

Private Sub PesoBulto_GotFocus()
    PesoBulto.SelStart = 0
    PesoBulto.SelLength = Len(PesoBulto)
End Sub

Private Sub VolumenBulto_Change()
    Resp = CheckCad(VolumenBulto, 3, , False)
End Sub

Private Sub VolumenBulto_Click()
    If ModoTouch Then TecladoAvanzado VolumenBulto
End Sub

Private Sub VolumenBulto_GotFocus()
    VolumenBulto.SelStart = 0
    VolumenBulto.SelLength = Len(VolumenBulto)
End Sub

Private Sub Cactual_LostFocus()
    If CActual.Text = "" Then CActual.Text = 0
    CActual = FormatNumber(CDbl(CActual), Std_Decm)
    
    If mGrabar = "insert" Then
        CAnterior = FormatNumber(CDbl(CActual), Std_Decm)
        CPromedio = FormatNumber(CDbl(CActual), Std_Decm)
        CReposicion = FormatNumber(CDbl(CActual), Std_Decm)
    End If
End Sub

Private Sub Canterior_LostFocus()
    If CAnterior.Text = "" Then CAnterior.Text = 0
    CAnterior = FormatNumber(CDbl(CAnterior), Std_Decm)
End Sub

Private Sub Cpromedio_LostFocus()
    If CPromedio.Text = "" Then CPromedio.Text = 0
    CPromedio = FormatNumber(CDbl(CPromedio), Std_Decm)
End Sub

Private Sub Precio1_LostFocus()
    
    On Error GoTo Errores
    
    Call CALCULAR_PRECIOS(Precio1, Margen1, Margen1C)
    'mCantidadDecimales = 2
    'PRECIO1.Text = FormatNumber(PRECIO1.Text, mCantidadDecimales)
    
    Exit Sub
    
Errores:

    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".btn_dpto_Click", Err.Source)
    Unload Me
    
End Sub

Private Sub Precio2_LostFocus()
    
    On Error GoTo Errores
    
    Call CALCULAR_PRECIOS(Precio2, Margen2, Margen2C)
    
    Exit Sub
    
Errores:

    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".btn_dpto_Click", Err.Source)
    Unload Me
    
End Sub

Private Sub Precio3_LostFocus()

    On Error GoTo Errores
    
    Call CALCULAR_PRECIOS(Precio3, Margen3, Margen3C)
    
    Exit Sub
    
Errores:

    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".btn_dpto_Click", Err.Source)
    Unload Me
    
End Sub

Private Sub Peso_LostFocus()

    On Error GoTo Errores
    
    If IsNumeric(Peso) Then
        Peso = FormatNumber(CDbl(Peso), 3)
    Else
        Peso = FormatNumber(0, 3)
        Peso.SetFocus
    End If
    
    Exit Sub
    
Errores:
    
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".Peso_LostFocus", Err.Source)
    Unload Me
    
End Sub

Private Sub Volumen_LostFocus()
    
    On Error GoTo Errores
    
    If IsNumeric(Volumen) Then
        Volumen = FormatNumber(CDbl(Volumen), 3)
    Else
        Volumen = FormatNumber(0, 3)
        Volumen.SetFocus
    End If
    
    Exit Sub
    
Errores:
    
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".Volumen_LostFocus", Err.Source)
    Unload Me
    
End Sub

Private Sub CantidadBulto_LostFocus()
    If IsNumeric(CantidadBulto) Then
        CantidadBulto = FormatNumber(CDbl(CantidadBulto), CInt(Decimales))
    Else
        CantidadBulto = FormatNumber(0, CInt(Decimales))
    End If
End Sub

Private Sub PesoBulto_LostFocus()
    
    On Error GoTo Errores
    
    If IsNumeric(PesoBulto) Then
        PesoBulto = FormatNumber(CDbl(PesoBulto), 3)
    Else
        PesoBulto = FormatNumber(0, 3)
        PesoBulto.SetFocus
    End If
    
    Exit Sub
    
Errores:

    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".Pesobulto_LostFocus", Err.Source)
    Unload Me
    
End Sub

Private Sub VolumenBulto_LostFocus()
    On Error GoTo Errores
    
    If IsNumeric(VolumenBulto) Then
        VolumenBulto = FormatNumber(CDbl(VolumenBulto), 3)
    Else
        VolumenBulto = FormatNumber(0, 3)
        VolumenBulto.SetFocus
    End If
    
    Exit Sub
    
Errores:
    
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".Volumenbulto_LostFocus", Err.Source)
    Unload Me
    
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
    
    On Error GoTo Errores
    
    If Sstab1.Tab <> 0 Then
        If Codigo.Text = "" Then
            Sstab1.Tab = 0
            If Codigo.Enabled = True Then Codigo.SetFocus
        ElseIf Descripcion.Text = "" Then
            Sstab1.Tab = 0
            Descripcion.SetFocus
        End If
        '        Toolbar1.Buttons(6).Enabled = False
        '        Toolbar1.Buttons(3).Enabled = False
        Else
        '        Toolbar1.Buttons(6).Enabled = True
        '        Toolbar1.Buttons(3).Enabled = True
    End If
    
    Exit Sub
    
Errores:
    
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".btn_dpto_Click", Err.Source)
    Unload Me
    
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case Button.Key
        Case Is = "Agregar"
            Call agregar_registro
        
        Case Is = "Buscar"
            Me.Tipo_Producto.Enabled = False
            Call buscar_registro
        
        Case Is = "Modificar"
            Call modificar_registro
        
        Case Is = "Cancelar"
            Toolbar1.Buttons(3).Enabled = False
            Call Limpiar_Datos
            Call cancelar_registro
        
        Case Is = "Eliminar"
            Call eliminar_registro
        
        Case Is = "Grabar"
            Call grabar_registro
        
        'Case Is = "Imprimir"
            'Call imprimir_registro
        
        Case Is = "Salir"
            Call Form_KeyDown(vbKeyF12, 1)
            
        Case Is = "Ayuda"
        
    End Select
    
    'Me.tipo_producto.Enabled = True
    
End Sub

Public Sub agregar_registro()
    
    On Error GoTo Errores
    
    ' Sstab1.TabEnabled(1) = False
    
    Sstab1.Tab = 0
    Cod_Alt.Enabled = True 'False
    Proveedor.Enabled = False
    Me.btn_Compuesto.Enabled = False
    Me.Btn_ProExt.Enabled = False
    Tipo_Producto.Enabled = True
    
    Call Limpiar_Datos
    Call Habilitar_Datos(True)
    
    mGrabar = "insert"
    Me.CActual.Enabled = True
    Me.CAnterior.Enabled = True
    Me.CPromedio.Enabled = True
    Me.CReposicion.Enabled = True
    
    Me.C�digo(73).Visible = True
    Me.Existencia.Visible = True
    
    Toolbar1.Buttons(1).Enabled = False
    Toolbar1.Buttons(2).Enabled = False
    Toolbar1.Buttons(3).Enabled = False
    Toolbar1.Buttons(6).Enabled = False
    Toolbar1.Buttons(5).Enabled = True
    
    Me.Tipo_Producto.Enabled = True
    If Ent.rsReglasComerciales.State = adStateClosed Then Ent.rsReglasComerciales.Open
    Ent.rsReglasComerciales.Requery
    AutoCodificacion = Ent.rsReglasComerciales!Auto_Codigo
    
    If Not Ent.rsReglasComerciales!Auto_Codigo Then
        Codigo.Enabled = True
        Codigo.SetFocus
    Else
        Ent.rsReglasComerciales.Requery
        NMASKARA = Ent.rsReglasComerciales!Caracter_Codigo
        Maskara = String(NMASKARA, "0")

        Codigo = Format(NO_CONSECUTIVO("cod_producto", False), Maskara)
        Codigo.Enabled = True
        Codigo.SetFocus
        If grutinas.PuedeObtenerFoco(Descripcion) Then Descripcion.SetFocus 'oTeclado.Key_Return
    End If
    
    Exit Sub
    
Errores:
    
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".agregar_registro", Err.Source)
    Unload Me
    
End Sub

Private Sub buscar_registro()
    
    Dim mArrProducto As Variant, mRs As New Recordset
    
    mArrProducto = BuscarInfoProducto_Basica(Ent.BDD, , , , , VistaMarca)
    
    If Not IsEmpty(mArrProducto) Then
        
        If Trim(mArrProducto(0)) <> Trim(Codigo.Text) Then
            If Not fImportarPro Then
                Codigo.Text = Trim(mArrProducto(0))
                Descripcion.Text = Trim(mArrProducto(1))
            End If
        End If
        
        Apertura_RecordsetC mRs
        
        mRs.Open "SELECT * FROM MA_PRODUCTOS WHERE c_Codigo = '" & Trim(mArrProducto(0)) & "'", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic
        
        If Not mRs.EOF Then
            Toolbar1.Buttons(3).Enabled = True
            Toolbar1.Buttons(6).Enabled = True
            Me.Codigo.Enabled = False
            mCargandoProducto = True
            Cargar_Campos mRs
            mCargandoProducto = False
        End If
        
    End If
    
End Sub

Private Sub modificar_registro()
    
    On Error GoTo Errores
    
    If Trim(Codigo) = "" And Trim(Descripcion) = "" Then Exit Sub
    Sstab1.Tab = 0
    mGrabar = "update"
    
    Call Habilitar_Datos(True)
    
    Toolbar1.Buttons(3).Enabled = True
    CActual.Enabled = True
    CAnterior.Enabled = False
    CPromedio.Enabled = False
    CReposicion.Enabled = False
    
    'If Sstab1.TabEnabled(1) = True Then Call Botones(True, False, False, False, False)
    Set MyExte = New ClsExtendidas
    MyExte.Set_Data_Only_Connections Ent.BDD, Ent.POS
    
    If MyExte.Plantilla_Had_Products(Codigo) Then
        Tipo_Producto.Enabled = False
    Else
        Tipo_Producto.Enabled = True
    End If
    
    Select Case Tipo_Producto.ListIndex
        
        Case intTipoCarExt
            Cod_Alt.Enabled = True
            Proveedor.Enabled = False
            Me.btn_Compuesto.Enabled = False
            Me.Btn_ProExt.Enabled = True
        
        Case IntTipoCompuesto
            Cod_Alt.Enabled = True
            Proveedor.Enabled = True
            Me.btn_Compuesto.Enabled = True
            Me.Btn_ProExt.Enabled = False
        
        Case Else
            Cod_Alt.Enabled = True
            Proveedor.Enabled = True
            Me.btn_Compuesto.Enabled = False
            Me.Btn_ProExt.Enabled = False
    
    End Select
    
    Descripcion.SetFocus
    
    Toolbar1.Buttons(1).Enabled = False
    Toolbar1.Buttons(2).Enabled = False
    Toolbar1.Buttons(3).Enabled = False
    Toolbar1.Buttons(6).Enabled = False
    Toolbar1.Buttons(5).Enabled = True
    Toolbar1.Buttons(7).Enabled = True
    
    txtCodCarExt_Change
    'Me.tipo_producto.Enabled = True
    
    BarCode.Enabled = False
    
    Exit Sub
    
Errores:
    
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".modificar_registro", Err.Source)
    Unload Me
    
End Sub

Private Sub cancelar_registro()
    
    'Call Botones(False, False, False, False, False)
    
    Call Habilitar_Datos(False)
    
    Set RsProductos = Nothing
    Set rscodigos = Nothing
    Set rsEureka = Nothing
    Set rsCierres = Nothing
    Set RsDepositos = Nothing
    Set fClsProCom = Nothing
    
    Me.Command3.Enabled = False
    Me.FrmDepGruSub.Enabled = False
    Me.FrmPlantilla.Enabled = False
    
    'Set Ficha_productos_Compuesto = Nothing
    
End Sub

Private Sub eliminar_registro()

    Screen.MousePointer = 11
    
    'If Not gObjMensajeria.Mensaje(Stellar_Mensaje(16195), True) Then Exit Sub '"�Est� seguro que desea Eliminar este producto? Presione Aceptar si est� de acuerdo.", True) Then Exit Sub
    
        Mensaje False, StellarMensaje(16195)
    
    If Not Retorno Then
        Screen.MousePointer = 0
        Exit Sub
    End If

    Call Apertura_Recordset(RsProductos)
    
    RsProductos.Open "select * from ma_productos where c_codigo ='" & Codigo.Text & "'", _
    Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    If Not RsProductos.EOF Then
        depoprod = False
        
        Call Apertura_Recordset(RsTrCompras)
        
        RsTrCompras.Open "select c_codarticulo from ma_depoprod where c_codarticulo = '" & Codigo.Text & "'", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not RsTrCompras.EOF Then
            depoprod = True
        End If
        
        'transaccion = False
        
        If depoprod = True Or transaccion = True Then
            Call Mensaje(True, Stellar_Mensaje(16196)) '"Existen transacciones en el sistema que no permiten la Eliminaci�n de este producto.")
            
            RsTrCompras.Close
            RsProductos.Close
            
            Set RsTrCompras = Nothing
            Set rstrproductos = Nothing
            
            Exit Sub
        Else
            Ent.BDD.BeginTrans
                RsProductos.Delete
                RsProductos.UpdateBatch
                
                Call Apertura_Recordset(rscodigos)
                
                mPasarTrpen = ManejaPOS(Ent.BDD) Or ManejaSucursales(Ent.BDD)
                
'                If Not mPasarTrpen Then
'                    rscodigos.Open "select * FROM MA_codigos where c_codnasa = '" & Codigo & "'", Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
'                    OperacionRs "TR_PENDIENTE_CODIGO", rscodigos, Ent.BDD
'                Else

                rscodigos.Open "select * FROM MA_codigos where c_codnasa = '" & Codigo & "'", _
                Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
                
'                End If

                Call Apertura_Recordset(rsProdxProv)
                
'                If Not mPasarTrpen Then
'                    rscodigos.Open "select * from MA_prodxprov where c_codigo = '" & Codigo & "'", Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
'                    OperacionRs "TR_PENDIENTE_CODIGO", rscodigos, Ent.BDD
'                Else
                    rsProdxProv.Open "DELETE from MA_prodxprov where c_codigo = '" & Codigo & "'", _
                    Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
'                End If
                
                '***************************************
                '**** TRANSACCIONES PENDIENTES
                '***************************************
                
                'Ent.rsReglasComerciales.Requery
                'If Ent.rsReglasComerciales!MANEJA_POS = True Then
                
                If mPasarTrpen Then
                
                    'PENDIENTE DE PRODUCTOS
                    
                    Call Apertura_Recordset(rsEureka)
                    
                    rsEureka.Open "SELECT * FROM tr_pendiente_prod WHERE C_CODIGO = '" & Codigo & "' AND TIPOCAMBIO <> 0", _
                    Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
                    
                    If Not rsEureka.EOF Then
                        rsEureka.Update
                    Else
                        rsEureka.AddNew
                        rsEureka!c_codigo = Codigo
                    End If
                    
                    rsEureka!TipoCambio = 1
                    
                    Call Update_Datos(rsEureka)
                    
                    rsEureka!HABLADOR = 0
                    rsEureka.UpdateBatch
                    
                    'PENDIENTE DE CODIGOS
                    
                    If Not rscodigos.EOF Then
                        While Not rscodigos.EOF
                            Call Apertura_Recordset(rsEureka)
                            
                            rsEureka.Open "SELECT * FROM tr_pendiente_codigo WHERE C_CODIGO = '" & rscodigos!c_codigo & "' AND TIPOCAMBIO <> 0", _
                            Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
'                            If Not rseureka.EOF Then
'                                rseureka.Update
'                            Else
                                rsEureka.AddNew
                                rsEureka!c_codigo = rscodigos!c_codigo
'                            End If
                            rsEureka!c_codnasa = rscodigos!c_codnasa
                            rsEureka!c_descripcion = rscodigos!c_descripcion
                            rsEureka!n_cantidad = rscodigos!n_cantidad
                            rsEureka!TipoCambio = 1
                            rsEureka.UpdateBatch
'                            rscodigos.delete
'                            rscodigos.UpdateBatch
                            rscodigos.MoveNext
                        Wend
                        
                        rscodigos.MoveFirst
                    End If
                End If
                
                '12/07/2004
                
                If Not rscodigos.EOF Then
                    While Not rscodigos.EOF
                        rscodigos.Delete
                        rscodigos.UpdateBatch
                        rscodigos.MoveNext
                    Wend
                End If
                
            Ent.BDD.CommitTrans
        End If
    
        Call Cerrar_Recordset(rsEureka)
        Call Cerrar_Recordset(rscodigos)
        Call Cerrar_Recordset(rsProdxProv)
        Call Cerrar_Recordset(RsTrCompras)
        
        Call Limpiar_Datos
    End If
    
    Screen.MousePointer = 0
    
End Sub

Private Sub grabar_registro()

    Dim RsDepoProd As New ADODB.Recordset

'On Error Resume Next
'    Ent.BDD.CommitTrans
'    Ent.BDD.BeginTrans
'    Err.Clear
'On Error GoTo 0

    '*******************************************************************
    'VERIFICACI�N QUE EL CODIGO NO ESTE VAC�O
    '*******************************************************************
    
        If Codigo.Text = "" Then
            Call Mensaje(True, Stellar_Mensaje(16197)) '"El C�digo del producto es obligatorio.")
            Codigo.SetFocus
            Exit Sub
        End If
    
    '*******************************************************************
    'VERIFICACI�N QUE LA DESCRIPCI�N NO ESTE VAC�A
    '*******************************************************************
    
        If Descripcion.Text = "" Then
            Call Mensaje(True, Stellar_Mensaje(16198)) '"La Descripci�n del producto es obligatoria.")
            Descripcion.SetFocus
            Exit Sub
        End If
    
    '***************************************************
    'VERIFICACI�N DE ASIGNACI�N DE DEPARTAMENTO
    '***************************************************
    
    If Me.FrmDepGruSub.Visible Then
        If Departamento.Text = "" And (Me.Tipo_Producto.ListIndex <> intTipoCarExt) Then '(Me.Tipo_Producto <> "Carac. Exte")
            Call Mensaje(True, Stellar_Mensaje(16199)) '"Primero debe seleccionar el departamento al cual pertenece el producto.")
            Departamento.SetFocus
            Exit Sub
        End If
    End If
    
    '***************************************************'***************************************************
    'VERIFICACI�N DE ASIGNACI�N DE LA PLANTILLA PRODUCTOS EXTENDIDOS
    '***************************************************'***************************************************
    
    If Not VerificarTxtNumericos Then Exit Sub
        
     Select Case Tipo_Producto.ListIndex
        Case intTipoCarExt
            If Len(Trim(Me.txtCodCarExt.Text)) = 0 Then
                 Call Mensaje(True, Stellar_Mensaje(16200)) '"Los Productos con Caracteristicas Extendidas deben tener una plantilla asociada.")
                 Command1.SetFocus
                 Exit Sub
            End If
        Case IntTipoPesado, IntTipoPesable
            If Me.Decimales = "" Or Val(Me.Decimales) = 0 Then
                Call Mensaje(True, Stellar_Mensaje(16201)) '"Los productos Pesados y Pesables deben tener una cantidad de Decimales diferente de cero.")
                Sstab1.Tab = 1
                If grutinas.PuedeObtenerFoco(Decimales) Then Decimales.SetFocus
                Exit Sub
            End If
     End Select

    '*********************************
    'COMIENZA TRANSACCION
    '*********************************
    
    'On Error GoTo ERROR_GRABAR_PRODUCTO
    
    If Ent.BDD.State = adStateOpen Then
        Ent.BDD.BeginTrans
    End If
    
        '*********************************
        'SI ES PRODUCTO NUEVO
        '*********************************
        If mGrabar = "insert" Then
            
            Call Apertura_Recordset(RsProductos)
            RsProductos.Open "SELECT * FROM MA_PRODUCTOS WHERE 1 = 2", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            '********************************************************************
            '* VERIFICA SI EXISTE AUTOCODIFICACI�N PARA ASUMIR EL CODIGO NUEVO
            '*******************************************************************
            
            If AutoCodificacion Then
                Codigo = Format(NO_CONSECUTIVO("Cod_Producto"), Maskara)
            End If
            
            '************************************************************
            '* Agregar el C�digo Maestro a la Tabla de C�digos Alternos
            '************************************************************
            
            Call Apertura_Recordset(rscodigos)

            rscodigos.Open "SELECT * FROM MA_CODIGOS WHERE 1 = 2", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            With rscodigos
                .AddNew
                !c_codigo = Codigo
                !c_descripcion = "CODIGO MAESTRO"
                !c_codnasa = Codigo
                !n_cantidad = 1
                !nu_intercambio = 0
                .UpdateBatch
            End With
            
            If BarCode.Text <> "" Then
                With rscodigos
                    .AddNew
                    !c_codigo = BarCode.Text
                    !c_descripcion = "BARCODE"
                    !c_codnasa = Codigo
                    !n_cantidad = 1
                    !nu_intercambio = 1
                    .UpdateBatch
                End With
            End If
            
            If Existencia.Text <> "" And IsNumeric(Existencia.Text) Then
            
                Call Apertura_Recordset(RsDepoProd)
                
                RsDepoProd.Open "SELECT * FROM MA_DEPOPROD WHERE 1 = 2", _
                Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
                
                With RsDepoProd
                    .AddNew
                    !c_coddeposito = DPS_Local
                    !c_codarticulo = Codigo
                    !c_descripcion = Descripcion.Text
                    !n_cantidad = CDbl(Existencia.Text)
                    .UpdateBatch
                End With
            
            End If
            
            '********************************************************************
            '* VERIFICA SI EL PRODUCTO ES COMPUESTO PARA CREAR SUS PARTES
            '********************************************************************
            
            'If Tipo_Producto.ListIndex = IntTipoCompuesto Then
                ' ...
            'End If
            
            '*********************************************
            '* Agregar el C�digo a la Tabla de Productos
            '*********************************************
            
            RsProductos.AddNew
            RsProductos!c_codigo = Codigo
            RsProductos!add_date = Date
            RsProductos!update_date = Date
            
        '*************************************************
        ' ACTUALIZACION DE PRODUCTOS
        '*************************************************
        
        ElseIf mGrabar = "update" Then
            Call Apertura_Recordset(RsProductos)
            
            RsProductos.Open "select * from MA_PRODUCTOS where c_codigo = '" & Codigo & "'", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            RsProductos.Update
            RsProductos!update_date = Date
            
            If partes = False And Tipo_Producto.ListIndex = 2 Then
                With Ficha_productos_Compuesto.fg2
                    For i = 1 To .Rows - 1
                        .Col = 0
                        If .Text = "" Then Exit For
                        .Row = i
                        Call Apertura_Recordset(rsMonitor)
                        rsMonitor.Open "select * from ma_partes where c_codigo = 'STELLAR'", Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
                        rsMonitor.AddNew
                            rsMonitor!c_codigo = Codigo
                            .Col = 0
                                rsMonitor!c_parte = .Text
                            .Col = 2
                                rsMonitor!n_cantidad = FormatNumber(CDbl(.Text), 3)
                        rsMonitor.UpdateBatch
                    Next
                End With
            End If
        
        End If '*** fin del insert/update
        
        '***************************************
        '**** ASIGNACI�N DE VALORES
        '***************************************
        
        Dim GeneraHablador As Integer
        GeneraHablador = 1
        
        Call Update_Datos(RsProductos, (mGrabar = "insert"), GeneraHablador)
        RsProductos.UpdateBatch
        
        Set gTrazas.ObjectConexion = Ent.BDD
        gTrazas.EscribirTraza Me.Name, lccodusuario, IIf(LCase(mGrabar) = "update", "UPD", "ADD"), RsProductos!c_codigo, nProductos
        
        '******************************************************
        ' Crea las transacciones de codigos alternos pendientes
        ' para los mercados
        '******************************************************
        
        Call Apertura_Recordset(rscodigos)
        
        rscodigos.Open "select * FROM MA_CODIGOS where c_codnasa = '" & Codigo & "'", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If rscodigos.EOF Then
            rscodigos.AddNew
            rscodigos!c_codigo = Codigo
            rscodigos!c_descripcion = "CODIGO MAESTRO"
            rscodigos!c_codnasa = Codigo
            rscodigos!n_cantidad = 1
            rscodigos.UpdateBatch
        End If
        
        If ManejaPOS(Ent.BDD) Or ManejaSucursales(Ent.BDD) Then
            Call Apertura_Recordset(Rec_Monitor)
            
            Rec_Monitor.Open "SELECT * FROM TR_PENDIENTE_CODIGO WHERE 1=2", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            With Rec_Monitor
                .AddNew
                !c_codigo = rscodigos!c_codigo
                !c_descripcion = rscodigos!c_descripcion
                '!c_codnasa = rscodigos!C_CODIGO
                !c_codnasa = rscodigos!c_codnasa
                !n_cantidad = rscodigos!n_cantidad
                !nu_intercambio = rscodigos!nu_intercambio
                !TipoCambio = 0
                .UpdateBatch
            End With
        End If
        
        '***************************************
        '**** TRANSACCIONES PENDIENTES
        '***************************************
        
        If ManejaPOS(Ent.BDD) Or ManejaSucursales(Ent.BDD) Then
            Call Apertura_Recordset(rsEureka)
            
            rsEureka.Open "SELECT * FROM tr_pendiente_prod WHERE C_CODIGO = '" & Codigo & "' AND TIPOCAMBIO = 0", _
            Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
            
            rsEureka.AddNew
            
            nCampos = RsProductos.Fields.Count - 1
            
            For qw = 0 To nCampos
                If UCase(RsProductos.Fields(qw).Name) <> "ID" Then
                    ' ESTO SE DEBE ALOS VALORES POR DEFECTO QUE NO HAN SIDO ASIGNADO
                    If Not (mGrabar = "insert" And _
                    (UCase(RsProductos.Fields(qw).Name) = UCase("F_Inicial") Or _
                    UCase(RsProductos.Fields(qw).Name) = UCase("F_Final") Or _
                    UCase(RsProductos.Fields(qw).Name) = UCase("h_inicial") Or _
                    UCase(RsProductos.Fields(qw).Name) = UCase("h_final") Or _
                    UCase(RsProductos.Fields(qw).Name) = UCase("DATE1") _
                    )) Then
                        rsEureka.Fields(RsProductos.Fields(qw).Name) = RsProductos.Fields(qw)
                    End If
                End If
            Next
    
            rsEureka!HABLADOR = GeneraHablador
            rsEureka!TipoCambio = 0
            rsEureka.UpdateBatch
            
        End If
        
    'Ent.BDD.RollbackTrans
    
    Ent.BDD.CommitTrans
    
    Call Cerrar_Recordset(rsEureka)
    
    Toolbar1.Buttons(3).Enabled = True
    mGrabar = ""
    
    Call cancelar_registro
    
    Sstab1.Tab = 0
    Me.f_general.Enabled = True
    Me.Command3.Enabled = True

Exit Sub

Error_Grabar_Producto:
    
    Me.Command3.Enabled = False
    Ent.BDD.RollbackTrans
    
    Call Mensaje(True, Err.Number & " " & Err.Description)
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".Grabar_registro", Err.Source)
    
    Unload Me

End Sub

Private Sub Codigo_LostFocus()
    
    On Error GoTo Errores
    
    If Codigo = "" Then
        'MsgBox "El C�digo del Producto es Obligatoria", vbInformation + vbOKOnly, "Mensaje Stellar"
        If Codigo.Enabled = True Then Codigo.SetFocus
    Else
        If Codigo.Enabled = True Then
            If AutoCodificacion = True Then
                Codigo.Enabled = False
            Else
                Apertura_RecordsetC rscodigos
                
                rscodigos.Open "select * from MA_CODIGOS where c_codigo = '" & Codigo & "'", Ent.BDD
                
                If Not rscodigos.EOF Then
                    Call Mensaje(True, Stellar_Mensaje(16052)) '"El C�digo ya Existe.")
                    Codigo = ""
                End If
                
                rscodigos.Close
                
                Set rscodigos = Nothing
                
                If AutoCodificacion = True And Codigo = "" Then
                    Call Mensaje(True, Stellar_Mensaje(16071)) '"Comun�quese con el Departamento de Inform�tica")
                    Exit Sub
                End If
            End If
        End If
    End If
    
    Exit Sub
    
Errores:
    
    Call Mensaje(True, Err.Number & " " & Err.Description)
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".codigo_lostfocus", Err.Source)
    Unload Me
    
End Sub

Private Sub Ingreso_Numero(tecla)
    Select Case tecla
        Case 44, 46, 48 To 57
        
        Case Else
            Beep
            tecla = 0
            KeyAscii = 0
    End Select
End Sub

Private Sub Ingreso_Precio(tecla)
    Select Case tecla
        Case 8, 44, 46, 48 To 57
        
        Case 37
            If porcentaje > 0 Then
                tecla = 0
            Else
                porcentaje = 1
            End If
    
        Case Else
            Beep
            tecla = 0
            KeyAscii = 0
    
    End Select
End Sub

Private Sub CALCULAR_PRECIOS(ID_PRECIO As Object, ID_MARGEN As Object, ID_MARGENC As Object)

'**18-10-2007 PERMITIR NO REDONDEAR PRECIOS REGULADOS
'Private Sub CALCULAR_PRECIOS(ID_PRECIO As Object, ID_MARGEN As Object, ID_MARGENC As Object, Optional EsRegulado As Boolean)

    Dim Campo_Costo As Object
    Dim Campo_Costo_Merma As Object
    
    On Error GoTo Errores
    
    If ID_PRECIO = "" Then
        ID_PRECIO = FormatNumber(0, Std_Decm)
        ID_MARGEN = FormatNumber(0, 2)
        ID_MARGENC = FormatNumber(0, 2)
        porcentaje = 0
        Exit Sub
    End If
    
    '******************************************************
    '** CALCULO A TRAVES DE UN PORCENTAJE
    '******************************************************
    
    If Mid(ID_PRECIO, Len(ID_PRECIO), 1) = "%" Then
        ID_PRECIO = Mid(ID_PRECIO, 1, Len(ID_PRECIO) - 1)
        If IsNumeric(ID_PRECIO) Then
        
            If CDbl(ID_PRECIO) = 0 Then
                ID_PRECIO = FormatNumber(0, Std_Decm)
                ID_MARGEN = FormatNumber(0, 2)
                ID_MARGENC = FormatNumber(0, 2)
                porcentaje = 0
            Exit Sub
            
            End If
            
            Ent.rsReglasComerciales.Requery
            
            Select Case Ent.rsReglasComerciales!Estimacion_PV
                
                '**** COSTO ACTUAL
                Case Is = 0
                    Set Campo_Costo = CActual
                    Set Campo_Costo_Merma = CActual_Merma
                                        
                '**** COSTO ANTERIOR
                Case Is = 1
                    Set Campo_Costo = CAnterior
                    Set Campo_Costo_Merma = CAnterior_Merma
                
                '**** COSTO PROMEDIO
                Case Is = 2
                    Set Campo_Costo = CPromedio
                    Set Campo_Costo_Merma = CPromedio_Merma
                    
                '**** COSTO REPOSICION
                Case Is = 3
                    Set Campo_Costo = CReposicion
                    Set Campo_Costo_Merma = CReposicion_merma
            
            End Select
            
            ID_MARGEN = FormatNumber(ID_PRECIO, 2)
            
            ' Nueva Rutina para distintas formas de calculo.
            
            If Not IsEmpty(txt_PorcentajeMerma) And CInt(txt_PorcentajeMerma) > 0 Then
                ID_PRECIO = FormatNumber(CalcularPrecios(Precio_CalculoMonto, CDbl(Campo_Costo), CDbl(ID_PRECIO), False), Std_Decm)
            Else
                ID_PRECIO = FormatNumber(CalcularPrecios(Precio_CalculoMonto, CDbl(Campo_Costo_Merma), CDbl(ID_PRECIO), False), Std_Decm)
            End If
            
        Else
            ID_PRECIO = FormatNumber(0, Std_Decm)
            ID_MARGEN = FormatNumber(0, 2)
        End If
        
        CalculodeCostosconMerma
    
    Else
        
        '******************************************************
        '** CALCULO A TRAVES DE UN MONTO INGRESADO
        '******************************************************
        
        If IsNumeric(ID_PRECIO) Then
            ID_PRECIO = FormatNumber(CDbl(ID_PRECIO), Std_Decm)
            If CDbl(ID_PRECIO) <> 0 Then
            
                Ent.rsReglasComerciales.Requery
                
                Select Case Ent.rsReglasComerciales!Estimacion_PV
                    '**** COSTO ACTUAL
                    Case Is = 0
                        Set Campo_Costo = CActual
                        Set Campo_Costo_Merma = CActual_Merma
                        
                    '**** COSTO ANTERIOR
                    Case Is = 1
                        Set Campo_Costo = CAnterior
                        Set Campo_Costo_Merma = CAnterior_Merma
                    
                    '**** COSTO PROMEDIO
                    Case Is = 2
                        Set Campo_Costo = CPromedio
                        Set Campo_Costo_Merma = CPromedio_Merma
                        
                    '**** COSTO REPOSICION
                    Case Is = 3
                        Set Campo_Costo = CReposicion
                        Set Campo_Costo_Merma = CReposicion_merma
                End Select
                
                ' Nueva Rutina para distintas formas de calculo.
                
                ID_MARGEN = FormatNumber(CalcularPrecios(Precio_CalculoPorcentaje, CDbl(Campo_Costo), CDbl(ID_PRECIO), False), 2)
                ID_MARGENC = FormatNumber(CalcularPrecios(Precio_CalculoPorcentaje, CDbl(Campo_Costo_Merma), CDbl(ID_PRECIO), False), 2)
                
            Else
           
                ID_MARGENC = FormatNumber(0, 2)
                ID_MARGEN = FormatNumber(0, 2)
                
            End If
        Else
            ID_PRECIO = FormatNumber(0, Std_Decm)
            ID_MARGEN = FormatNumber(0, 2)
            ID_MARGENC = FormatNumber(0, 2)
        End If
    End If
    
    If ID_PRECIO <> 0 And CDbl(mPrecioInicial) <> CDbl(ID_PRECIO) Then
        Ent.rsReglasComerciales.Requery
        If Ent.rsReglasComerciales!Redondear_Precios = True Then
            If Me.chk_PrecioRegulado And mRestringirRedondeoRegulados Then
                '** NO REDONDEAR
            Else
                If Not mNoRedondear Or Not mCargandoProducto Then ID_PRECIO = FormatNumber(REDONDEAR_PRECIO(CDbl(ID_PRECIO), CDbl(Imp1), CDbl(Imp2), CDbl(Imp3)), Std_Decm)
            End If
        End If
    End If
    
    porcentaje = 0
    
    Exit Sub
    
Errores:
    
    'Resume ' Debug
    
    Call Mensaje(True, Err.Description)
    Err.Clear

End Sub

Private Sub Update_Datos(Rec As ADODB.Recordset, Optional pNuevo As Boolean = False, Optional ByRef GeneraHablador As Integer = 1)
    
    Dim FS, f, s, Counter As Long
    
    On Error GoTo Errores
    
'    stemp = App.Path & "\STELLAR_BK001.BMP"
'    Set fs = CreateObject("Scripting.FileSystemObject")
'    If fs.FileExists(stemp) Then
'        Set f = fs.getfile(stemp)
'        f.delete
'    End If

    On Error GoTo Error_Imagen
    
    Resp = Dir(imagen_name)
    
    If (Imagen.Picture.Handle <> imagen_hanled) And imagen_name <> "" And Resp <> "" Then
'        stemp = "temp.bmp"
'        LibreArc = FreeFile()
'        Open stemp For Binary Access Read As #LibreArc
'        buffer = Input(LOF(LibreArc), LibreArc)
'        rec.Fields("c_fileimagen").AppendChunk (buffer)
'        Close #LibreArc
        Call PushImageToDb(imagen_name, Rec, "c_fileimagen")
    Else
        If Imagen.Picture.Handle = 0 Then Rec!c_FileImagen = Null
    End If
    
    On Error GoTo 0

    Dim COSTOANTERIOR As Double
    Dim COSTOREPANTERIOR As Double
    Dim PRECIO1ANTERIOR As Double
    Dim PRECIO2ANTERIOR As Double
    Dim PRECIO3ANTERIOR As Double
    COSTOANTERIOR = Rec!N_CostoAct
    COSTOREPANTERIOR = Rec!N_Costorep
    PRECIO1ANTERIOR = Rec!n_precio1
    PRECIO2ANTERIOR = Rec!N_Precio2
    PRECIO3ANTERIOR = Rec!N_Precio3
    
    Rec!c_descri = Descripcion.Text
    Rec!c_departamento = Me.Departamento.Text
    Rec!c_grupo = Me.Grupo.Text
    Rec!c_subgrupo = Me.SubGrupo.Text
    Rec!c_marca = Trim(Marca)
    Rec!c_modelo = Trim(Modelo)
    Rec!c_procede = IIf(Procedencia.ListIndex = 0, 0, 1)
    Rec!N_CostoAct = CDbl(CActual)
    Rec!N_CostoAnt = CDbl(CAnterior)
    Rec!N_CostoPro = CDbl(CPromedio)
    Rec!N_Costorep = CDbl(CReposicion)
    
    If Not pNuevo And BuscarReglaNegocioBoolean("Hablador_SoloenCambiodePrecios", "1") Then
        If Rec!n_precio1 <> CDbl(Precio1) Or Rec!N_Precio2 <> CDbl(Precio2) Or Rec!N_Precio3 <> CDbl(Precio3) Then
            GeneraHablador = 1
        Else
            GeneraHablador = 0
        End If
    End If
    
    Rec!n_precio1 = CDbl(Precio1)
    Rec!N_Precio2 = CDbl(Precio2)
    Rec!N_Precio3 = CDbl(Precio3)
    
    If IsNumeric(Imp1) Then
        Rec!n_impuesto1 = CDbl(Imp1)
    Else
        Rec!n_impuesto1 = 0
    End If
    
    If IsNumeric(Imp2) Then
        Rec!N_Impuesto2 = CDbl(Imp2)
    Else
        Rec!N_Impuesto2 = 0
    End If
    
    If IsNumeric(Imp3) Then
        Rec!N_Impuesto3 = CDbl(Imp3)
    Else
        Rec!N_Impuesto3 = 0
    End If
    
    Rec!C_SERIALES = Seriales.Value
    
    If mGrabar <> "insert" Then
        If (COSTOANTERIOR <> CDbl(CActual)) Or (COSTOREPANTERIOR <> CDbl(CReposicion)) Or (PRECIO1ANTERIOR <> CDbl(Precio1)) Or (PRECIO2ANTERIOR <> CDbl(Precio2)) Or (PRECIO3ANTERIOR <> CDbl(Precio3)) Then
            If ExisteTabla("MA_HISTORICO_COSTO_PRECIO", Ent.BDD) Then
                
                Dim Rs As New ADODB.Recordset
                
                CorrelativoHistorico = NO_CONSECUTIVO("HISTORICO_COSTO_PRECIO", True)
                
                cadena = "insert into MA_HISTORICO_COSTO_PRECIO (c_Historico,d_FechaCambio,c_ProcesoOrigen,c_Documento,c_Usuario,c_CodArticulo,Precio1_Ant,Precio1_Nuevo,Precio2_Ant,Precio2_Nuevo,Precio3_Ant,Precio3_Nuevo,CostoAct_Ant,CostoAct_Nuevo,CostoRep_Ant,CostoRep_Nuevo)" _
                & " values('" & CorrelativoHistorico & "', GETDATE(), 'FICHA DE PRODUCTOS', '','" & lccodusuario & "','" & Codigo & "', " & PRECIO1ANTERIOR & "," & Rec!n_precio1 & "," & PRECIO2ANTERIOR & "," & Rec!N_Precio2 & "," & PRECIO3ANTERIOR & "," & Rec!N_Precio3 & "," & COSTOANTERIOR & "," & Rec!N_CostoAct & "," & COSTOREPANTERIOR & "," & Rec!N_Costorep & ")"
                Rs.Open cadena, Ent.BDD, adOpenDynamic, adLockBatchOptimistic
                
                Debug.Print cadena
            End If
        End If
    End If
    
    If pNuevo Then
        Rec!f_inicial = DateSerial(1980, 1, 1)
        Rec!f_final = DateSerial(1980, 1, 1)
        Rec!H_inicial = "01:00:00"
        Rec!H_final = "01:00:00"
    End If
    
    'rec!c_compuesto = Compuesto.value
    
    Rec!c_OBSERVACIO = Observacion.Text
    Rec!c_presenta = CStr(Left(Presentacion.Text, 8))
    Rec!n_peso = CDbl(Peso)
    Rec!n_volumen = CDbl(Volumen)
    Rec!n_cantibul = CDbl(CantidadBulto)
    Rec!n_pesobul = CDbl(PesoBulto)
    Rec!n_volbul = CDbl(VolumenBulto)
    
    'rec!c_fileimagen = IIf(imagen_prod = "", " ", imagen_prod)

    Rec!c_cod_arancel = cod_Arancel.Text
    Rec!c_des_arancel = des_Arancel.Text
    Rec!n_por_arancel = CDbl(imp_Arancel)
    Rec!n_costo_original = CDbl(Costo_Original)
    Rec!n_activo = Activo.Value
    
    If ExisteCampoTabla("bs_permitecantidad", Rec) Then
        Rec!bs_permitecantidad = chk_Cantidad.Value
    End If
    
    If ExisteCampoTabla("bs_permiteteclado", Rec) Then
        Rec!bs_permiteteclado = chk_Teclado.Value
    End If
    
    If ExisteCampoTabla("nu_stockmin", Rec) Then
        If IsNumeric(txt_StockMin.Text) Then
            Rec!nu_stockmin = CDbl(txt_StockMin.Text)
        Else
            Rec!nu_stockmin = 0
        End If
    End If
    
    If ExisteCampoTabla("nu_stockmax", Rec) Then
        If IsNumeric(txt_StockMax.Text) Then
            Rec!nu_stockmax = CDbl(txt_StockMax.Text)
        Else
            Rec!nu_stockmax = 0
        End If
    End If
    
    Rec!n_tipopeso = Tipo_Producto.ListIndex
    
    Rec!C_CODMONEDA = DBMONEDA.Text
    Rec!C_CODFABRICANTE = " "
    
    If Decimales.Text = "" Then Decimales.Text = 0
    
    Rec!CANT_DECIMALES = CInt(Decimales)
    
    Rec!moneda_act = FormatNumber(CDbl(msk_factor), Std_Decm)
    Rec!MONEDA_ANT = FormatNumber(CDbl(msk_factor), Std_Decm)
    Rec!MONEDA_PRO = FormatNumber(CDbl(msk_factor), Std_Decm)
    
    If mGrabar <> "update" Then
        Rec!c_usuarioadd = lccodusuario
    End If
    
    Rec!c_usuarioupd = lccodusuario
    
    If gCambioHechos Then
        Rec!nu_precioregulado = Me.chk_PrecioRegulado.Value
        Rec!nu_insumointerno = Me.chk_insumo.Value
        Rec!nu_pocentajemerma = Me.txt_PorcentajeMerma
        Rec!cu_descripcion_corta = Me.txt_descripcionCorta
        Rec!nu_nivelClave = Me.cmb_NivelProducto.ListIndex
    End If
    
    Dim MyExte As New ClsExtendidas
    
    If MyExte.bln_Usa_ProdExt(Rec) Then
        Select Case Tipo_Producto.ListIndex
            Case intTipoCarExt
                If Len(Trim(Me.txtCodCarExt.Text)) <> 0 Then
                     Rec!C_COD_PLANTILLA = txtCodCarExt.Text
                     Set MyExte.Connection = Ent.BDD
                     MyExte.Hijos_Actualizar Me.Codigo, Rec!C_COD_PLANTILLA, Me.Descripcion, Me.Marca
                End If
            Case Else
                Rec!C_COD_PLANTILLA = ""
        End Select
    End If
    
    Set MyExte = Nothing
    
    Exit Sub

Error_Imagen:

    Resume Next
    Exit Sub
    
Errores:
    
    Call Mensaje(True, Err.Number & " " & Err.Description)
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".update_datos", Err.Source)
    Unload Me
    
End Sub

Private Sub Buscar_Imagen()

On Error GoTo Errores

    found = Dir(App.Path & "\logo.bmp")
    If found <> "" Then
        Imagen.Picture = LoadPicture(App.Path & "\logo.bmp")
    End If

Exit Sub
Errores:
    
    Call Mensaje(True, Err.Number & " " & Err.Description)
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".BUSCAR_IMAGEN", Err.Source)
    Unload Me
   
End Sub

Private Sub Reformatear_Campos()
    On Error GoTo Errores
    
    CActual = FormatNumber(CDbl(CActual), Std_Decm)
    CAnterior = FormatNumber(CDbl(CAnterior), Std_Decm)
    CPromedio = FormatNumber(CDbl(CPromedio), Std_Decm)
    CReposicion = FormatNumber(CDbl(CReposicion), Std_Decm)
    
    Precio1 = FormatNumber(CDbl(Precio1), Std_Decm)
    Precio2 = FormatNumber(CDbl(Precio2), Std_Decm)
    Precio3 = FormatNumber(CDbl(Precio3), Std_Decm)
    PrecioO = FormatNumber(CDbl(PrecioO), Std_Decm)
    
    Exit Sub
    
Errores:

    Call Mensaje(True, Err.Number & " " & Err.Description)
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".REFORMATEAR_CAMPOS", Err.Source)
    Unload Me
    
End Sub

Private Sub MostrarUbicacionProducto()

    Set mMask = New cls_MascarasUbicacion
    
    'mMask.ConsultaUbicacion = true
    mMask.UbicxProducto = True
    mMask.CodigoUbicar = Trim(Codigo.Text)
    mMask.DescripUbicar = Me.Descripcion
    Call mMask.UbicacionInterfaz(Ent.BDD)
   ' Set Frm_Mascara_Cat.formPadre = Me
    'Frm_Mascara_Cat.Show vbModal
    Set mMask = Nothing
    
End Sub

Private Function CalculoCostoMerma(txtObje)

    On Error GoTo Errores
    
    If txt_PorcentajeMerma = "" Then txt_PorcentajeMerma = FormatNumber(0, 2)
    If txtObje = "" Then txtObje = FormatNumber(0, 2)
    'CalculoCostoMerma = FormatNumber((1 + (CDbl(Me.txt_porcentajemerma) / 100)) * CDbl(txtobje), 2)
    CalculoCostoMerma = FormatNumber(CDbl(txtObje) / (1 - (CDbl(Me.txt_PorcentajeMerma) / 100)), 2)
    
    Exit Function
    
Errores:

    CalculoCostoMerma = 0
    
End Function

Private Sub CalculodeCostosconMerma()
    Me.CReposicion_merma = CalculoCostoMerma(Me.CReposicion)
    Me.CPromedio_Merma = CalculoCostoMerma(Me.CPromedio)
    Me.CAnterior_Merma = CalculoCostoMerma(Me.CAnterior)
    Me.CActual_Merma = CalculoCostoMerma(Me.CActual)
    
    Call CALCULAR_PRECIOS(Precio1, Margen1, Margen1C)
    Call CALCULAR_PRECIOS(Precio2, Margen2, Margen2C)
    Call CALCULAR_PRECIOS(Precio3, Margen3, Margen3C)
End Sub

Private Sub ImportarArchivoTexto()

    Dim mClsArchivo As New cls_Archivo_Productos
    
    mArchivo = BuscarArchivoImportar
    If mArchivo <> "ERROR" Then mClsArchivo.ActualizarProductosMasivo Ent.BDD, CStr(mArchivo)
    
End Sub

Private Function BuscarArchivoImportar() As String
    
    On Error GoTo Errores
    
    With caja
        .DialogTitle = "Ruta Archivo Importar"
        .CancelError = True
        .Filter = "Todos los Archivos|*.*"
        .Flags = cdlOFNPathMustExist
        .ShowOpen
        BuscarArchivoImportar = .FileName
    End With
    
    Exit Function
    
Errores:

    BuscarArchivoImportar = "Error"

End Function

Private Sub BuscarIndiceComboImpuesto(pValor)
    For M = 0 To Imp1.ListCount - 1
        If CStr(Imp1.List(M)) = CStr(pValor) Then
            Imp1.ListIndex = M
            Exit Sub
        End If
    Next M
End Sub

Private Function VerificarTxtNumericos() As Boolean

    If Not IsNumeric(CActual) Then CActual = FormatNumber(0, 2)
    If Not IsNumeric(CAnterior) Then CAnterior = FormatNumber(0, 2)
    If Not IsNumeric(CPromedio) Then CPromedio = FormatNumber(0, 2)
    If Not IsNumeric(CReposicion) Then CReposicion = FormatNumber(0, 2)
    If Not IsNumeric(Precio1) Then CActual = FormatNumber(0, 2)
    If Not IsNumeric(Precio2) Then CAnterior = FormatNumber(0, 2)
    If Not IsNumeric(Precio3) Then CPromedio = FormatNumber(0, 2)
    If Not IsNumeric(PrecioO) Then CReposicion = FormatNumber(0, 2)
    
    VerificarTxtNumericos = True
    
End Function

