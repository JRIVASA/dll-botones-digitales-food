VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form Frm_Super_Consultas 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7035
   ClientLeft      =   1350
   ClientTop       =   1365
   ClientWidth     =   11835
   ControlBox      =   0   'False
   Icon            =   "Frm_Super_Consultas1.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7035
   ScaleMode       =   0  'User
   ScaleWidth      =   11835
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Medir 
      Height          =   135
      Left            =   9600
      ScaleHeight     =   75
      ScaleWidth      =   75
      TabIndex        =   15
      Top             =   1440
      Visible         =   0   'False
      Width           =   135
   End
   Begin VB.CommandButton CmdBuscar 
      Appearance      =   0  'Flat
      Caption         =   "&Buscar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Index           =   0
      Left            =   9360
      Picture         =   "Frm_Super_Consultas1.frx":628A
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   5910
      Width           =   1035
   End
   Begin VB.CommandButton CmdCancelar 
      Appearance      =   0  'Flat
      Caption         =   "&Cancelar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Left            =   10605
      Picture         =   "Frm_Super_Consultas1.frx":800C
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   5910
      Width           =   1035
   End
   Begin VB.Frame FrameTitulo 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   0
      TabIndex        =   11
      Top             =   0
      Width           =   12120
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   11160
         Picture         =   "Frm_Super_Consultas1.frx":9D8E
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   9075
         TabIndex        =   12
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.Frame Frame3 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   900
      Left            =   -90
      TabIndex        =   7
      Top             =   421
      Width           =   11865
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   810
         Left            =   300
         TabIndex        =   8
         Top             =   60
         Width           =   10710
         _ExtentX        =   18891
         _ExtentY        =   1429
         ButtonWidth     =   2170
         ButtonHeight    =   1429
         AllowCustomize  =   0   'False
         Style           =   1
         ImageList       =   "Iconos_Encendidos"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   3
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "&Informaci�n"
               Key             =   "Informacion"
               Object.ToolTipText     =   "Informacion sobre el elemento Seleccionado"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Object.Visible         =   0   'False
               Caption         =   "&Salir"
               Key             =   "Salir"
               Object.ToolTipText     =   "Salir de la B�squeda"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "&Ayuda"
               Key             =   "Ayuda"
               Object.ToolTipText     =   "Ayuda del Sistema"
               ImageIndex      =   3
            EndProperty
         EndProperty
         Begin VB.Timer Tim_Progreso 
            Enabled         =   0   'False
            Interval        =   500
            Left            =   5760
            Top             =   120
         End
      End
   End
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   " Buscar "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   1700
      Left            =   225
      TabIndex        =   6
      Top             =   5250
      Width           =   8955
      Begin VB.TextBox txtDato 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3150
         TabIndex        =   0
         Top             =   495
         Width           =   4500
      End
      Begin VB.ComboBox cmbItemBusqueda 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   240
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   495
         Width           =   2610
      End
      Begin MSComctlLib.ProgressBar Barra_Prg 
         Height          =   225
         Left            =   3150
         TabIndex        =   9
         Top             =   1245
         Width           =   4500
         _ExtentX        =   7938
         _ExtentY        =   397
         _Version        =   393216
         Appearance      =   0
         Max             =   1000
         Scrolling       =   1
      End
      Begin VB.Label lblTeclado 
         BackColor       =   &H8000000A&
         BackStyle       =   0  'Transparent
         Height          =   1005
         Left            =   7755
         TabIndex        =   16
         Top             =   375
         Width           =   1095
      End
      Begin VB.Image CmdTeclado 
         Height          =   600
         Left            =   7995
         MouseIcon       =   "Frm_Super_Consultas1.frx":BB10
         MousePointer    =   99  'Custom
         Picture         =   "Frm_Super_Consultas1.frx":BE1A
         Stretch         =   -1  'True
         Top             =   600
         Width           =   600
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00AE5B00&
         X1              =   2250
         X2              =   8700
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Criterios de busqueda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   0
         Width           =   3015
      End
      Begin VB.Label Lbl_Progreso 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Progreso"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   315
         Left            =   3150
         TabIndex        =   10
         Top             =   990
         Width           =   4785
      End
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   1125
      Top             =   960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Super_Consultas1.frx":BEC6
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Super_Consultas1.frx":DC58
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Super_Consultas1.frx":13C5A
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView GRID 
      CausesValidation=   0   'False
      Height          =   3285
      Left            =   225
      TabIndex        =   4
      Top             =   1770
      Width           =   11350
      _ExtentX        =   20029
      _ExtentY        =   5794
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   5790296
      BackColor       =   16448250
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
   Begin VB.Image CmdSelect 
      Height          =   480
      Left            =   9400
      Picture         =   "Frm_Super_Consultas1.frx":159EC
      Top             =   5280
      Width           =   480
   End
   Begin VB.Image CmdDown 
      Height          =   480
      Left            =   11075
      Picture         =   "Frm_Super_Consultas1.frx":1AA6B
      Top             =   5280
      Width           =   480
   End
   Begin VB.Image CmdUp 
      Height          =   480
      Left            =   10230
      Picture         =   "Frm_Super_Consultas1.frx":1B735
      Top             =   5280
      Width           =   480
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00AE5B00&
      X1              =   480
      X2              =   11575
      Y1              =   1620
      Y2              =   1620
   End
   Begin VB.Image Image1 
      Height          =   2160
      Left            =   3465
      Picture         =   "Frm_Super_Consultas1.frx":1C3FF
      Stretch         =   -1  'True
      Top             =   2580
      Width           =   2400
   End
   Begin VB.Label lblTitulo 
      Appearance      =   0  'Flat
      BackColor       =   &H00FAFAFA&
      BackStyle       =   0  'Transparent
      Caption         =   "#"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   255
      Left            =   225
      TabIndex        =   5
      Top             =   1500
      Width           =   4095
   End
End
Attribute VB_Name = "Frm_Super_Consultas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public WithEvents mClsBusqueda As clsBusqueda
Attribute mClsBusqueda.VB_VarHelpID = -1
Private mPresStop As Boolean
Private mCargando As Boolean
Private FormaCargada As Boolean

Private Sub FrameTitulo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbLeftButton Then MoverVentana Me.hWnd
End Sub

Private Sub lbl_Organizacion_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    FrameTitulo_MouseMove Button, Shift, X, Y
End Sub

Private Sub lbl_website_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    FrameTitulo_MouseMove Button, Shift, X, Y
End Sub

Private Sub cmbItemBusqueda_Click()
    
    Me.txtDato = ""
    
    If txtDato.Enabled And txtDato.Visible Then Me.txtDato.SetFocus
    
End Sub

Private Sub CmdCancelar_Click()
    If mClsBusqueda.Consultando Then
        mClsBusqueda.PresionoStop = True
        If PuedeObtenerFoco(txtDato) Then txtDato.SetFocus
    Else
        Exit_Click
    End If
End Sub

Private Sub CmdTeclado_Click()
    
    'txtDato.SetFocus
    
    'TECLADO.Show vbModal
    TecladoWindows txtDato
    
End Sub

Private Sub CmdUp_Click()
    
    If GRID.ListItems.Count > 0 Then
        
        Dim CantSaltos As Long
        
        CantSaltos = CLng(Val(mClsBusqueda.NumeroLineaAvance))
        
        If (GRID.SelectedItem.Index - CantSaltos) <= 1 Then
            Set GRID.SelectedItem = GRID.ListItems(1)
            GRID.SelectedItem.EnsureVisible
            CmdUp.Visible = False
        Else
            Set GRID.SelectedItem = GRID.ListItems(GRID.SelectedItem.Index - CantSaltos)
            GRID.SelectedItem.EnsureVisible
        End If
        
        CmdDown.Visible = True
        GRID.SetFocus
        
    End If
    
End Sub

Private Sub CmdDown_Click()
    
    If GRID.ListItems.Count > 0 Then
        
        Dim CantSaltos As Long
        CantSaltos = CLng(Val(mClsBusqueda.NumeroLineaAvance))
        
        If (GRID.SelectedItem.Index + CantSaltos) > GRID.ListItems.Count Then
            Set GRID.SelectedItem = GRID.ListItems(GRID.ListItems.Count)
            GRID.SelectedItem.EnsureVisible
            CmdDown.Visible = False
        Else
            Set GRID.SelectedItem = GRID.ListItems(GRID.SelectedItem.Index + CantSaltos)
            GRID.SelectedItem.EnsureVisible
        End If
        
        CmdUp.Visible = True
        GRID.SetFocus
        
    End If
    
End Sub

Private Sub CmdSelect_Click()
    If GRID.ListItems.Count > 0 Then GRID_DblClick
End Sub

Private Sub Exit_Click()
    txtDato_KeyDown vbKeyEscape, 0
End Sub

Private Sub lblUpClickArea_Click()
    CmdUp_Click
End Sub

Private Sub lblDownClickArea_Click()
    CmdDown_Click
End Sub

Private Sub lblSelectClickArea_Click()
    CmdSelect_Click
End Sub

Private Sub lblTeclado_Click()
    CmdTeclado_Click
End Sub

Private Sub CmdBuscar_Click(Index As Integer)
    
    Dim mSQl As String
    Dim mFechaI As Date, mFechaF As Date
    Dim RsBusqueda As New ADODB.Recordset
    
    If mCargando Then Exit Sub
    
    mCargando = True
    
    On Error GoTo Errores
    
    Me.GRID.ListItems.Clear
    
    mSQl = mClsBusqueda.TextoComandoSql '& BuscarWhereFechas
    
    mSQl = mSQl & mClsBusqueda.BuscarFiltroConsulta(mSQl, txtDato, cmbItemBusqueda.ItemData(cmbItemBusqueda.ListIndex), mFechaI, mFechaF)
    
    If mClsBusqueda.OrdenarPor <> vbNullString Then mSQl = mSQl & GetLines & "ORDER BY " & mClsBusqueda.OrdenarPor
    
    If Trim(mSQl) <> "" Then
        
        RsBusqueda.CursorLocation = adUseClient
        RsBusqueda.Open mSQl, mClsBusqueda.ConexionBusqueda, adOpenForwardOnly, adLockReadOnly, adCmdText
        RsBusqueda.ActiveConnection = Nothing
        
        If Not RsBusqueda.EOF Then
            IniciarBarra True, RsBusqueda.RecordCount
            DoEvents
            mClsBusqueda.LlenarLvBusqueda GRID, RsBusqueda
            IniciarBarra False, 0
            If PuedeObtenerFoco(GRID) Then GRID.SetFocus
        Else
            mClsBusqueda.MensajeSistema "No se encontr� informaci�n.", False
        End If
        
        RsBusqueda.Close
        
    End If
    
    mCargando = False
    
    Exit Sub
    
Errores:
    
    mCargando = False
    If RsBusqueda.State = adStateOpen Then RsBusqueda.Close
    mClsBusqueda.MensajeSistema Err.Description, False
    
End Sub

Private Sub Form_Activate()
    
    If mCargando Then txtDato.SetFocus
    mCargando = False
        
    If mClsBusqueda.TamanoLetra > 0 Then GRID.Font.Size = mClsBusqueda.TamanoLetra
    If Trim(mClsBusqueda.NumeroLineaAvance) = vbNullString Then mClsBusqueda.NumeroLineaAvance = "7" ' Default
    
    If Not FormaCargada Then
        FormaCargada = True
        If mClsBusqueda.AutoBusqueda Then txtDato_KeyPress vbKeyReturn
    End If
    
End Sub

Private Sub Form_Load()
    
    mCargando = True
    FormaCargada = False
    Me.lbl_Organizacion.Caption = mClsBusqueda.Titulo
    'optFecha(eOpcionesFecha.ofNiniguna).Value = True
    mClsBusqueda.IniciarFrmBusqueda GRID, cmbItemBusqueda
    
    txtDato.Text = mClsBusqueda.TextoBusquedaInicial
    txtDato.SelStart = Len(txtDato.Text)
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    On Error GoTo Errores
        Select Case KeyCode
                    
            Case Is = vbKeyF1
                'Llamar Ayuda
                
            Case Is = vbKeyF2
                'Me.formPadre.Boton = 1
                'Call grid_DblClick
            
            Case Is = vbKeyF12
                txtDato_KeyDown vbKeyEscape, 0
                
        End Select
    Exit Sub
    
Errores:
    Unload Me
    
End Sub

Private Sub mClsBusqueda_AvanceBusqueda(RegActual As Long, NumRegistros As Long)
    If mCargando Then
        Me.Lbl_Progreso.Caption = "Registro " & RegActual & " de " & NumRegistros
        Me.Barra_Prg.Value = RegActual
    End If
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        
        Case "Informaci�n"
            'Me.FormPadre.boton = 1
            'Call GRID_DblClick
        
        Case "Salir"
            txtDato_KeyDown vbKeyEscape, 0
            
    End Select
End Sub

Private Sub txtDato_Click()
    If ModoTouch Then CmdTeclado_Click
End Sub

Private Sub txtDato_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
       Unload Me
    End If
End Sub

Private Sub txtDato_KeyPress(KeyAscii As Integer)
    
    Select Case KeyAscii
        Case Is = 39
            KeyAscii = 0
        Case Is = vbKeyReturn
            CmdBuscar_Click 0
    End Select
    
End Sub

Private Sub GRID_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    
    With GRID
        If .AllowColumnReorder Then
            .SortOrder = IIf(.SortOrder = lvwDescending, lvwAscending, lvwDescending)
            .SortKey = ColumnHeader.Index - 1
            .Sorted = True
        End If
    End With
        
End Sub

Private Sub GRID_DblClick()
    If Not GRID.SelectedItem Is Nothing Then
        If mClsBusqueda.ValidarSeleccion(GRID) Then
            mClsBusqueda.ResultadoBusqueda = mClsBusqueda.TomarDatosSeleccionados(GRID)
            mClsBusqueda.Consultando = False: mCargando = False
            Unload Me
        Else
            mClsBusqueda.MensajeSistema IIf(mClsBusqueda.MultiSeleccion, "No ha marcado CheckBox de ning�n Item.", "Debe seleccionar al menos un Item."), False
        End If
    End If
End Sub

Private Sub grid_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then GRID_DblClick
End Sub

Private Sub IniciarBarra(Mostrar As Boolean, NumReg As Long)
    Me.Barra_Prg.Min = 0
    Me.Barra_Prg.Value = 0
    Me.Barra_Prg.Max = NumReg + 1
    Me.Barra_Prg.Visible = Mostrar
    Me.Lbl_Progreso.Visible = Mostrar
End Sub
