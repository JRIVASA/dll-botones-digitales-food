VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form frmBotones 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10425
   ClientLeft      =   15
   ClientTop       =   60
   ClientWidth     =   10695
   ControlBox      =   0   'False
   Icon            =   "frmBotones.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10425
   ScaleWidth      =   10695
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame FrameTitulo 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   33
      Top             =   0
      Width           =   13680
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Botones Perfil"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   35
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   8715
         TabIndex        =   34
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -480
      ScaleHeight     =   1050
      ScaleWidth      =   14505
      TabIndex        =   31
      Top             =   421
      Width           =   14535
      Begin MSComctlLib.Toolbar tb 
         Height          =   810
         Left            =   600
         TabIndex        =   32
         Top             =   120
         Width           =   12165
         _ExtentX        =   21458
         _ExtentY        =   1429
         ButtonWidth     =   1402
         ButtonHeight    =   1429
         Style           =   1
         ImageList       =   "ImageList1"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   14
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Importar"
               Key             =   "perfil"
               Object.ToolTipText     =   "Importar Perfil"
               ImageIndex      =   1
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   2
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "ImportarNuevo"
                     Text            =   "Importar Perfil"
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "ImportarExistente"
                     Text            =   "Modificar Perfil"
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Nuevo"
               Key             =   "add"
               ImageIndex      =   2
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   3
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Object.Visible         =   0   'False
                     Key             =   "AgregarBoton"
                     Text            =   "Bot�n"
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Object.Visible         =   0   'False
                     Key             =   "AgregarPlato"
                     Text            =   "Plato"
                  EndProperty
                  BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Object.Visible         =   0   'False
                     Key             =   "AgregarProducto"
                     Text            =   "Producto"
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Editar"
               Key             =   "editar"
               Object.ToolTipText     =   "Editar Propiedades Boton"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Grabar"
               Key             =   "guardar"
               Object.ToolTipText     =   "Grabar Cambios"
               ImageIndex      =   4
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Eliminar"
               Key             =   "eliminar"
               Object.ToolTipText     =   "Eliminar Boton"
               ImageIndex      =   5
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Grupo"
               Key             =   "grupo"
               Object.ToolTipText     =   "Agregar Grupo"
               ImageIndex      =   6
            EndProperty
            BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Cambiar"
               Key             =   "cambiar"
               ImageIndex      =   7
            EndProperty
            BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Ins Izq"
               Key             =   "insertarI"
               ImageIndex      =   8
            EndProperty
            BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Ins. Der"
               Key             =   "insertarD"
               ImageIndex      =   9
            EndProperty
            BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button14 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Salir"
               Key             =   "salir"
               ImageIndex      =   10
            EndProperty
         EndProperty
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   6840
      Top             =   1560
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBotones.frx":000C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBotones.frx":1D9E
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBotones.frx":2A78
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBotones.frx":480A
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBotones.frx":659C
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBotones.frx":832E
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBotones.frx":A0C0
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBotones.frx":BE52
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBotones.frx":DBE4
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBotones.frx":F976
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   8295
      Left            =   960
      TabIndex        =   0
      Top             =   1800
      Width           =   8535
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   5
         Left            =   6915
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   360
         Width           =   1365
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   4
         Left            =   5580
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   360
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   3
         Left            =   4245
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   360
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   2
         Left            =   2910
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   360
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   1
         Left            =   1575
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   360
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   0
         Left            =   240
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   360
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   11
         Left            =   6915
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   1920
         Width           =   1365
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   10
         Left            =   5580
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   1920
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   9
         Left            =   4245
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   1920
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   8
         Left            =   2910
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   1920
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   7
         Left            =   1575
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   1920
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   6
         Left            =   240
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   1920
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   17
         Left            =   6915
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   3480
         Width           =   1365
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   16
         Left            =   5580
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   3480
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   15
         Left            =   4245
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   3480
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   14
         Left            =   2910
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   3480
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   13
         Left            =   1575
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   3480
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   12
         Left            =   240
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   3480
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   23
         Left            =   6960
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   5040
         Width           =   1365
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   22
         Left            =   5580
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   5040
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   21
         Left            =   4245
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   5040
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   20
         Left            =   2910
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   5040
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   19
         Left            =   1575
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   5040
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   18
         Left            =   240
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   5040
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         BackColor       =   &H00E0E0E0&
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   29
         Left            =   6915
         MaskColor       =   &H00000000&
         Picture         =   "frmBotones.frx":11708
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   6600
         Width           =   1365
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   28
         Left            =   5580
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   6600
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   27
         Left            =   4245
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   6600
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   26
         Left            =   2910
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   6600
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   25
         Left            =   1575
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   6600
         Width           =   1335
      End
      Begin VB.CommandButton boton 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1454
         Index           =   24
         Left            =   240
         MaskColor       =   &H00000000&
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   6600
         Width           =   1335
      End
      Begin VB.Shape shSel 
         BorderWidth     =   2
         Height          =   1480
         Index           =   0
         Left            =   240
         Top             =   350
         Width           =   1335
      End
   End
End
Attribute VB_Name = "frmBotones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public fCls As clsConfigBotones
Private mBotonSel As clsBoton
Private mPosBoton As Integer, mPosBotonCambiar As Integer
Private mCambiarPos As Boolean, mCambiosRealizados As Boolean
Private mRelacion As Collection
Private Const MaxBoton As Integer = 29
Private mEjecuto As Boolean, mCargando As Boolean
Private ModificarPerfil As Boolean

Private Sub FrameTitulo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbLeftButton Then MoverVentana Me.hWnd
End Sub

Private Sub lbl_Organizacion_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    FrameTitulo_MouseMove Button, Shift, X, Y
End Sub

Private Sub lbl_website_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    FrameTitulo_MouseMove Button, Shift, X, Y
End Sub

Private Sub boton_Click(Index As Integer)
    
    If Index = MaxBoton And Not mEjecuto Then boton_GotFocus Index
    mEjecuto = False
    
    If mCambiarPos Then CambiarPosicion tb.Buttons("cambiar")
    
End Sub

Private Sub boton_GotFocus(Index As Integer)
    Dim Relacion As String
    
    mEjecuto = True
    If Index < MaxBoton Then
        
        Relacion = mRelacion.Item(mRelacion.Count)
        Set mBotonSel = SeleccionarBoton(Index + 1, Relacion)
        
        If mCambiarPos Then
            mPosBotonCambiar = Index
            If shSel.Count = 1 Then
                Load shSel(1)
                shSel(1).Visible = True
                MoverShape shSel(1), Index
            Else
                MoverShape shSel(1), Index
            End If
        Else
            mPosBoton = Index
            mPosBotonCambiar = Index
            MoverShape shSel(0), Index
        End If
        ActivarToolbar
    Else
        
        Set mBotonSel = Nothing
        ActivarToolbar
        If Not mCargando Then
            If mRelacion.Count > 1 Then mRelacion.Remove (mRelacion.Count)
            LlenarBotones mRelacion.Item(mRelacion.Count)
        End If
        mCargando = False
    End If
End Sub

Private Sub Form_Load()
    Set cMensaje = New clsMensajeria
    Set mRelacion = New Collection
    mRelacion.Add "RAIZ"
    LlenarBotones "RAIZ"
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If mCambiosRealizados Then
        If Not fCls.Mensaje("�Esta seguro de salir sin guardar?", True) Then
            Cancel = 1
        End If
    End If
End Sub

Private Sub tb_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case LCase(Button.Key)
        Case "add"
            Set mBotonSel = SeleccionarBoton(mPosBoton + 1, mRelacion.Item(mRelacion.Count))
            If mBotonSel Is Nothing Then
                AgregarBoton mRelacion.Item(mRelacion.Count), mPosBoton + 1
            End If
        Case "perfil"
            ImportarPerfil
        Case "editar"
            EditarBoton
        Case "guardar"
            If fCls.GrabarPerfil Then Unload Me
        Case "eliminar"
            EliminarBoton
        Case "grupo"
            ConfigurarGrupo
        Case "cambiar"
            CambiarPosicion Button
        Case "insertari"
            Insertar True
        Case "insertard"
            Insertar False
        Case "salir"
            Unload Me
    End Select
End Sub

Private Sub tb_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
    Select Case UCase(ButtonMenu.Key)
        Case UCase("AgregarBoton")
            tb_ButtonClick ButtonMenu.Parent
        Case UCase("AgregarPlato") ' Se manejar� en el BUSINESS por usuario como opci�n separada.
'            Dim ClsConfigPlatos As Object
'            Set ClsConfigPlatos = SafeCreateObject("DLLConfigPlatos.ClsConfigPlatos")
'
'            If ClsConfigPlatos Is Nothing Then
'                fCls.Mensaje "El m�dulo no esta correctamente instalado / registrado." _
'                & vbNewLine & "Por favor notifique al personal de Soporte T�cnico."
'            Else
'
'                ClsConfigPlatos.IniciarInterfaz fCls.Conexion.Servidor, 0
'
'                Set ClsConfigPlatos = Nothing
'
'            End If
        Case UCase("AgregarProducto") ' Se manejar� en el BUSINESS por usuario como opci�n separada.
        
        Case UCase("ImportarNuevo")
            ModificarPerfil = False
            tb_ButtonClick ButtonMenu.Parent
        Case UCase("ImportarExistente")
            ModificarPerfil = True
            tb_ButtonClick ButtonMenu.Parent
    End Select
End Sub

Private Sub Insertar(Optional Izquierda As Boolean = True)

    Dim nPosActual As Integer, nProxPosDisp As Integer
    Dim mBtn As clsBoton
    
    nPosActual = mBotonSel.Posicion - 1
    nProxPosDisp = ProxPosicionInsertar(Izquierda, nPosActual)
    If nProxPosDisp < 0 Or nProxPosDisp > 28 Then Exit Sub
    
    If Izquierda Then
        For I = nProxPosDisp + 1 To nPosActual
            Set mBtn = SeleccionarBoton(I + 1, mBotonSel.Relacion)
            If Not mBtn Is Nothing Then
                mBtn.Posicion = mBtn.Posicion - 1
            Else
                Exit For
            End If
        Next I
    Else
        For I = nProxPosDisp - 1 To nPosActual Step -1
            Set mBtn = SeleccionarBoton(I + 1, mBotonSel.Relacion)
            If Not mBtn Is Nothing Then
                mBtn.Posicion = mBtn.Posicion + 1
            Else
                Exit For
            End If
        Next
    End If
    
    LlenarBotones mRelacion.Item(mRelacion.Count)
    
End Sub

Private Function ProxPosicionInsertar(Izquierda As Boolean, PosActual As Integer) As Integer
    If Izquierda Then
        For I = PosActual To 0 Step -1
            If boton(I).Caption = "" Then
                ProxPosicionInsertar = I
                Exit Function
            End If
        Next
    Else
        For I = PosActual To 28
            If boton(I).Caption = "" Then
                ProxPosicionInsertar = I
                Exit Function
            End If
        Next
    End If
    ProxPosicionInsertar = I
End Function

Private Sub MoverShape(ShapeSel As Shape, Index As Integer)
    ShapeSel.Move boton(Index).Left - 10, boton(Index).Top - 10
End Sub

Private Sub ActivarBottones(Activar As Boolean, Optional IndexEx As Integer = -1)
    Dim mButton As Button
    
    For Each mButton In tb.Buttons
        If mButton.Index <> IndexEx Then
            mButton.Enabled = Activar
        End If
    Next
End Sub

Private Sub LlenarBotones(Relacion As String)
    
    Dim mBoton As clsBoton
    
    For I = 0 To 28
        IniciarBoton CInt(I)
    Next
    
    For Each mBoton In fCls.Botones.Botones
        If StrComp(mBoton.Relacion, Relacion, vbTextCompare) = 0 Then
            With boton(mBoton.Posicion - 1)
                .Caption = mBoton.Descripcion
                .BackColor = mBoton.Color
                .FontName = mBoton.Fuente.Name
                .FontBold = mBoton.Fuente.Bold
                .FontItalic = mBoton.Fuente.Italic
                .FontSize = mBoton.Fuente.Size
                .FontUnderline = mBoton.Fuente.Underline
                If mBoton.Imagen.Handle <> 0 Then
                    .Picture = mBoton.Imagen
                End If
            End With
            
        End If
    Next
    
    mPosBoton = MaxBoton
    MoverShape shSel(0), MaxBoton
    mCargando = True
    
    If boton(MaxBoton).Enabled And boton(MaxBoton).Visible Then boton(MaxBoton).SetFocus
    
End Sub

Private Function SeleccionarBoton(POS As Integer, Relacion As String) As clsBoton
    
    Dim mBot As clsBoton
    
    For Each mBot In fCls.Botones.Botones
        If mBot.Posicion = POS And StrComp(mBot.Relacion, Relacion, vbTextCompare) = 0 Then
            Set SeleccionarBoton = mBot
            Exit Function
        End If
    Next
    
    A = 1
    
End Function

Private Sub ActivarToolbar()
    
    tb.Buttons("editar").Enabled = Not mBotonSel Is Nothing And Not mCambiarPos And mPosBoton <> MaxBoton
    tb.Buttons("cambiar").Enabled = Not mBotonSel Is Nothing Or mCambiarPos And mPosBoton <> MaxBoton
    tb.Buttons("insertarI").Enabled = Not mBotonSel Is Nothing And Not mCambiarPos And mPosBoton <> MaxBoton
    tb.Buttons("insertarD").Enabled = Not mBotonSel Is Nothing And Not mCambiarPos And mPosBoton <> MaxBoton
    tb.Buttons("eliminar").Enabled = Not mBotonSel Is Nothing And Not mCambiarPos And mPosBoton <> MaxBoton
    tb.Buttons("add").Enabled = mBotonSel Is Nothing And Not mCambiarPos And mPosBoton <> MaxBoton
    tb.Buttons("guardar").Enabled = Not mCambiarPos
    tb.Buttons("perfil").Enabled = Not mCambiarPos
    
    If Not mCambiarPos Then
        If Not mBotonSel Is Nothing Then
            tb.Buttons("grupo").Enabled = mBotonSel.esGrupo
        Else
            tb.Buttons("grupo").Enabled = False
        End If
    End If
    
End Sub

Private Sub EliminarBoton()
    
    Dim nBoton As Integer
    Dim mBotonE As clsBoton
    
    If fCls.Mensaje("�Est� Seguro?", True) Then
        If ValidarEliminar(mBotonSel.Clave) Then
            Do While nBoton < fCls.Botones.Count
                nBoton = nBoton + 1
                Set mBotonE = fCls.Botones.Item(nBoton)
                If mBotonE.Posicion = mBotonSel.Posicion And _
                    StrComp(mBotonE.Relacion, mBotonSel.Relacion, vbTextCompare) = 0 Then
                      'eliminar el boton seleccionado
                    fCls.Botones.Remove (nBoton)
                    nBoton = nBoton - 1
                ElseIf StrComp(mBotonSel.Clave, mBotonE.Relacion, vbTextCompare) = 0 Then
                    ' eliminar items del grupo
                    fCls.Botones.Remove (nBoton)
                    nBoton = nBoton - 1
                End If
            Loop
            LlenarBotones mBotonSel.Relacion
        End If
    End If
    
End Sub

Private Sub IniciarBoton(POS As Integer)
    boton(POS).Caption = ""
    boton(POS).BackColor = BotonBkColor
    boton(POS).Font = boton(MaxBoton).Font
    boton(POS).FontSize = boton(MaxBoton).FontSize
    boton(POS).FontBold = boton(MaxBoton).FontBold
    boton(POS).FontItalic = boton(MaxBoton).FontItalic
    boton(POS).FontUnderline = boton(MaxBoton).FontUnderline
    boton(POS).Picture = LoadPicture()
End Sub

Private Sub ConfigurarGrupo()
    If Not mBotonSel Is Nothing Then
        If mBotonSel.esGrupo Then
            mRelacion.Add mBotonSel.Clave
            LlenarBotones mBotonSel.Clave
        End If
    End If
End Sub

Private Sub CambiarPosicion(Button As Button)
    
    Dim mBtn As clsBoton
    
    If Not mCambiarPos Then
        mCambiarPos = True
        boton(MaxBoton).Enabled = False
        Button.Value = tbrPressed
        ActivarBottones False, Button.Index
    Else
        mCambiarPos = False
        boton(MaxBoton).Enabled = True
        Button.Value = tbrUnpressed
        If shSel.Count = 2 Then Unload shSel(1)
        If mPosBotonCambiar <> mPosBoton Then
            'MoverBoton
        
            
            Set mBtn = SeleccionarBoton(mPosBoton + 1, mRelacion.Item(mRelacion.Count))
            If mBtn Is Nothing Then Exit Sub
            mBtn.Posicion = mPosBotonCambiar + 1
            
            If Not mBotonSel Is Nothing Then
                mBotonSel.Posicion = mPosBoton + 1
            End If
            LlenarBotones mBtn.Relacion
        End If
        mPosBotonCambiar = 0
        ActivarBottones True
        LlenarBotones mRelacion.Item(mRelacion.Count)
    End If
    
End Sub

Private Sub EditarBoton()
    If Not mBotonSel Is Nothing Then
        fCls.AgregarModificarBoton mBotonSel, boton(mBotonSel.Posicion - 1)
        LlenarBotones mRelacion.Item(mRelacion.Count)
    End If
End Sub

Private Sub AgregarBoton(Relacion As String, POS As Integer)
    
    Dim mBoton As clsBoton
    
    Set mBoton = New clsBoton
    mBoton.Clave = "K" & fCls.Conexion.Correlativo("ClaveTmp")
    mBoton.Color = BotonBkColor
    mBoton.Fuente.Bold = boton(MaxBoton).FontBold
    mBoton.Fuente.Name = boton(MaxBoton).FontName
    mBoton.Fuente.Italic = boton(MaxBoton).FontItalic
    mBoton.Fuente.Size = boton(MaxBoton).FontSize
    mBoton.Relacion = Relacion
    mBoton.Posicion = POS
    mBoton.tecla = ""
    mBoton.TeclaMask = ""
    
    fCls.AgregarModificarBoton mBoton, boton(POS - 1), True
    
    LlenarBotones Relacion
    
End Sub

Private Function ValidarEliminar(Clave As String) As Boolean
    
    Dim mBtn As clsBoton
    
    For Each mBtn In fCls.Botones.Botones
        If StrComp(mBtn.Relacion, Clave, vbTextCompare) = 0 Then
            ValidarEliminar = fCls.Mensaje("El grupo posee Items relacionados, �Est� seguro de eliminar?", True)
            Exit Function
        End If
    Next
    
    ValidarEliminar = True
    
End Function

Private Sub ImportarPerfil()
    
    Dim mBusqueda As clsBusqueda
    Dim mSQl As String
    Dim mResul As Variant
    
    On Error GoTo Errores
    
    If ValidarImportar Then
        
        Set mBusqueda = New clsBusqueda
        
        mSQl = "SELECT DISTINCT CodigoPerfil, Descripcion " & _
        "FROM MA_CAJA_BOTONES_DIGITALES_PERFIL " & _
        "WHERE CodigoPerfil <> '" & fCls.PerfilSel.Perfil & "' "
                
        With mBusqueda
            
            Set .ConexionBusqueda = fCls.Conexion.Conexion
            .AgregarCamposBusqueda "Codigo", "codigoperfil", "codigoperfil", 3030, True, opCampoStr
            .AgregarCamposBusqueda "Descripcion", "Descripcion", "Descripcion", 8070, True, opCampoStr, True
            .TextoComandoSql = mSQl
            .OrdenarPor = "CodigoPerfil DESC"
            .MostrarInterfazBusqueda "P E R F I L E S", True, False, , , "%"
            
            If Not IsEmpty(.ResultadoBusqueda) Then
                 If fCls.BuscarBotones(CStr(.ResultadoBusqueda(0)(0)), True) Then
                    Do While mRelacion.Count > 1
                       mRelacion.Remove mRelacion.Count
                    Loop
                    LlenarBotones mRelacion.Item(mRelacion.Count)
                    If ModificarPerfil Then fCls.PerfilSel.Perfil = CStr(.ResultadoBusqueda(0)(0))
                Else
                    Unload Me
                End If
                 
            End If
            
        End With
        
        Set mBusqueda = Nothing
        
    End If
    
    Exit Sub
    
Errores:
    
    fCls.Mensaje Err.Description, False
    Err.Clear
    
End Sub

Private Function ValidarImportar() As Boolean
    If fCls.Botones.Count > 0 Then
        If Not fCls.Mensaje("�Est� seguro de cambiar la configuraci�n actual?", True) Then Exit Function
    End If
    ValidarImportar = True
End Function
