VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8040
   ClientLeft      =   15
   ClientTop       =   75
   ClientWidth     =   11010
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8040
   ScaleWidth      =   11010
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Coolbar 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   11145
      TabIndex        =   4
      Top             =   480
      Width           =   11175
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   810
         Left            =   240
         TabIndex        =   5
         Top             =   120
         Width           =   8520
         _ExtentX        =   15028
         _ExtentY        =   1429
         ButtonWidth     =   1720
         ButtonHeight    =   1429
         Style           =   1
         ImageList       =   "ImageList2"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   3
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Crear Perfil"
               Key             =   "perfil"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               ImageIndex      =   1
               Style           =   3
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Salir"
               Key             =   "salir"
               ImageIndex      =   2
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame FrameTitulo 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   12120
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   8235
         TabIndex        =   3
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Configuración de Botones POS - FOOD"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   75
         Width           =   5295
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   10320
         Picture         =   "frmMain.frx":0000
         Top             =   0
         Width           =   480
      End
   End
   Begin MSFlexGridLib.MSFlexGrid mfgrid 
      Height          =   6015
      Left            =   240
      TabIndex        =   0
      Top             =   1800
      Width           =   10455
      _ExtentX        =   18441
      _ExtentY        =   10610
      _Version        =   393216
      RowHeightMin    =   500
      BackColor       =   16448250
      ForeColor       =   4210752
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   16761024
      ForeColorSel    =   16777215
      BackColorBkg    =   16448250
      GridColor       =   4210752
      GridLinesFixed  =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   9240
      Top             =   1200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1D82
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   9840
      Top             =   1200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2A5C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":3736
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Enum eColGridCajas
    eColGrdCodigo
    eColGrdDescripcion
    eColGrdPerfil
    eColGrdBotones
End Enum

Public fCls As clsConfigBotones
Private mCajaSel As clsCaja
Private mPerfilSel As clsPerfil
    
Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub FrameTitulo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbLeftButton Then MoverVentana Me.hWnd
End Sub

Private Sub lbl_Organizacion_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    FrameTitulo_MouseMove Button, Shift, X, Y
End Sub

Private Sub lbl_website_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    FrameTitulo_MouseMove Button, Shift, X, Y
End Sub
    
Private Sub Form_Load()
    IniciarForm
End Sub

Private Sub IniciarForm()
    IniciarGrid
    LlenarCajas
End Sub

Private Sub IniciarGrid()
    Dim I As Integer
    With Me.mfgrid
        .Clear
        .Rows = 2
        .RowHeightMin = 800
        .Cols = eColGridCajas.eColGrdBotones + 1
        .Row = 0
        For I = 0 To .Cols - 1
            .Col = I
            .ColWidth(I) = BuscarAncho(I)
            .Text = BuscarTexto(I)
            .CellAlignment = 4
            
        Next
    End With
End Sub

Private Function BuscarAncho(Col As Integer) As Integer
    Select Case Col
        Case eColGrdCodigo
            BuscarAncho = 0
        Case eColGrdDescripcion
            BuscarAncho = 7100
        Case eColGrdPerfil
            BuscarAncho = 2000
        Case eColGrdBotones
            BuscarAncho = 1000
    
    End Select
End Function

Private Function BuscarTexto(Col As Integer) As String
    Select Case Col
        Case eColGrdCodigo
            BuscarTexto = ""
        Case eColGrdDescripcion
            BuscarTexto = "Caja"
        Case eColGrdPerfil
            BuscarTexto = "Perfil"
        Case eColGrdBotones
            BuscarTexto = "Config."
    End Select
End Function

Private Sub LlenarCajas()
    
    Dim mCaja As clsCaja
    Dim I As Integer
    
    For Each mCaja In fCls.Cajas.Cajas
        I = I + 1
        LlenarGridCaja mCaja, I
    Next
    
End Sub

Private Sub LlenarGridCaja(caja As clsCaja, Fila As Integer)
    If Fila > mfgrid.Rows - 1 Then mfgrid.Rows = mfgrid.Rows + 1
    mfgrid.TextMatrix(Fila, eColGridCajas.eColGrdCodigo) = caja.Codigo
    mfgrid.TextMatrix(Fila, eColGridCajas.eColGrdDescripcion) = caja.Descripcion
    mfgrid.TextMatrix(Fila, eColGridCajas.eColGrdPerfil) = caja.Perfil
    mfgrid.Col = eColGridCajas.eColGrdBotones
    mfgrid.Row = Fila
    Set mfgrid.CellPicture = ImageList1.ListImages(1).Picture
    mfgrid.CellPictureAlignment = 4
End Sub

Private Sub LlenarGridPerfil(Perfil As clsPerfil, Fila As Integer)
    If Fila > mfgrid.Rows - 1 Then mfgrid.Rows = mfgrid.Rows + 1
    mfgrid.TextMatrix(Fila, eColGridCajas.eColGrdCodigo) = Perfil.Perfil
    mfgrid.TextMatrix(Fila, eColGridCajas.eColGrdDescripcion) = Perfil.Descripcion
    mfgrid.TextMatrix(Fila, eColGridCajas.eColGrdPerfil) = Perfil.Perfil
    mfgrid.Col = eColGridCajas.eColGrdBotones
    mfgrid.Row = Fila
    Set mfgrid.CellPicture = ImageList1.ListImages(1).Picture
    mfgrid.CellPictureAlignment = 4
End Sub

Private Sub mfgrid_Click()
    If SeleccionarCaja(mfgrid.Row) Then
        If mfgrid.Col = eColGridCajas.eColGrdBotones Then
            fCls.ConfigurarPerfil mCajaSel.Perfil
            fCls.LlenarCajas
            IniciarForm
        End If
    End If
End Sub

Private Function SeleccionarCaja(Fila As Integer) As Boolean
    If fCls.Cajas.Count >= Fila Then
        Set mCajaSel = fCls.Cajas.Item(Fila)
        SeleccionarCaja = True
    End If
End Function

Private Function SeleccionarPerfil(Fila As Integer) As Boolean
    If fCls.Perfiles.Count >= Fila Then
        Set mPerfilSel = fCls.Perfiles(Fila)
        SeleccionarPerfil = True
    End If
End Function

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case LCase(Button.Key)
        Case "perfil"
            
            fCls.ConfigurarPerfil ""
            fCls.LlenarCajas
            IniciarForm
           
        Case "salir"
            Unload Me
    End Select
End Sub
