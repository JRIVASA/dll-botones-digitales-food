VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSComCt2.ocx"
Begin VB.Form frmPerfil 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   5805
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   11010
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5805
   ScaleWidth      =   11010
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkDia 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Todo el dia"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   375
      Left            =   720
      TabIndex        =   11
      Top             =   3120
      Width           =   2655
   End
   Begin VB.ListBox lstDias 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   20.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3705
      Left            =   7440
      Style           =   1  'Checkbox
      TabIndex        =   10
      Top             =   1800
      Width           =   3255
   End
   Begin MSComCtl2.DTPicker dtHini 
      Height          =   375
      Left            =   840
      TabIndex        =   6
      Top             =   4680
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   232914946
      CurrentDate     =   42608
   End
   Begin VB.TextBox txtDescripcion 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   375
      Left            =   600
      TabIndex        =   4
      Top             =   2400
      Width           =   6015
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   14505
      TabIndex        =   3
      Top             =   421
      Width           =   14535
      Begin MSComctlLib.Toolbar tb 
         Height          =   810
         Left            =   240
         TabIndex        =   12
         Top             =   120
         Width           =   12165
         _ExtentX        =   21458
         _ExtentY        =   1429
         ButtonWidth     =   1323
         ButtonHeight    =   1429
         Style           =   1
         ImageList       =   "ImageList1"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   11
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Object.Visible         =   0   'False
               Caption         =   "Importar"
               Key             =   "importar"
               Object.ToolTipText     =   "Importar Perfil"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Object.Visible         =   0   'False
               Style           =   3
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Object.Visible         =   0   'False
               Caption         =   "Nuevo"
               Key             =   "add"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Object.Visible         =   0   'False
               Caption         =   "Editar"
               Key             =   "editar"
               Object.ToolTipText     =   "Editar Propiedades Boton"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Grabar"
               Key             =   "guardar"
               Object.ToolTipText     =   "Grabar Cambios"
               ImageIndex      =   4
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Eliminar"
               Key             =   "eliminar"
               Object.ToolTipText     =   "Eliminar Boton"
               ImageIndex      =   5
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Botones"
               Key             =   "botones"
               Object.ToolTipText     =   "Agregar Grupo"
               ImageIndex      =   6
            EndProperty
            BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Salir"
               Key             =   "salir"
               ImageIndex      =   10
            EndProperty
            BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Teclado"
               Key             =   "Teclado"
               Description     =   "Teclado"
               Object.ToolTipText     =   "Teclado"
               ImageIndex      =   12
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame FrameTitulo 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   13680
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   10320
         Picture         =   "frmPerfil.frx":0000
         Top             =   -30
         Width           =   480
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   8235
         TabIndex        =   2
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Perfil de Botones"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   75
         Width           =   5295
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   5760
      Top             =   1320
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   12
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPerfil.frx":1D82
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPerfil.frx":3B14
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPerfil.frx":47EE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPerfil.frx":6580
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPerfil.frx":8312
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPerfil.frx":A0A4
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPerfil.frx":BE36
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPerfil.frx":DBC8
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPerfil.frx":F95A
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPerfil.frx":116EC
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPerfil.frx":1347E
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPerfil.frx":15210
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComCtl2.DTPicker dthFin 
      Height          =   375
      Left            =   3360
      TabIndex        =   9
      Top             =   4680
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   135069698
      CurrentDate     =   42608
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Fin"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   255
      Index           =   2
      Left            =   3360
      TabIndex        =   8
      Top             =   4320
      Width           =   1575
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Inicio"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   255
      Index           =   1
      Left            =   840
      TabIndex        =   7
      Top             =   4320
      Width           =   1575
   End
   Begin VB.Label Label2 
      Height          =   1095
      Left            =   480
      TabIndex        =   14
      Top             =   4200
      Width           =   6375
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Descripcion"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   255
      Index           =   0
      Left            =   600
      TabIndex        =   5
      Top             =   2040
      Width           =   3135
   End
   Begin VB.Label Fondo 
      BackColor       =   &H00E7E8E8&
      Height          =   3735
      Left            =   240
      TabIndex        =   13
      Top             =   1800
      Width           =   6855
   End
End
Attribute VB_Name = "frmPerfil"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public fCls As clsConfigBotones

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    If PuedeObtenerFoco(txtDescripcion) Then txtDescripcion.SetFocus
End Sub

Private Sub FrameTitulo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbLeftButton Then MoverVentana Me.hWnd
End Sub

Private Sub lbl_Organizacion_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    FrameTitulo_MouseMove Button, Shift, X, Y
End Sub

Private Sub lbl_website_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    FrameTitulo_MouseMove Button, Shift, X, Y
End Sub

Private Sub chkDia_Click()
    HabilitarHoras chkDia.Value = 0
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        Case vbKeyF2
            If BotonActivo("importar") Then Buscar
        
        Case vbKeyF3
            If BotonActivo("add") Then Nuevo
            
        Case vbKeyF4
            If BotonActivo("guardar") Then Guardar
            
        Case vbKeyF5
            If BotonActivo("editar") Then Editar
            
        Case vbKeyF6
            If BotonActivo("eliminar") Then Eliminar
            
        Case vbKeyF7
            'If BotonActivo("cancelar") Then Cancelar
        
        Case vbKeyF10
            If BotonActivo("botones") Then ConfigurarBotones
            
        Case vbKeyF12
            Unload Me
    End Select
    
End Sub

Private Sub Form_Load()
    IniciarForm
End Sub

Private Sub IniciarForm()
    If fCls.PerfilSel.Botones.Count = 0 And fCls.PerfilSel.Perfil = vbNullString Then
        FormNuevo
    Else
        CargarForm
    End If
End Sub

Private Sub FormNuevo()
    
    fCls.IniciarPerfil
    
    Me.dtHini.Value = Now
    Me.dthFin.Value = Now
    
    Me.lstDias.Clear
    
    For I = 1 To 7
        lstDias.AddItem WeekdayName(I, , vbMonday)
        lstDias.Selected(I - 1) = True
    Next I
    
    lstDias.Enabled = fCls.CamposNuevos
    chkDia.Enabled = fCls.CamposNuevos
    
    chkDia.Value = 1
    HabilitarHoras False
    EstadoToolbar estbStandard
    
    ' Mientras no exista a�n la funcionalidad acorde en el FOOD. Deshabilitar uso de campos.
    
    lstDias.Enabled = False
    dtHini.Enabled = False
    dthFin.Enabled = False
    chkDia.Enabled = False
    
End Sub

Private Sub CargarForm()
    
    Me.dtHini.Value = fCls.PerfilSel.HoraIni
    Me.dthFin.Value = fCls.PerfilSel.HoraFin
    Me.txtDescripcion.Text = fCls.PerfilSel.Descripcion
    Me.lstDias.Clear
    
    If fCls.CamposNuevos Then
        For I = 0 To 6
            lstDias.AddItem WeekdayName(I + 1, , vbMonday)
            lstDias.Selected(I) = 2 ^ I And fCls.PerfilSel.Dias
        Next I
        chkDia.Value = IIf(DateDiff("n", fCls.PerfilSel.HoraIni, fCls.PerfilSel.HoraFin) >= 1439, 1, 0)
        HabilitarHoras chkDia.Value = 0
    Else
        chkDia.Enabled = False
        lstDias.Enabled = False
        For I = 0 To 6
            lstDias.AddItem WeekdayName(I + 1, , vbMonday)
            lstDias.Selected(I) = True
        Next I
        lstDias.ListIndex = -1
        chkDia.Value = 1
        HabilitarHoras False
    End If
    
    EstadoToolbar estbBusqueda
    ActivarControles True
    
End Sub

Private Sub tb_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case LCase(Button.Key)
        Case "importar"
            Form_KeyDown vbKeyF2, 0
        Case "editar"
            Form_KeyDown vbKeyF5, 0
        Case "add"
            Form_KeyDown vbKeyF3, 0
        Case "eliminar"
            Form_KeyDown vbKeyF6, 0
        Case "cancelar"
            Form_KeyDown vbKeyF7, 0
        Case "guardar"
            Form_KeyDown vbKeyF4, 0
        Case "botones"
            Form_KeyDown vbKeyF10, 0
        Case "salir"
            Form_KeyDown vbKeyF12, 0
        Case "teclado"
            TecladoWindows
    End Select
    
End Sub

Private Sub Guardar()
    
    If ValidarDatos() Then
                
        If fCls.PerfilSel Is Nothing Then
            Set fCls.PerfilSel = New clsPerfil
        End If
        
        fCls.PerfilSel.Descripcion = Me.txtDescripcion.Text
        
        fCls.PerfilSel.HoraIni = Me.dtHini.Value
        fCls.PerfilSel.HoraFin = Me.dthFin.Value
        fCls.PerfilSel.Dias = BuscarDias
        
        If fCls.GrabarPerfil Then
            ReplicarOtrasCajas
            Unload Me
        End If
        
    End If
    
End Sub

Private Function ValidarDatos() As Boolean
    
    If Trim(txtDescripcion.Text) = "" Then
        fCls.Mensaje "Debe introducir la Descripci�n.", False
        Exit Function
    End If
    
    If fCls.PerfilSel.Botones.Count = 0 Then
        fCls.Mensaje "Debe asignar los botones a la configuraci�n.", False
        Exit Function
    End If
    
    If lstDias.SelCount = 0 Then
        fCls.Mensaje "Debe seleccionar al menos un d�a.", False
        Exit Function
    End If
    
    If chkDia.Value = 0 Then
        If dtHini.Value > dthFin.Value Then
            fCls.Mensaje "La hora de finalizaci�n debe ser posterior a la hora de inicio.", False
            Exit Function
        End If
    End If
    
    ValidarDatos = True
    
End Function

Private Sub HabilitarHoras(Activar As Boolean)
    If Not Activar Then
        dtHini.Value = TimeSerial(0, 0, 0)
        dthFin.Value = TimeSerial(23, 59, 59)
    End If
    ' Mientras no exista a�n la funcionalidad acorde en el FOOD. Deshabilitar uso de campos.
    dtHini.Enabled = Activar And False
    dthFin.Enabled = Activar And False
End Sub

Private Function BuscarDias() As Integer
    Dim mDias As Integer
    For I = 0 To lstDias.ListCount - 1
        If lstDias.Selected(I) Then
            mDias = mDias + (2 ^ I)
        End If
    Next I
    BuscarDias = mDias
End Function

Private Sub EstadoToolbar(Estado As eEstadoToolbar)
    
'    tb.Buttons("add").Enabled = Estado = estbStandard
'    tb.Buttons("importar").Enabled = Estado = estbStandard
'    'tb.Buttons("cancel").Enabled = Estado <> estbStandard
'    tb.Buttons("editar").Enabled = Estado = estbBusqueda
'    tb.Buttons("eliminar").Enabled = Estado = estbBusqueda
'    tb.Buttons("guardar").Enabled = Estado = estbEdicion
'    tb.Buttons("botones").Enabled = Estado = estbEdicion
    
End Sub

Private Function BotonActivo(KeyBtn As String) As Boolean
    BotonActivo = tb.Buttons(KeyBtn).Enabled
End Function

Private Sub Nuevo()
    fCls.IniciarPerfil
    IniciarForm
    EstadoToolbar estbEdicion
    ActivarControles True
    txtDescripcion.SetFocus
End Sub

Private Sub Buscar()
    
    Dim mBus As clsBusqueda
    
    Set mBus = New clsBusqueda
    Set mBus.ConexionBusqueda = fCls.Conexion.Conexion
    
    mBus.AgregarCamposBusqueda "Codigo", "codigoperfil", "codigoperfil", 1200, True, opCampoStr
    mBus.AgregarCamposBusqueda "Descripcion", "Descripcion", "Descripcion", 4000, True, opCampoStr, True
    mBus.MostrarInterfazBusqueda "P E R F I L E S", True, False, , , "%"
    
    If Not IsEmpty(mBus.ResultadoBusqueda) Then
        fCls.BuscarPerfil CStr(mBus.ResultadoBusqueda(0)(0))
        If Not fCls.PerfilSel Is Nothing Then
            CargarForm
        End If
    End If
    
End Sub

Private Sub Cancelar()
    fCls.ResetPerfil
    IniciarForm
    ActivarControles False
End Sub

Private Sub Editar()
    EstadoToolbar estbEdicion
    ActivarControles True
End Sub

Private Sub Eliminar()
    
    ' Anteriormente Eliminar el seleccionado
    
    'If fCls.ValidarEliminar(fCls.PerfilSel.Perfil) Then
        'If fCls.EliminarPerfil(fCls.PerfilSel.Perfil) Then
            'fCls.Mensaje "Perfil Eliminado", False
            'Cancelar
        'End If
    'End If
    
    ' Ahora no se toma en cuenta el perfil seleccionado.
    ' Se permitiran eliminar varios perfiles.
    
    Dim mBus As clsBusqueda
    Dim mCaja As clsCaja
    Dim mSQl As String, mWhere As String
    
    Set mBus = New clsBusqueda
    Set mBus.ConexionBusqueda = fCls.Conexion.Conexion
    
    mBus.AgregarCamposBusqueda "C�digo", "CodigoPerfil", "CodigoPerfil", 3030, True, opCampoStr, True
    mBus.AgregarCamposBusqueda "Descripci�n", "Descripcion", "Descripcion", 8070, True, opCampoStr, True
    mBus.TextoComandoSql = "SELECT DISTINCT CodigoPerfil, Descripcion FROM MA_CAJA_BOTONES_DIGITALES_PERFIL"
    mBus.OrdenarPor = "CodigoPerfil ASC"
    mBus.MostrarInterfazBusqueda "P E R F I L E S", True, True, , , "%"
                
    If Not IsEmpty(mBus.ResultadoBusqueda) Then
        
        Respuesta = fCls.Mensaje("�Est� seguro de eliminar los perfiles seleccionados?", True)
        
        If Respuesta Then
            
            For I = 0 To UBound(mBus.ResultadoBusqueda)
                
                If fCls.ValidarEliminar(mBus.ResultadoBusqueda(I)(0)) Then
                    If Not fCls.EliminarPerfil(mBus.ResultadoBusqueda(I)(0)) Then
                        fCls.Mensaje "El Perfil [" & mBus.ResultadoBusqueda(I)(0) & "][" & mBus.ResultadoBusqueda(I)(1) & "] no pudo ser eliminado."
                    End If
                End If
                
            Next I
            
            fCls.Mensaje "Proceso Culminado."
            
        End If
        
    End If
            
    
End Sub

Private Sub ConfigurarBotones()
    fCls.ConfigurarBotonesCaja
End Sub

Private Sub ActivarControles(Activar As Boolean)
    txtDescripcion.Enabled = Activar
    chkDia.Enabled = Activar And fCls.CamposNuevos
    lstDias.Enabled = Activar And fCls.CamposNuevos
    dthFin.Enabled = Not chkDia.Enabled And fCls.CamposNuevos
    dtHini.Enabled = Not chkDia.Enabled And fCls.CamposNuevos
End Sub

Private Sub ReplicarOtrasCajas()
    
    Dim mBus As clsBusqueda
    Dim mCaja As clsCaja
    Dim mSQl As String, mWhere As String
    
    'If ExistenOtrasCajasSinPerfil(fCls.PerfilSel.Perfil) Then
    ' No vale la pena condicionar.... es mejor siempre dar la opci�n...
        
        If fCls.Mensaje("�Desea aplicar el perfil a otras cajas?", True) Then
        
            Set mBus = New clsBusqueda
            'Set mBus.ConexionBusqueda = fCls.Conexion.Conexion
            'mBus.AgregarCamposBusqueda "C�digo", "c_Codigo", "c_Codigo", 5000, True, opCampoStr, True
            'mBus.TextoComandoSql = "Select C.c_Codigo from MA_CAJA C LEFT JOIN MA_CAJA_BOTONES_DIGITALES B ON C.C_Codigo = B.CAJA WHERE B.CODIGOPERFIL <> '" & fCls.PerfilSel.Perfil & "' OR B.CODIGOPERFIL IS NULL"
            'mBus.MostrarInterfazBusqueda "C A J A S", True, True
            
            Set FrmSeleccionarItems.fCls = Me.fCls
            FrmSeleccionarItems.Show vbModal
            
            mBus.ResultadoBusqueda = FrmSeleccionarItems.ResultadoBusqueda
            Set FrmSeleccionarItems = Nothing
                        
            If Not IsEmpty(mBus.ResultadoBusqueda) Then
                For I = 0 To UBound(mBus.ResultadoBusqueda)
                    fCls.CopiarPerfilCaja CStr(mBus.ResultadoBusqueda(I)(0)), fCls.PerfilSel.Perfil
                Next I
            End If
            
        End If
        
    'End If
    
End Sub

Private Function ExistenOtrasCajasSinPerfil(Perfil As String)
    
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    
    If fCls.Conexion.ConectarBD Then
        mSQl = "Select C.C_Codigo from MA_CAJA C LEFT JOIN MA_CAJA_BOTONES_DIGITALES B ON C.C_Codigo = B.CAJA WHERE CODIGOPERFIL <> '" & Perfil & "' OR CODIGOPERFIL IS NULL"
        Set mRs = New ADODB.Recordset
        mRs.Open mSQl, fCls.Conexion.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
        ExistenOtrasCajasSinPerfil = Not mRs.EOF
    End If
    
End Function

Private Sub txtDescripcion_Click()
    If ModoTouch Then TecladoWindows
End Sub

Private Sub txtDescripcion_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyReturn
            If fCls.CamposNuevos Then
                
            Else
                If tb.Buttons("botones").Enabled And fCls.PerfilSel.Botones.Count = 0 Then
                    Form_KeyDown vbKeyF10, 0
                ElseIf tb.Buttons("botones").Enabled And fCls.PerfilSel.Botones.Count > 0 Then
                    Form_KeyDown vbKeyF4, 0
                End If
            End If
    End Select
End Sub
