VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmProductosLite 
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   8070
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   15300
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8070
   ScaleWidth      =   15300
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame f_general 
      Appearance      =   0  'Flat
      Caption         =   "Datos Generales"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   6405
      Left            =   0
      TabIndex        =   6
      Top             =   1560
      Width           =   15045
      Begin VB.TextBox BarCode 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   255
         MaxLength       =   40
         TabIndex        =   60
         Top             =   1350
         Width           =   2415
      End
      Begin VB.ComboBox Tipo_Producto 
         BackColor       =   &H00FFFFFF&
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "frmProductosLite.frx":0000
         Left            =   6765
         List            =   "frmProductosLite.frx":0016
         Style           =   2  'Dropdown List
         TabIndex        =   59
         Top             =   1350
         Width           =   2490
      End
      Begin VB.CheckBox Activo 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Activo"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   9315
         TabIndex        =   58
         TabStop         =   0   'False
         Top             =   1380
         Value           =   1  'Checked
         Width           =   930
      End
      Begin VB.TextBox Presentacion 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4770
         MaxLength       =   100
         TabIndex        =   57
         Top             =   1350
         Width           =   1935
      End
      Begin VB.TextBox Marca 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2760
         MaxLength       =   40
         TabIndex        =   56
         Top             =   1350
         Width           =   1935
      End
      Begin VB.TextBox Codigo 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   240
         MaxLength       =   15
         TabIndex        =   55
         Top             =   615
         Width           =   1770
      End
      Begin VB.TextBox Descripcion 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2070
         MaxLength       =   255
         TabIndex        =   54
         Top             =   615
         Width           =   8085
      End
      Begin VB.Frame FrmDepGruSub 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   1575
         Left            =   120
         TabIndex        =   35
         Top             =   2040
         Width           =   10110
         Begin VB.TextBox Grupo 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   1890
            MaxLength       =   5
            TabIndex        =   41
            Top             =   660
            Width           =   1305
         End
         Begin VB.TextBox SubGrupo 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   1890
            MaxLength       =   6
            TabIndex        =   40
            Top             =   1080
            Width           =   1305
         End
         Begin VB.CommandButton Btn_Dpto 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Times New Roman"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   350
            Left            =   3300
            Picture         =   "frmProductosLite.frx":005F
            Style           =   1  'Graphical
            TabIndex        =   39
            Top             =   255
            Width           =   435
         End
         Begin VB.CommandButton Btn_Grupo 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Times New Roman"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   350
            Left            =   3315
            Picture         =   "frmProductosLite.frx":0861
            Style           =   1  'Graphical
            TabIndex        =   38
            Top             =   660
            Width           =   435
         End
         Begin VB.TextBox Departamento 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   1890
            MaxLength       =   5
            TabIndex        =   37
            Top             =   240
            Width           =   1305
         End
         Begin VB.CommandButton Btn_SubGrupo 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Times New Roman"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   350
            Left            =   3315
            Picture         =   "frmProductosLite.frx":1063
            Style           =   1  'Graphical
            TabIndex        =   36
            Top             =   1080
            Width           =   435
         End
         Begin VB.Label Lbl_Departamento 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   3930
            TabIndex        =   53
            Top             =   240
            Width           =   6060
         End
         Begin VB.Label Lbl_Grupo 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   3930
            TabIndex        =   52
            Top             =   660
            Width           =   6060
         End
         Begin VB.Label Lbl_SubGrupo 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   3930
            TabIndex        =   51
            Top             =   1080
            Width           =   6060
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Sub Grupo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Index           =   4
            Left            =   135
            TabIndex        =   50
            Top             =   1125
            Width           =   1215
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Departamento"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   240
            Index           =   2
            Left            =   120
            TabIndex        =   49
            Top             =   255
            Width           =   1215
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Grupo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   3
            Left            =   135
            TabIndex        =   48
            Top             =   675
            Width           =   1335
         End
         Begin VB.Label Lbl_Grupo_por 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   8670
            TabIndex        =   47
            Top             =   4740
            Width           =   900
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Index           =   47
            Left            =   9630
            TabIndex        =   46
            Top             =   5205
            Width           =   225
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Index           =   48
            Left            =   9630
            TabIndex        =   45
            Top             =   4755
            Width           =   225
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Index           =   49
            Left            =   9630
            TabIndex        =   44
            Top             =   4335
            Width           =   225
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Utilidad"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   57
            Left            =   8760
            TabIndex        =   43
            Top             =   4020
            Width           =   630
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "*"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   240
            Index           =   60
            Left            =   1350
            TabIndex        =   42
            Top             =   255
            Width           =   120
         End
      End
      Begin VB.Frame Frame5 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Caption         =   "Imagen"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   3930
         Left            =   10440
         TabIndex        =   31
         Top             =   240
         Width           =   4455
         Begin VB.Label C�digo 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Click Derecho para eliminar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C0C0C0&
            Height          =   240
            Index           =   74
            Left            =   1155
            TabIndex        =   34
            Top             =   2280
            Width           =   2370
         End
         Begin VB.Label C�digo 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Doble Click para cambiar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C0C0C0&
            Height          =   240
            Index           =   63
            Left            =   1155
            TabIndex        =   33
            Top             =   1920
            Width           =   2130
         End
         Begin VB.Label C�digo 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Imagen"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   15.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C0C0C0&
            Height          =   375
            Index           =   62
            Left            =   1650
            TabIndex        =   32
            Top             =   1440
            Width           =   1080
         End
         Begin VB.Image Imagen 
            Appearance      =   0  'Flat
            Height          =   3795
            Left            =   75
            Stretch         =   -1  'True
            Top             =   75
            Width           =   4275
         End
      End
      Begin VB.Frame f_precios 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Caption         =   "Precios"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   2580
         Left            =   120
         TabIndex        =   7
         Top             =   3720
         Width           =   10050
         Begin VB.ComboBox cboDecimales 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   8910
            Style           =   2  'Dropdown List
            TabIndex        =   76
            Top             =   450
            Width           =   900
         End
         Begin VB.TextBox Existencia 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#.##0,00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   8202
               SubFormatType   =   1
            EndProperty
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   350
            Left            =   7680
            MaxLength       =   18
            TabIndex        =   13
            Top             =   480
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton Cod_Alt 
            Appearance      =   0  'Flat
            Caption         =   "&C�digos Alternos"
            CausesValidation=   0   'False
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   945
            Left            =   8280
            Picture         =   "frmProductosLite.frx":1865
            Style           =   1  'Graphical
            TabIndex        =   12
            ToolTipText     =   "C�digos para este Producto"
            Top             =   1440
            Width           =   1620
         End
         Begin VB.TextBox CActual 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#.##0,00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   8202
               SubFormatType   =   1
            EndProperty
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   350
            Left            =   6375
            MaxLength       =   18
            TabIndex        =   11
            Top             =   480
            Width           =   1005
         End
         Begin VB.ComboBox Imp1 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   4950
            Style           =   2  'Dropdown List
            TabIndex        =   10
            Top             =   480
            Width           =   900
         End
         Begin VB.TextBox Precio1 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#.##0,00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   8202
               SubFormatType   =   0
            EndProperty
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   1755
            MaxLength       =   18
            TabIndex        =   9
            Top             =   480
            Width           =   1725
         End
         Begin VB.TextBox PrecioO 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "#.##0,00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   8202
               SubFormatType   =   0
            EndProperty
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   1755
            Locked          =   -1  'True
            MaxLength       =   18
            TabIndex        =   8
            Top             =   975
            Width           =   1725
         End
         Begin MSComCtl2.DTPicker OfertaDesde 
            Height          =   345
            Left            =   795
            TabIndex        =   14
            Top             =   1905
            Width           =   2685
            _ExtentX        =   4736
            _ExtentY        =   609
            _Version        =   393216
            Enabled         =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CalendarTitleBackColor=   -2147483624
            CalendarTrailingForeColor=   -2147483624
            CustomFormat    =   "dd/MM/yyyy hh:mm:ss tt"
            Format          =   102039555
            CurrentDate     =   38106
         End
         Begin MSComCtl2.DTPicker OfertaHasta 
            Height          =   345
            Left            =   4275
            TabIndex        =   15
            Top             =   1905
            Width           =   2685
            _ExtentX        =   4736
            _ExtentY        =   609
            _Version        =   393216
            Enabled         =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CustomFormat    =   "dd/MM/yyyy hh:mm:ss tt"
            Format          =   102039555
            CurrentDate     =   38106
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Decimales"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   1
            Left            =   8880
            TabIndex        =   77
            Top             =   120
            Width           =   870
         End
         Begin VB.Label C�digo 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Cantidad"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   73
            Left            =   7575
            TabIndex        =   30
            Top             =   150
            Visible         =   0   'False
            Width           =   1200
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Costo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   40
            Left            =   6630
            TabIndex        =   29
            Top             =   150
            Width           =   480
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "% "
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Index           =   7
            Left            =   5925
            TabIndex        =   28
            Top             =   450
            Width           =   300
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Impuestos"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   55
            Left            =   4920
            TabIndex        =   27
            Top             =   150
            Width           =   885
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Utilidad"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   52
            Left            =   3720
            TabIndex        =   26
            Top             =   150
            Width           =   630
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Precio"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   51
            Left            =   2280
            TabIndex        =   25
            Top             =   165
            Width           =   525
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Vigencia de la Oferta"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   50
            Left            =   120
            TabIndex        =   24
            Top             =   1575
            Width           =   1800
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Hasta"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Index           =   35
            Left            =   3585
            TabIndex        =   23
            Top             =   1980
            Width           =   660
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Desde"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Index           =   34
            Left            =   105
            TabIndex        =   22
            Top             =   1980
            Width           =   660
         End
         Begin VB.Label Margen1 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   3540
            TabIndex        =   21
            Top             =   480
            Width           =   900
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Precio de Venta 1"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   23
            Left            =   105
            TabIndex        =   20
            Top             =   555
            Width           =   1515
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Index           =   16
            Left            =   4560
            TabIndex        =   19
            Top             =   480
            Width           =   225
         End
         Begin VB.Label MargenO 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   3540
            TabIndex        =   18
            Top             =   975
            Width           =   900
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Precio de Oferta"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   17
            Left            =   105
            TabIndex        =   17
            Top             =   1035
            Width           =   1395
         End
         Begin VB.Label C�digo 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   270
            Index           =   14
            Left            =   4560
            TabIndex        =   16
            Top             =   975
            Width           =   225
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H00FFFFFF&
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00E0E0E0&
            FillStyle       =   0  'Solid
            Height          =   1050
            Index           =   3
            Left            =   0
            Top             =   1440
            Width           =   7335
         End
      End
      Begin MSComCtl2.DTPicker Actualizado 
         Height          =   330
         Left            =   12750
         TabIndex        =   61
         Top             =   4770
         Width           =   2025
         _ExtentX        =   3572
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CalendarTitleBackColor=   -2147483624
         CalendarTrailingForeColor=   -2147483624
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   102039555
         CurrentDate     =   38106
      End
      Begin MSComCtl2.DTPicker Ingresado 
         Height          =   330
         Left            =   10440
         TabIndex        =   62
         Top             =   4770
         Width           =   2025
         _ExtentX        =   3572
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CalendarTitleBackColor=   -2147483624
         CalendarTrailingForeColor=   -2147483624
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   102039555
         CurrentDate     =   38106
      End
      Begin VB.Label C�digo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo de Barra"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   72
         Left            =   255
         TabIndex        =   75
         Top             =   1095
         Width           =   1380
      End
      Begin VB.Label C�digo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "*"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   240
         Index           =   59
         Left            =   3180
         TabIndex        =   74
         Top             =   390
         Width           =   120
      End
      Begin VB.Label C�digo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "*"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   240
         Index           =   58
         Left            =   855
         TabIndex        =   73
         Top             =   390
         Width           =   120
      End
      Begin VB.Label C�digo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "*"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   240
         Index           =   56
         Left            =   8685
         TabIndex        =   72
         Top             =   1095
         Width           =   120
      End
      Begin VB.Label C�digo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   240
         Index           =   6
         Left            =   2070
         TabIndex        =   71
         Top             =   360
         Width           =   975
      End
      Begin VB.Label C�digo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Status"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   32
         Left            =   9315
         TabIndex        =   70
         Top             =   1095
         Width           =   540
      End
      Begin VB.Label C�digo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Tipo de Producto"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   28
         Left            =   6765
         TabIndex        =   69
         Top             =   1095
         Width           =   1455
      End
      Begin VB.Label C�digo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Presentacion"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   13
         Left            =   4770
         TabIndex        =   68
         Top             =   1095
         Width           =   1095
      End
      Begin VB.Label C�digo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Marca"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   5
         Left            =   2760
         TabIndex        =   67
         Top             =   1095
         Width           =   525
      End
      Begin VB.Label C�digo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   240
         Index           =   0
         Left            =   240
         TabIndex        =   66
         Top             =   360
         Width           =   585
      End
      Begin VB.Label C�digo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "* Datos Obligatorios"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   240
         Index           =   61
         Left            =   10440
         TabIndex        =   65
         Top             =   4200
         Width           =   1740
      End
      Begin VB.Label C�digo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Fecha de Creaci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Index           =   36
         Left            =   10440
         TabIndex        =   64
         Top             =   4530
         Width           =   1665
      End
      Begin VB.Label C�digo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "�ltima Actualizaci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Index           =   37
         Left            =   12750
         TabIndex        =   63
         Top             =   4530
         Width           =   1710
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00FFFFFF&
         BorderColor     =   &H00000000&
         BorderStyle     =   0  'Transparent
         FillColor       =   &H00E7E8E8&
         FillStyle       =   0  'Solid
         Height          =   1635
         Index           =   0
         Left            =   120
         Top             =   240
         Width           =   10110
      End
   End
   Begin VB.PictureBox CoolBar1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   0
      ScaleHeight     =   1050
      ScaleWidth      =   15330
      TabIndex        =   3
      Top             =   420
      Width           =   15360
      Begin VB.Frame frame_toolbar 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   810
         Left            =   30
         TabIndex        =   4
         Top             =   140
         Width           =   15240
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   810
            Left            =   120
            TabIndex        =   5
            Top             =   0
            Width           =   10575
            _ExtentX        =   18653
            _ExtentY        =   1429
            ButtonWidth     =   2408
            ButtonHeight    =   1429
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "Iconos_Encendidos"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   10
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "(F3) Agr&egar"
                  Key             =   "Agregar"
                  Object.ToolTipText     =   "Agregar una Nueva Ficha"
                  ImageIndex      =   2
                  Style           =   5
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   3
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "APRODUCTO"
                        Object.Tag             =   "APRODUCTO"
                        Text            =   "Agregar"
                     EndProperty
                     BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "AIMPORTAR"
                        Object.Tag             =   "AIMPORTAR"
                        Text            =   "Agregar Importar "
                     EndProperty
                     BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "imparchivo"
                        Text            =   "Importar Archivo"
                     EndProperty
                  EndProperty
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "(F2) &Buscar"
                  Key             =   "Buscar"
                  Object.ToolTipText     =   "Buscar una Ficha"
                  ImageIndex      =   6
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "(F5) &Modificar"
                  Key             =   "Modificar"
                  Object.ToolTipText     =   "Modificar esta Ficha"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "(F7) &Cancelar"
                  Key             =   "Cancelar"
                  Object.ToolTipText     =   "Cancelar esta Ficha"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "(F6) Bo&rrar"
                  Key             =   "Eliminar"
                  Object.ToolTipText     =   "Borrar esta Ficha"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "(F4) &Grabar"
                  Key             =   "Grabar"
                  Object.ToolTipText     =   "Grabar esta Ficha"
                  ImageIndex      =   7
               EndProperty
               BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "(F12) &Salir"
                  Key             =   "Salir"
                  Object.ToolTipText     =   "Salir de Ficheros"
                  ImageIndex      =   5
               EndProperty
               BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Object.Visible         =   0   'False
                  Caption         =   "&Ayuda"
                  Key             =   "Ayuda"
                  Object.ToolTipText     =   "Ayuda"
               EndProperty
            EndProperty
         End
      End
      Begin MSComDlg.CommonDialog caja 
         Left            =   14700
         Top             =   45
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
   End
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   13150
         TabIndex        =   1
         Top             =   75
         Width           =   1815
      End
   End
   Begin MSComDlg.CommonDialog dialogo 
      Left            =   14880
      Top             =   1320
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      DialogTitle     =   "Buscar Imagenes"
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   14310
      Top             =   1200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmProductosLite.frx":35E7
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmProductosLite.frx":5379
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmProductosLite.frx":6053
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmProductosLite.frx":6D2D
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmProductosLite.frx":8ABF
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmProductosLite.frx":A851
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmProductosLite.frx":C5E3
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmProductosLite"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public fCls As clsProductoUI

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
            Case Is = vbKeyReturn
                SendKeys Chr(vbKeyTab)
            
            Case Is = vbKeyF2
                If Not Toolbar1.Buttons(2).Enabled Then Exit Sub
                Call BuscarProducto
               
                
            Case Is = vbKeyF3
                If Not Toolbar1.Buttons(1).Enabled Then Exit Sub
                Call agregar_registro
                    
                
            
            Case Is = vbKeyF4
                If Not Toolbar1.Buttons(7).Enabled Then Exit Sub
                Call grabar_registro
                
            
            Case Is = vbKeyF5
                If Not Toolbar1.Buttons(3).Enabled Then Exit Sub
                Call modificar_registro
                    
            
            Case Is = vbKeyF6
                If Not Toolbar1.Buttons(6).Enabled Then Exit Sub
                Call eliminar_registro
                
            
            Case Is = vbKeyF7
                If Not Toolbar1.Buttons(5).Enabled Then Exit Sub
                Call cancelar_registro
            
            Case Is = vbKeyF12
                Unload Me
        End Select
    End If
End Sub

Private Sub Form_Load()
    fCls.CargarComboImpuesto Me.Imp1
    For i = 0 To 5
        cboDecimales.AddItem i
    Next
    cboDecimales.ListIndex = 0
    Cancelar
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    
    Select Case Button.Key
        Case Is = "Agregar"
            Form_KeyDown vbKeyF3, 0
        
        Case Is = "Buscar"
            Form_KeyDown vbKeyF2, 0
        
        Case Is = "Modificar"
            Form_KeyDown vbKeyF5, 0
        
        Case Is = "Cancelar"
            Form_KeyDown vbKeyF7, 0
        
        Case Is = "Eliminar"
            Form_KeyDown vbKeyF6, 0
        
        Case Is = "Grabar"
            Form_KeyDown vbKeyF4, 0
        
        Case Is = "Salir"
            Call Form_KeyDown(vbKeyF12, 1)
            
        
        
    End Select
End Sub





'RUTINAS

Private Sub agregar_registro()
    cancelar_registro
    modificar_registro
    If fCls.AutoCodigo Then
        Me.Codigo.Text = fCls.ClsCn.Correlativo("cod_producto", False)
        Codigo.Locked = True
    Else
        Codigo.Locked = False
    End If
    
End Sub

Private Sub BuscarProducto()
    
    If fCls.BuscarProducto() Then
        Cargar_Campos
        EstatusToolbar 1
    End If
    
    
End Sub

Private Sub modificar_registro()
    EstatusToolbar 2
    f_general.Enabled = True
    
    
End Sub

Private Sub cancelar_registro()
    EstatusToolbar 0
    f_general.Enabled = False
    LimpiarDatos
    
End Sub



Private Sub eliminar_registro()

    If fCls.EliminarProducto Then
        cancelar_registro
    End If

    Call Apertura_Recordset(RsProductos)
    
    RsProductos.Open "select * from ma_productos where c_codigo ='" & Codigo.Text & "'", _
    Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    If Not RsProductos.EOF Then
        depoprod = False
        
        Call Apertura_Recordset(RsTrCompras)
        
        RsTrCompras.Open "select c_codarticulo from ma_depoprod where c_codarticulo = '" & Codigo.Text & "'", _
        Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not RsTrCompras.EOF Then
            depoprod = True
        End If
        
        'transaccion = False
        
        If depoprod = True Or transaccion = True Then
            Call Mensaje(True, Stellar_Mensaje(16196)) '"Existen transacciones en el sistema que no permiten la Eliminaci�n de este producto.")
            
            RsTrCompras.Close
            RsProductos.Close
            
            Set RsTrCompras = Nothing
            Set rstrproductos = Nothing
            
            Exit Sub
        Else
            Ent.BDD.BeginTrans
                RsProductos.Delete
                RsProductos.UpdateBatch
                
                Call Apertura_Recordset(rscodigos)
                
                mPasarTrpen = ManejaPOS(Ent.BDD) Or ManejaSucursales(Ent.BDD)
                
'                If Not mPasarTrpen Then
'                    rscodigos.Open "select * FROM MA_codigos where c_codnasa = '" & Codigo & "'", Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
'                    OperacionRs "TR_PENDIENTE_CODIGO", rscodigos, Ent.BDD
'                Else

                rscodigos.Open "select * FROM MA_codigos where c_codnasa = '" & Codigo & "'", _
                Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
                
'                End If

                Call Apertura_Recordset(rsProdxProv)
                
'                If Not mPasarTrpen Then
'                    rscodigos.Open "select * from MA_prodxprov where c_codigo = '" & Codigo & "'", Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
'                    OperacionRs "TR_PENDIENTE_CODIGO", rscodigos, Ent.BDD
'                Else
                    rsProdxProv.Open "DELETE from MA_prodxprov where c_codigo = '" & Codigo & "'", _
                    Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
'                End If
                
                '***************************************
                '**** TRANSACCIONES PENDIENTES
                '***************************************
                
                'Ent.rsReglasComerciales.Requery
                'If Ent.rsReglasComerciales!MANEJA_POS = True Then
                
                If mPasarTrpen Then
                
                    'PENDIENTE DE PRODUCTOS
                    
                    Call Apertura_Recordset(rsEureka)
                    
                    rsEureka.Open "SELECT * FROM tr_pendiente_prod WHERE C_CODIGO = '" & Codigo & "' AND TIPOCAMBIO <> 0", _
                    Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
                    
                    If Not rsEureka.EOF Then
                        rsEureka.Update
                    Else
                        rsEureka.AddNew
                        rsEureka!c_codigo = Codigo
                    End If
                    
                    rsEureka!TipoCambio = 1
                    
                    Call Update_Datos(rsEureka)
                    
                    rsEureka!HABLADOR = 0
                    rsEureka.UpdateBatch
                    
                    'PENDIENTE DE CODIGOS
                    
                    If Not rscodigos.EOF Then
                        While Not rscodigos.EOF
                            Call Apertura_Recordset(rsEureka)
                            
                            rsEureka.Open "SELECT * FROM tr_pendiente_codigo WHERE C_CODIGO = '" & rscodigos!c_codigo & "' AND TIPOCAMBIO <> 0", _
                            Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
'                            If Not rseureka.EOF Then
'                                rseureka.Update
'                            Else
                                rsEureka.AddNew
                                rsEureka!c_codigo = rscodigos!c_codigo
'                            End If
                            rsEureka!c_codnasa = rscodigos!c_codnasa
                            rsEureka!c_descripcion = rscodigos!c_descripcion
                            rsEureka!n_cantidad = rscodigos!n_cantidad
                            rsEureka!TipoCambio = 1
                            rsEureka.UpdateBatch
'                            rscodigos.delete
'                            rscodigos.UpdateBatch
                            rscodigos.MoveNext
                        Wend
                        
                        rscodigos.MoveFirst
                    End If
                End If
                
                '12/07/2004
                
                If Not rscodigos.EOF Then
                    While Not rscodigos.EOF
                        rscodigos.Delete
                        rscodigos.UpdateBatch
                        rscodigos.MoveNext
                    Wend
                End If
                
            Ent.BDD.CommitTrans
        End If
    
        Call Cerrar_Recordset(rsEureka)
        Call Cerrar_Recordset(rscodigos)
        Call Cerrar_Recordset(rsProdxProv)
        Call Cerrar_Recordset(RsTrCompras)
        
        Call Limpiar_Datos
    End If
    
    Screen.MousePointer = 0
    
End Sub

Private Sub grabar_registro()
    If ValidarDatos Then
        If fCls.grabarproducto Then
            cancelar_registro
        End If
    End If
    
    

    Dim RsDepoProd As New ADODB.Recordset

'On Error Resume Next
'    Ent.BDD.CommitTrans
'    Ent.BDD.BeginTrans
'    Err.Clear
'On Error GoTo 0

    '*******************************************************************
    'VERIFICACI�N QUE EL CODIGO NO ESTE VAC�O
    '*******************************************************************
    
        If Codigo.Text = "" Then
            Call Mensaje(True, Stellar_Mensaje(16197)) '"El C�digo del producto es obligatorio.")
            Codigo.SetFocus
            Exit Sub
        End If
    
    '*******************************************************************
    'VERIFICACI�N QUE LA DESCRIPCI�N NO ESTE VAC�A
    '*******************************************************************
    
        If Descripcion.Text = "" Then
            Call Mensaje(True, Stellar_Mensaje(16198)) '"La Descripci�n del producto es obligatoria.")
            Descripcion.SetFocus
            Exit Sub
        End If
    
    '***************************************************
    'VERIFICACI�N DE ASIGNACI�N DE DEPARTAMENTO
    '***************************************************
    
    If Me.FrmDepGruSub.Visible Then
        If Departamento.Text = "" And (Me.Tipo_Producto.ListIndex <> intTipoCarExt) Then '(Me.Tipo_Producto <> "Carac. Exte")
            Call Mensaje(True, Stellar_Mensaje(16199)) '"Primero debe seleccionar el departamento al cual pertenece el producto.")
            Departamento.SetFocus
            Exit Sub
        End If
    End If
    
    '***************************************************'***************************************************
    'VERIFICACI�N DE ASIGNACI�N DE LA PLANTILLA PRODUCTOS EXTENDIDOS
    '***************************************************'***************************************************
    
    If Not VerificarTxtNumericos Then Exit Sub
        
     Select Case Tipo_Producto.ListIndex
        Case intTipoCarExt
            If Len(Trim(Me.txtCodCarExt.Text)) = 0 Then
                 Call Mensaje(True, Stellar_Mensaje(16200)) '"Los Productos con Caracteristicas Extendidas deben tener una plantilla asociada.")
                 Command1.SetFocus
                 Exit Sub
            End If
        Case IntTipoPesado, IntTipoPesable
            If Me.Decimales = "" Or Val(Me.Decimales) = 0 Then
                Call Mensaje(True, Stellar_Mensaje(16201)) '"Los productos Pesados y Pesables deben tener una cantidad de Decimales diferente de cero.")
                Sstab1.Tab = 1
                If grutinas.PuedeObtenerFoco(Decimales) Then Decimales.SetFocus
                Exit Sub
            End If
     End Select

    '*********************************
    'COMIENZA TRANSACCION
    '*********************************
    
    'On Error GoTo ERROR_GRABAR_PRODUCTO
    
    If Ent.BDD.State = adStateOpen Then
        Ent.BDD.BeginTrans
    End If
    
        '*********************************
        'SI ES PRODUCTO NUEVO
        '*********************************
        If mGrabar = "insert" Then
            
            Call Apertura_Recordset(RsProductos)
            RsProductos.Open "SELECT * FROM MA_PRODUCTOS WHERE 1 = 2", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            '********************************************************************
            '* VERIFICA SI EXISTE AUTOCODIFICACI�N PARA ASUMIR EL CODIGO NUEVO
            '*******************************************************************
            
            If AutoCodificacion Then
                Codigo = Format(NO_CONSECUTIVO("Cod_Producto"), Maskara)
            End If
            
            '************************************************************
            '* Agregar el C�digo Maestro a la Tabla de C�digos Alternos
            '************************************************************
            
            Call Apertura_Recordset(rscodigos)

            rscodigos.Open "SELECT * FROM MA_CODIGOS WHERE 1 = 2", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            With rscodigos
                .AddNew
                !c_codigo = Codigo
                !c_descripcion = "CODIGO MAESTRO"
                !c_codnasa = Codigo
                !n_cantidad = 1
                !nu_intercambio = 0
                .UpdateBatch
            End With
            
            If BarCode.Text <> "" Then
                With rscodigos
                    .AddNew
                    !c_codigo = BarCode.Text
                    !c_descripcion = "BARCODE"
                    !c_codnasa = Codigo
                    !n_cantidad = 1
                    !nu_intercambio = 1
                    .UpdateBatch
                End With
            End If
            
            If Existencia.Text <> "" And IsNumeric(Existencia.Text) Then
            
                Call Apertura_Recordset(RsDepoProd)
                
                RsDepoProd.Open "SELECT * FROM MA_DEPOPROD WHERE 1 = 2", _
                Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
                
                With RsDepoProd
                    .AddNew
                    !c_coddeposito = DPS_Local
                    !c_codarticulo = Codigo
                    !c_descripcion = Descripcion.Text
                    !n_cantidad = CDbl(Existencia.Text)
                    .UpdateBatch
                End With
            
            End If
            
            '********************************************************************
            '* VERIFICA SI EL PRODUCTO ES COMPUESTO PARA CREAR SUS PARTES
            '********************************************************************
            
            'If Tipo_Producto.ListIndex = IntTipoCompuesto Then
                ' ...
            'End If
            
            '*********************************************
            '* Agregar el C�digo a la Tabla de Productos
            '*********************************************
            
            RsProductos.AddNew
            RsProductos!c_codigo = Codigo
            RsProductos!add_date = Date
            RsProductos!update_date = Date
            
        '*************************************************
        ' ACTUALIZACION DE PRODUCTOS
        '*************************************************
        
        ElseIf mGrabar = "update" Then
            Call Apertura_Recordset(RsProductos)
            
            RsProductos.Open "select * from MA_PRODUCTOS where c_codigo = '" & Codigo & "'", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            RsProductos.Update
            RsProductos!update_date = Date
            
            If partes = False And Tipo_Producto.ListIndex = 2 Then
                With Ficha_productos_Compuesto.fg2
                    For i = 1 To .Rows - 1
                        .Col = 0
                        If .Text = "" Then Exit For
                        .Row = i
                        Call Apertura_Recordset(rsMonitor)
                        rsMonitor.Open "select * from ma_partes where c_codigo = 'STELLAR'", Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
                        rsMonitor.AddNew
                            rsMonitor!c_codigo = Codigo
                            .Col = 0
                                rsMonitor!c_parte = .Text
                            .Col = 2
                                rsMonitor!n_cantidad = FormatNumber(CDbl(.Text), 3)
                        rsMonitor.UpdateBatch
                    Next
                End With
            End If
        
        End If '*** fin del insert/update
        
        '***************************************
        '**** ASIGNACI�N DE VALORES
        '***************************************
        
        Dim GeneraHablador As Integer
        GeneraHablador = 1
        
        Call Update_Datos(RsProductos, (mGrabar = "insert"), GeneraHablador)
        RsProductos.UpdateBatch
        
        Set gTrazas.ObjectConexion = Ent.BDD
        gTrazas.EscribirTraza Me.Name, lccodusuario, IIf(LCase(mGrabar) = "update", "UPD", "ADD"), RsProductos!c_codigo, nProductos
        
        '******************************************************
        ' Crea las transacciones de codigos alternos pendientes
        ' para los mercados
        '******************************************************
        
        Call Apertura_Recordset(rscodigos)
        
        rscodigos.Open "select * FROM MA_CODIGOS where c_codnasa = '" & Codigo & "'", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If rscodigos.EOF Then
            rscodigos.AddNew
            rscodigos!c_codigo = Codigo
            rscodigos!c_descripcion = "CODIGO MAESTRO"
            rscodigos!c_codnasa = Codigo
            rscodigos!n_cantidad = 1
            rscodigos.UpdateBatch
        End If
        
        If ManejaPOS(Ent.BDD) Or ManejaSucursales(Ent.BDD) Then
            Call Apertura_Recordset(Rec_Monitor)
            
            Rec_Monitor.Open "SELECT * FROM TR_PENDIENTE_CODIGO WHERE 1=2", _
            Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            With Rec_Monitor
                .AddNew
                !c_codigo = rscodigos!c_codigo
                !c_descripcion = rscodigos!c_descripcion
                '!c_codnasa = rscodigos!C_CODIGO
                !c_codnasa = rscodigos!c_codnasa
                !n_cantidad = rscodigos!n_cantidad
                !nu_intercambio = rscodigos!nu_intercambio
                !TipoCambio = 0
                .UpdateBatch
            End With
        End If
        
        '***************************************
        '**** TRANSACCIONES PENDIENTES
        '***************************************
        
        If ManejaPOS(Ent.BDD) Or ManejaSucursales(Ent.BDD) Then
            Call Apertura_Recordset(rsEureka)
            
            rsEureka.Open "SELECT * FROM tr_pendiente_prod WHERE C_CODIGO = '" & Codigo & "' AND TIPOCAMBIO = 0", _
            Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
            
            rsEureka.AddNew
            
            nCampos = RsProductos.Fields.Count - 1
            
            For qw = 0 To nCampos
                If UCase(RsProductos.Fields(qw).Name) <> "ID" Then
                    ' ESTO SE DEBE ALOS VALORES POR DEFECTO QUE NO HAN SIDO ASIGNADO
                    If Not (mGrabar = "insert" And _
                    (UCase(RsProductos.Fields(qw).Name) = UCase("F_Inicial") Or _
                    UCase(RsProductos.Fields(qw).Name) = UCase("F_Final") Or _
                    UCase(RsProductos.Fields(qw).Name) = UCase("h_inicial") Or _
                    UCase(RsProductos.Fields(qw).Name) = UCase("h_final") Or _
                    UCase(RsProductos.Fields(qw).Name) = UCase("DATE1") _
                    )) Then
                        rsEureka.Fields(RsProductos.Fields(qw).Name) = RsProductos.Fields(qw)
                    End If
                End If
            Next
    
            rsEureka!HABLADOR = GeneraHablador
            rsEureka!TipoCambio = 0
            rsEureka.UpdateBatch
            
        End If
        
    'Ent.BDD.RollbackTrans
    
    Ent.BDD.CommitTrans
    
    Call Cerrar_Recordset(rsEureka)
    
    Toolbar1.Buttons(3).Enabled = True
    mGrabar = ""
    
    Call cancelar_registro
    
    Sstab1.Tab = 0
    Me.f_general.Enabled = True
    Me.Command3.Enabled = True

Exit Sub

Error_Grabar_Producto:
    
    Me.Command3.Enabled = False
    Ent.BDD.RollbackTrans
    
    Call Mensaje(True, Err.Number & " " & Err.Description)
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name & ".Grabar_registro", Err.Source)
    
    Unload Me

End Sub


Private Sub EstatusToolbar(Estado As Integer)
    '0 estandar,1 busqueda,2 edicion
     With Toolbar1
        .Buttons("Agregar").Enabled = Estado = 0
        .Buttons("Buscar").Enabled = Estado = 0
        .Buttons("Modificar").Enabled = Estado = 1
        .Buttons("Eliminar").Enabled = Estado = 1
        .Buttons("Cancelar").Enabled = Estado > 0
        .Buttons("Grabar").Enabled = Estado = 2
    End With
        
End Sub


Private Sub LimpiarDatos()
    For Each mctrl In Me.Controls
        If TypeOf mctrl Is TextBox Then
            mctrl.Text = ""
        ElseIf TypeOf mctrl Is ComboBox Then
            ctrl.ListIndex = 0
        End If
    Next
    OfertaDesde.Value = "01/01/1980"
    OfertaHasta.Value = "01/01/1980"
    Imagen.Picture = LoadPicture()
    Me.Lbl_Departamento.Caption = ""
    Me.Lbl_Grupo.Caption = ""
    Me.Lbl_SubGrupo.Caption = ""
End Sub
