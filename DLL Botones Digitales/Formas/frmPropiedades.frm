VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "ComDlg32.ocx"
Begin VB.Form frmPropiedades 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8505
   ClientLeft      =   8685
   ClientTop       =   1830
   ClientWidth     =   8295
   ControlBox      =   0   'False
   Icon            =   "frmPropiedades.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8505
   ScaleWidth      =   8295
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame FrameTitulo 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   500
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   13680
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Propiedades del Bot�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   100
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   8715
         TabIndex        =   7
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   14505
      TabIndex        =   4
      Top             =   421
      Width           =   14535
      Begin MSComctlLib.Toolbar tb 
         Height          =   810
         Left            =   240
         TabIndex        =   5
         Top             =   120
         Width           =   12165
         _ExtentX        =   21458
         _ExtentY        =   1429
         ButtonWidth     =   1296
         ButtonHeight    =   1429
         Style           =   1
         ImageList       =   "ImageList1"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   4
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Grabar"
               Key             =   "guardar"
               Object.ToolTipText     =   "Grabar Cambios"
               ImageIndex      =   4
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Salir"
               Key             =   "salir"
               ImageIndex      =   10
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Teclado"
               Key             =   "Teclado"
               Description     =   "Teclado"
               Object.ToolTipText     =   "Teclado"
               ImageIndex      =   11
            EndProperty
         EndProperty
      End
   End
   Begin VB.PictureBox picBk 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1680
      ScaleHeight     =   225
      ScaleWidth      =   345
      TabIndex        =   3
      Top             =   3840
      Width           =   375
   End
   Begin MSComDlg.CommonDialog cdg 
      Left            =   5160
      Top             =   600
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.TextBox txtEdit 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   600
      Left            =   3480
      TabIndex        =   2
      Top             =   2160
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.ComboBox cboEdit 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   20.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   615
      Left            =   1920
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   2160
      Visible         =   0   'False
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid grid 
      Height          =   6495
      Left            =   240
      TabIndex        =   0
      Top             =   1800
      Width           =   7815
      _ExtentX        =   13785
      _ExtentY        =   11456
      _Version        =   393216
      RowHeightMin    =   600
      BackColor       =   16448250
      ForeColor       =   4210752
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   16761024
      ForeColorSel    =   16777215
      BackColorBkg    =   16448250
      GridColor       =   4210752
      GridLinesFixed  =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   5040
      Top             =   1260
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   11
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPropiedades.frx":000C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPropiedades.frx":1D9E
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPropiedades.frx":2A78
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPropiedades.frx":480A
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPropiedades.frx":659C
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPropiedades.frx":832E
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPropiedades.frx":A0C0
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPropiedades.frx":BE52
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPropiedades.frx":DBE4
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPropiedades.frx":F976
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPropiedades.frx":11708
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmPropiedades"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Enum eRowGridProperty
    erowTipo
    erowDescripcion
    erowTeclaMask
    erowTecla
    erowBkColor
    erowFuente
    eRowimagen
End Enum

Public fCls As clsConfigBotones
Private mBotonTmp As clsBoton
Private cGrid As clsGrid
Private mColEdit As Long, mRowEdit As Long
Private mCancelar As Boolean

Private Sub FrameTitulo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbLeftButton Then MoverVentana Me.hWnd
End Sub

Private Sub lbl_Organizacion_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    FrameTitulo_MouseMove Button, Shift, X, Y
End Sub

Private Sub lbl_website_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    FrameTitulo_MouseMove Button, Shift, X, Y
End Sub

Private Sub cboEdit_Click()
    cboEdit_LostFocus
End Sub

Private Sub cboEdit_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyReturn
            cboEdit_LostFocus
        Case vbKeyEscape
            cboEdit.Visible = False
    End Select
End Sub

Private Sub cboEdit_LostFocus()
    Dim mValor As String
    
    If cboEdit.Visible Then
        mValor = cboEdit.List(cboEdit.ListIndex)
        cboEdit.Visible = False
        If mRowEdit > erowTipo Then
            If ValidarTecla(CInt(mRowEdit), mValor) Then
                mProperty = IIf(mRowEdit = erowTecla, "Tecla", "TeclaMask")
                CallByName mBotonTmp, mProperty, VbLet, mValor
                GRID.TextMatrix(mRowEdit, 1) = mValor
            End If
        Else
            mBotonTmp.esGrupo = cboEdit.ListIndex = 0
            GRID.TextMatrix(mRowEdit, 1) = mValor
            GRID.Row = 1: grid_KeyPress 0
        End If
    End If
End Sub

Private Sub Form_Load()
    Set cGrid = New clsGrid
    Set mBotonTmp = New clsBoton
    ClonarBoton fCls.BotonSel, mBotonTmp
    IniciarGrid
    
    Me.Height = GRID.Height + 100 + GRID.Top + 400
    'CargarPropiedades
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set cGrid = Nothing
End Sub

Private Sub grid_Click()
    grid_KeyPress 0
End Sub

Private Sub PreventExtraClicks()
    
    If Not GlobalEventTimer Is Nothing Then Set GlobalEventTimer = Nothing
    
    Set GlobalEventTimer = New SelfTimer
    
    GlobalEventTimer.IsGlobalEventHandler = True
        
    ' Inicializar Delegate con los par�metros.
    
    GlobalEventTimer.EventDelegate = New ClsDelegate
    
    With GlobalEventTimer.EventDelegate
        .Initialize
        
        .Name = "AllowCtlEvents"
        
        If Not IsMissing(pControl) Then .SetParam "pControl", "Object", Me
    End With
    
    ' Esperar...
    
    GlobalEventTimer.Interval = 500
    GlobalEventTimer.Enabled = True
    
End Sub

Private Sub grid_KeyPress(KeyAscii As Integer)
    Select Case GRID.Row
        Case erowTipo
            If mBotonTmp.Clave Like "K*" Then
                If Not fCls.PoseeItemsRelacionados(mBotonTmp.Clave) Then
                    'solo mostrar cuando no tenga clave osea es nuevo
                    LlenarCombo GRID.Row
                    cGrid.MostrarEditorCombo Me, GRID, cboEdit, mRowEdit, mColEdit
                Else
                    fCls.Mensaje "No puede cambiar el tipo mientras el bot�n posee items relacionados."
                End If
            End If
            
        Case erowTeclaMask, erowTecla
            
            LlenarCombo GRID.Row
            cGrid.MostrarEditorCombo Me, GRID, cboEdit, mRowEdit, mColEdit
            
        Case erowDescripcion
            If mBotonTmp.esGrupo Then
                cGrid.MostrarEditorTexto Me, GRID, txtEdit, mRowEdit, mColEdit, KeyAscii
            Else
                BuscarProducto
                Me.Enabled = False
                PreventExtraClicks
            End If
        
        Case erowBkColor
            
            CambiarColor
        
        Case erowFuente
            CambiarFuente
            
        Case eRowimagen
            CambiarImagen
            
    End Select
End Sub

Private Sub tb_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case Button.Index
        Case 1 'guardar
            txtEdit_LostFocus
            If Not mBotonTmp.esGrupo Then
                If mBotonTmp.Codigo = vbNullString Then
                    fCls.Mensaje "Debe seleccionar un producto."
                    GRID.Row = 1
                    grid_KeyPress 0
                End If
            End If
            ClonarBoton mBotonTmp, fCls.BotonSel
            fCls.Grabo = True
            Unload Me
        Case 3 'salir
            Unload Me
        Case 4
            TecladoWindows
    End Select
        
End Sub

Private Sub txtEdit_Click()
    If ModoTouch Then TecladoWindows
End Sub

Private Sub txtEdit_GotFocus()
    If ModoTouch Then TecladoWindows
End Sub

Private Sub txtEdit_KeyDown(KeyCode As Integer, Shift As Integer)
     Select Case KeyCode
        Case vbKeyReturn
            txtEdit_LostFocus
        
        Case vbKeyEscape
            txtEdit.Visible = False
            
    End Select
End Sub

Private Sub txtEdit_LostFocus()
    If txtEdit.Visible Then
        txtEdit.Visible = False
        mBotonTmp.Descripcion = txtEdit.Text
        GRID.TextMatrix(erowDescripcion, 1) = mBotonTmp.Descripcion
        ActualizarBoton False, False
    End If
End Sub

Private Sub IniciarGrid()
    Dim I As Integer
    Dim nFilas As Integer
    
    nFilas = IIf(fCls.CamposNuevos, eRowimagen + 1, erowTecla + 1)
   
    cGrid.IniciarGrid GRID, 2, nFilas, flexGridFlat, flexGridFlat, 1, 0
    cGrid.EncabezadoGrid GRID, 0, 1500, "", flexAlignLeftCenter, True
    cGrid.EncabezadoGrid GRID, 1, 6260, "", flexAlignRightCenter, True
    GRID.RowHeightMin = 600
    For I = 0 To nFilas - 1
        GRID.TextMatrix(I, 0) = BuscarTexto(I)
        If I = erowBkColor Then
            picBk.BackColor = BuscarValor(I)
       
        Else
            GRID.TextMatrix(I, 1) = BuscarValor(I)
        End If
    Next I
    If fCls.CamposNuevos Then cGrid.MostrarPicture Me, GRID, picBk, erowBkColor, 1
    GRID.Height = GRID.RowHeightMin * (GRID.Rows) + 100
End Sub

Private Function BuscarTexto(Row As Integer) As String
    Select Case Row
        Case erowTipo
            BuscarTexto = "Tipo"
        Case erowDescripcion
            BuscarTexto = "Descripcion"
        Case erowTeclaMask
            BuscarTexto = "Tecla Mask"
        Case erowTecla
            BuscarTexto = "Tecla"
        Case erowBkColor
            BuscarTexto = "Color"
        Case erowFuente
            BuscarTexto = "Fuente"
        Case eRowimagen
            BuscarTexto = "Imagen"
    End Select
End Function

Private Function BuscarValor(Row As Integer) As String
    Select Case Row
        Case erowTipo
            BuscarValor = IIf(fCls.BotonSel.esGrupo, "Grupo", "Producto")
        Case erowDescripcion
            BuscarValor = fCls.BotonSel.Descripcion
        Case erowTeclaMask
            BuscarValor = fCls.BotonSel.TeclaMask
        Case erowTecla
            BuscarValor = fCls.BotonSel.tecla
        Case erowBkColor
            BuscarValor = fCls.BotonSel.Color
        Case erowFuente
            BuscarValor = fCls.BotonSel.Fuente.Name
        Case eRowimagen
            BuscarValor = IIf(fCls.BotonSel.Imagen.Handle > 0, "Imagen", "N/A")
    End Select
End Function

Private Sub CambiarColor()
    
    On Error GoTo Errores
    
    RowEdit = erowBkColor
    
    cdg.CancelError = True
    
    cdg.Flags = cdCCFullOpen
    
    cdg.ShowColor
    
    mBotonTmp.Color = cdg.Color
    
    picBk.BackColor = mBotonTmp.Color
    
    ActualizarBoton False, False
    
    Exit Sub
    
Errores:
    
    'Debug.Print Err.Number, Err.Description
    
    If Err.Number <> 32755 Then ' Omitir error de cancelar selecci�n en el dialogo.
        fCls.Mensaje "Color inv�lido / Ha ocurrido un error: " & Err.Description
        Err.Clear
    Else
        
        mBotonTmp.Color = BotonBkColor
        
        picBk.BackColor = mBotonTmp.Color
        
        ActualizarBoton False, False
        
    End If
    
End Sub

Private Sub CambiarFuente()
    
    On Error GoTo Errores
    
    mRowEdit = erowFuente
    
    cdg.CancelError = True
    'cdg.Flags = cdlCFANSIOnly
    
    cdg.FontBold = mBotonTmp.Fuente.Bold
    cdg.FontName = mBotonTmp.Fuente.Name
    cdg.FontItalic = mBotonTmp.Fuente.Italic
    cdg.FontSize = mBotonTmp.Fuente.Size
    cdg.FontUnderline = mBotonTmp.Fuente.Underline
    
    cdg.ShowFont
    
    mBotonTmp.Fuente.Bold = cdg.FontBold
    mBotonTmp.Fuente.Name = cdg.FontName
    mBotonTmp.Fuente.Italic = cdg.FontItalic
    mBotonTmp.Fuente.Size = cdg.FontSize
    mBotonTmp.Fuente.Underline = cdg.FontUnderline
    
    GRID.TextMatrix(erowFuente, 1) = mBotonTmp.Fuente.Name
    
    ActualizarBoton True, False
    
    Exit Sub
    
Errores:
    
    'Debug.Print Err.Number, Err.Description
    
    If Err.Number <> 32755 Then ' Omitir error de cancelar selecci�n en el dialogo.
        fCls.Mensaje "Fuente inv�lida / Ha ocurrido un error: " & Err.Description
        Err.Clear
    Else
        
        mBotonTmp.Fuente.Bold = BotonFuenteNeg
        mBotonTmp.Fuente.Name = BotonFuente
        mBotonTmp.Fuente.Italic = False
        mBotonTmp.Fuente.Size = BotonFuenteTam
        mBotonTmp.Fuente.Underline = False
        
        GRID.TextMatrix(erowFuente, 1) = ""
        
        ActualizarBoton True, False
        
    End If
    
End Sub

Private Sub CambiarImagen()
    
    On Error GoTo Errores
    
    mRowEdit = eRowimagen
    
    cdg.CancelError = True
    
    cdg.Flags = cdlOFNFileMustExist
    cdg.Filter = "Im�genes (*.bmp;*.ico;*.jpg;*.gif)|*.bmp;*.ico;*.jpg;*.gif"

    cdg.ShowOpen
    
    If cdg.FileName <> "" Then
        Set mBotonTmp.Imagen = LoadPicture(cdg.FileName)
        GRID.TextMatrix(eRowimagen, 1) = "Imagen"
        ActualizarBoton False, True
    End If
    
    Exit Sub
    
Errores:
    
    'Debug.Print Err.Number, Err.Description
    
    If Err.Number <> 32755 Then ' Omitir error de cancelar selecci�n en el dialogo.
        fCls.Mensaje "Imagen inv�lida / Ha ocurrido un error: " & Err.Description
        Err.Clear
    Else
        Set mBotonTmp.Imagen = LoadPicture()
        GRID.TextMatrix(eRowimagen, 1) = "N/A"
        ActualizarBoton False, True
    End If
    
End Sub

Private Sub LlenarCombo(iRow As Integer)
    
    Dim Var As Variant
    
    Select Case iRow
        Case erowTipo
            Var = Array("Grupo", "Producto")
        Case erowTeclaMask
            Var = Array("Ninguna", "Alt", "Ctrl", "Shift")
        Case erowTecla
            ReDim Var(0)
            Var(0) = "Ninguna"
            For I = Asc("A") To Asc("Z")
                ReDim Preserve Var(I - Asc("A") + 1)
                Var(I - Asc("A") + 1) = Chr(I)
            Next
            
    End Select
    
    cboEdit.Clear
    
    For I = 0 To UBound(Var)
        cboEdit.AddItem Var(I)
    Next I
    
    cboEdit.ListIndex = 0
    
End Sub

Public Function BuscarProducto()
    
    Dim mBusqueda As clsBusqueda
    Dim mResp As Variant
    Dim mSQl As String
    
    On Error GoTo Errores
    
    If Not fCls.Conexion.ConectarBD Then Exit Function
    
    mSQl = "SELECT DISTINCT p.c_Codigo, p.c_Descri, c.c_Codigo AS Barra FROM MA_PRODUCTOS P INNER JOIN MA_CODIGOS C ON c.c_CodNasa = p.c_Codigo "
    
    Set mBusqueda = New clsBusqueda
    
    With mBusqueda
        
        Set .ConexionBusqueda = fCls.Conexion.Conexion
        
        .TextoComandoSql = mSQl
        .AgregarCamposBusqueda "Codigo", "c_codigo", "c_codigo", 1620
        .AgregarCamposBusqueda "Descripcion", "c_descri", "c_descri", 6600, True, opCampoStr, True
        .AgregarCamposBusqueda "Alterno", "c.c_codigo", "barra", 2880, True, opCampoStr
        
        .OrdenarPor = "c_Descri"
        
        .MostrarInterfazBusqueda "P R O D U C T O S", , , , , "%"
        
        If Not IsEmpty(.ResultadoBusqueda) Then
            'mBotonTmp.Codigo = .ResultadoBusqueda(0)(0)
            mBotonTmp.Codigo = .ResultadoBusqueda(0)(2)
            mBotonTmp.Descripcion = .ResultadoBusqueda(0)(1)
            GRID.TextMatrix(erowDescripcion, 1) = mBotonTmp.Descripcion
            ActualizarBoton False, False
        End If
        
    End With
    
    Set mBusqueda = Nothing
    
    Exit Function
    
Errores:
    
    fCls.Mensaje Err.Description, False
    Err.Clear
    
End Function

Private Function ValidarTecla(RowEdit As Integer, Valor As String) As Boolean
    
    'combinaciones ya utilizadas en food
    'Alt + A (actualizar), B (busines) , C (cierres), G (gaveta),
    'Alt + F (doc fiscal no impresos), V (verificacion elec),Z (imprimir Z)
    
    Select Case RowEdit
        Case erowTeclaMask
            ' editando Tecla Mask
            If StrComp(Valor, "Alt", vbTextCompare) = 0 Then
                Select Case mBotonTmp.tecla
                    Case "A", "B", "C", "F", "G", "V", "Z"
                        fCls.Mensaje "Combinaci�n no v�lida o reservada para el sistema.", False
                        Exit Function
                End Select
            End If
            
        Case Else
            ' editando Tecla
            Select Case Valor
                Case "A", "B", "C", "F", "G", "V", "Z"
                    If StrComp(mBotonTmp.TeclaMask, "Alt", vbTextCompare) = 0 Then
                        fCls.Mensaje "Combinaci�n no v�lida o reservada para el sistema.", False
                        Exit Function
                    End If
                
            End Select
    End Select
    
    ValidarTecla = True
    
End Function

Private Sub ClonarBoton(BotonOrig As clsBoton, BotonDest As clsBoton)
    
    Dim mArrPropertys As Variant
    
    mArrPropertys = Array("Clave", "Codigo", "Color", "Descripcion", "esGrupo", "Fuente", "Imagen", "Tecla", "TeclaMask")
    
    For I = 0 To UBound(mArrPropertys)
        If mArrPropertys(I) <> "Imagen" And mArrPropertys(I) <> "Fuente" Then
            CallByName BotonDest, mArrPropertys(I), VbLet, CallByName(BotonOrig, mArrPropertys(I), VbGet)
        Else
            CallByName BotonDest, mArrPropertys(I), VbSet, CallByName(BotonOrig, mArrPropertys(I), VbGet)
        End If
    Next I
    
End Sub

Private Sub ActualizarBoton(updFuente As Boolean, updImagen As Boolean)
    With fCls.CmdButton
        .BackColor = mBotonTmp.Color
        .Caption = mBotonTmp.Descripcion
        If updFuente Then
            .FontBold = mBotonTmp.Fuente.Bold
            .FontItalic = mBotonTmp.Fuente.Italic
            .FontName = mBotonTmp.Fuente.Name
            .FontSize = mBotonTmp.Fuente.Size
            .FontUnderline = mBotonTmp.Fuente.Underline
        End If
        If updImagen Then
            If mBotonTmp.Imagen.Handle > 0 Then
                .Picture = mBotonTmp.Imagen
            Else
                .Picture = LoadPicture()
            End If
        End If
    End With
End Sub
