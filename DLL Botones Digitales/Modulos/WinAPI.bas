Attribute VB_Name = "WinAPI"
Public Declare Function SendMessage Lib "user32" _
                        Alias "SendMessageA" (ByVal hWnd As Long, _
                                              ByVal wMsg As Long, _
                                              ByVal wParam As Long, _
                                              lParam As Any) As Long

Public Declare Sub ReleaseCapture Lib "user32" ()

Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" ( _
                ByVal hWnd As Long, _
                ByVal lpOperation As String, _
                ByVal lpFile As String, _
                ByVal lpParameters As String, _
                ByVal lpDirectory As String, _
                ByVal nShowCmd As Long) As Long

Const WM_NCLBUTTONDOWN = &HA1
Const HTCAPTION = 2

Public Sub MoverVentana(hWnd As Long)
    
    On Error Resume Next
    
    Dim LngReturnValue As Long
    
    Call ReleaseCapture
    LngReturnValue = SendMessage( _
    hWnd, WM_NCLBUTTONDOWN, HTCAPTION, 0&)
    
End Sub
